﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Assets.SceneBuilder.Editor.Providers
{
    public sealed class NodeProvider : IObservable<NodeMessageSignature, NodeMessageData>
    {
        private List<IObserver<NodeMessageSignature, NodeMessageData>> observers;

        public NodeProvider()
        {
            if (observers == null)
            {
                observers = new List<IObserver<NodeMessageSignature, NodeMessageData>>();
            }
        }

        ~NodeProvider()
        {
            Debug.Log("NodeProvider Destructor called");
            observers = null;
        }

        public IDisposable Subscribe(IObserver<NodeMessageSignature, NodeMessageData> observer)
        {
            if (!observers.Contains(observer))
                observers.Add(observer);
            return new Unsubscriber(observers, observer);
        }

        class Unsubscriber : IDisposable
        {
            private List<IObserver<NodeMessageSignature, NodeMessageData>> _observers;
            private IObserver<NodeMessageSignature, NodeMessageData> _observer;

            ~Unsubscriber()
            {
                //Debug.Log("Unsubscriber Class Destructor Called.");
            }

            public Unsubscriber(List<IObserver<NodeMessageSignature, NodeMessageData>> observers, IObserver<NodeMessageSignature, NodeMessageData> observer)
            {
                this._observers = observers;
                this._observer = observer;
            }

            public void Dispose()
            {
                if (_observer != null && _observers.Contains(_observer))
                    _observers.Remove(_observer);
            }
        }

        public void Notify(NodeMessageSignature _signature, NodeMessageData _message)
        {
            foreach (var observer in observers)
            {
                if (_message == null)
                    observer.OnError(new NodeException());
                else
                    if (_signature.Equals(NodeMessageSignature.Exit) || _signature.Equals(NodeMessageSignature.Removed))
                    {
                        observer.OnCompleted(_signature, _message);
                    }
                    else
                    {
                        observer.OnNext(_signature, _message);
                    }
            }
        }

    }
}