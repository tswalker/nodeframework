﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using Assets.SceneBuilder.Editor.Utilities;

namespace Assets.SceneBuilder.Editor.Providers
{
    public class ElementMessageData
    {

        private bool _stateSwitch;
        private ActionTarget _actionTarget;

        public bool StateSwitch
        {
            get { return _stateSwitch; }
            set { _stateSwitch = value; }
        }

        public ActionTarget MessageActionTarget
        {
            get { return _actionTarget; }
            set { _actionTarget = value; }
        }

        public ElementMessageData(bool stateSwitch)
        {
            _stateSwitch = stateSwitch;
        }

        public ElementMessageData(ActionTarget target)
        {
            _actionTarget = target;
        }
    }

    public enum ElementMessageSignature
    {
        Exit,
        Removed,
        Focus,
        Lock
    }
}