﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Assets.SceneBuilder.Editor.Providers
{
    public sealed class ElementProvider : IObservable<ElementMessageSignature, ElementMessageData>
    {

        private List<IObserver<ElementMessageSignature, ElementMessageData>> observers;

        public ElementProvider()
        {
            if (observers == null)
            {
                observers = new List<IObserver<ElementMessageSignature, ElementMessageData>>();
            }
        }

        ~ElementProvider()
        {
            Debug.Log("ElementProvider Destructor called");
            observers = null;
        }

        public IDisposable Subscribe(IObserver<ElementMessageSignature, ElementMessageData> observer)
        {
            if (!observers.Contains(observer))
                observers.Add(observer);
            return new Unsubscriber(observers, observer);
        }

        class Unsubscriber : IDisposable
        {
            private List<IObserver<ElementMessageSignature, ElementMessageData>> _observers;
            private IObserver<ElementMessageSignature, ElementMessageData> _observer;

            ~Unsubscriber()
            {
                //Debug.Log("Unsubscriber Class Destructor Called.");
            }

            public Unsubscriber(List<IObserver<ElementMessageSignature, ElementMessageData>> observers, IObserver<ElementMessageSignature, ElementMessageData> observer)
            {
                this._observers = observers;
                this._observer = observer;
            }

            public void Dispose()
            {
                if (_observer != null && _observers.Contains(_observer))
                    _observers.Remove(_observer);
            }
        }

        public void Notify(ElementMessageSignature _signature, ElementMessageData _message)
        {
            foreach (var observer in observers)
            {
                if (_message == null)
                    observer.OnError(new ElementException());
                else
                    if (_signature.Equals(ElementMessageSignature.Exit) | _signature.Equals(ElementMessageSignature.Removed))
                    {
                        observer.OnCompleted(_signature, _message);
                    }
                    else
                    {
                        observer.OnNext(_signature, _message);
                    }
            }
        }
    }
}