﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Assets.SceneBuilder.Editor.Providers
{
    public sealed class PanelProvider : IObservable<PanelMessageSignature, PanelMessageData>
    {

        private List<IObserver<PanelMessageSignature, PanelMessageData>> observers;

        public PanelProvider()
        {
            if (observers == null)
            {
                observers = new List<IObserver<PanelMessageSignature, PanelMessageData>>();
            }
        }

        ~PanelProvider()
        {
            Debug.Log("PanelProvider Destructor called");
            observers = null;
        }

        public IDisposable Subscribe(IObserver<PanelMessageSignature, PanelMessageData> observer)
        {
            if (!observers.Contains(observer))
                observers.Add(observer);
            return new Unsubscriber(observers, observer);
        }

        class Unsubscriber : IDisposable
        {
            private List<IObserver<PanelMessageSignature, PanelMessageData>> _observers;
            private IObserver<PanelMessageSignature, PanelMessageData> _observer;

            ~Unsubscriber()
            {
                //Debug.Log("Unsubscriber Class Destructor Called.");
            }

            public Unsubscriber(List<IObserver<PanelMessageSignature, PanelMessageData>> observers, IObserver<PanelMessageSignature, PanelMessageData> observer)
            {
                this._observers = observers;
                this._observer = observer;
            }

            public void Dispose()
            {
                if (_observer != null && _observers.Contains(_observer))
                    _observers.Remove(_observer);
            }
        }

        public void Notify(PanelMessageSignature _signature, PanelMessageData _message)
        {
            foreach (var observer in observers)
            {
                if (_message == null)
                    observer.OnError(new PanelException());
                else
                    if (_signature.Equals(PanelMessageSignature.Exit))
                    {
                        observer.OnCompleted(_signature, _message);
                    }
                    else
                    {
                        observer.OnNext(_signature, _message);
                    }
            }
        }
    }
}