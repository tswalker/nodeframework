﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;

namespace Assets.SceneBuilder.Editor.Providers
{
    public class PanelMessageData
    {
        public bool StateSwitch { get; private set; }
        public Vector2 Translate { get; private set; }

        public PanelMessageData(bool stateSwitch)
        {
            StateSwitch = stateSwitch;
        }

        public PanelMessageData(Vector2 translate)
        {
            Translate = translate;
        }
    }

    public enum PanelMessageSignature
    {
        None,
        Exit,
        Translate
    }
}