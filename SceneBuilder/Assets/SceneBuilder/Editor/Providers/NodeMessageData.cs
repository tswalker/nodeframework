﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using Assets.SceneBuilder.Editor.Utilities;

namespace Assets.SceneBuilder.Editor.Providers
{
    public class NodeMessageData
    {
        private bool _stateSwitch;
        private String _message;
        private Vector2 _translate;
        private ActionTarget _actionTarget;
        private Marquis _marquis;

        public String Message
        {
            get { return _message; }
            set { _message = value; }
        }
        public bool StateSwitch
        {
            get { return _stateSwitch; }
            set { _stateSwitch = value; }
        }

        public Vector2 Translate
        {
            get { return _translate; }
            set { _translate = value; }
        }

        public ActionTarget MessageActionTarget
        {
            get { return _actionTarget; }
            set { _actionTarget = value; }
        }

        public Marquis MarquisFrame
        {
            get { return _marquis; }
            set { _marquis = value; }
        }
        public NodeMessageData(bool stateSwitch)
        {
            _stateSwitch = stateSwitch;
        }

        public NodeMessageData(Vector2 translate)
        {
            _translate = translate;
        }

        public NodeMessageData(ActionTarget target)
        {
            _actionTarget = target;
        }

        public NodeMessageData(Marquis marquis)
        {
            _marquis = marquis;
        }

        public NodeMessageData(String _string)
        {
            _message = _string;
        }

    }

    public enum NodeMessageSignature
    {
        Exit,
        Lock,
        Translate,
        Draggable,
        Focus,
        Removed,
        Marquis
    }

    public class Marquis
    {
        private Rect _marquisArea;

        public Rect Area
        {
            get { return _marquisArea; }
        }
        public Marquis(Rect _view)
        {
            _marquisArea = new Rect(_view);
        }
    }
}