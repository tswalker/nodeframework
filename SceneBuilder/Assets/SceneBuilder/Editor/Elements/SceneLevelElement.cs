﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using Assets.SceneBuilder.Editor.Providers;

namespace Assets.SceneBuilder.Editor.Elements
{
    [Serializable]
    [ElementAttribute(ElementSubCategory.Level, ElementCategory.SCENE, typeof(SceneLevelElement))]
    sealed class SceneLevelElement : Element
    {

        public override void Initialize(String _parent, Vector2 _location, string _title, Vector2 _size)
        {
            base.Initialize(_parent, _location, _title, _size);
        }
    }
}
