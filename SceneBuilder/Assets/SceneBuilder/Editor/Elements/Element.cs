﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using Assets.SceneBuilder.Editor.Utilities;
using Assets.SceneBuilder.Editor.Providers;
using Assets.SceneBuilder.Editor.Data;
using System.ComponentModel;

namespace Assets.SceneBuilder.Editor.Elements
{
    [Serializable]
    public class Element : ScriptableObject, IObserver<ElementMessageSignature, ElementMessageData>, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;


        [NonSerialized]
        private IDisposable unsubscriber;

        [SerializeField]
        private Rect _frameArea, _linkArea;

        [SerializeField]
        private Vector2 _position;

        [SerializeField]
        private Vector2 _elementSize;

        private static String _elementActiveID;

        private SSBStyles _elementStyle, _linkStyle;
        private bool _elementHOT, _linkHOT;
        private bool _inDrag = false;

        public RollupLocation RollupLocation { get; set; }

        public static bool Marqing { get; set; }

        //private static GUISkinSSB Skin;
        //private GUISkinSSB Skin;

        [SerializeField]
        public String ParentNodeGUID { get; set; }

        ~Element() //Destructor
        {
            Debug.Log("Element Destructor called:" + ElementGUID);
            //Skin = null;
        }

        public virtual void OnEnable() //required
        {
            hideFlags = HideFlags.HideAndDontSave;
            //if (Skin == null)
            //{
            //    Skin = new GUISkinSSB();
            //}
        }
        public void OnDisable()
        {
            Debug.Log("Element Base Class: OnDisable Called.");
            //Skin = null;
        }

        public virtual void OnCompleted(ElementMessageSignature signature, ElementMessageData value)
        {
            if (signature == ElementMessageSignature.Removed && value.MessageActionTarget == ActionTarget.SELECTED && ElementActive)
            {
                Debug.Log("Element Provider; Remove Selected Condition, ElementID:" + ElementGUID);
            }
            if (signature == ElementMessageSignature.Removed && value.MessageActionTarget == ActionTarget.ALL)
            {
                Debug.Log("Element Provider; Remove All Condition, ElementID:" + ElementGUID);
            }
            if (signature == ElementMessageSignature.Exit)
            {
                Debug.Log("Element Provider; Exit Condition, ElementID:" + ElementGUID);
            }
        }

        public void OnError(Exception exception)
        {
            Debug.Log("Provider Error Message:" + exception.ToString());
        }

        public virtual void OnNext(ElementMessageSignature signature, ElementMessageData value)
        {
            if (signature == ElementMessageSignature.Focus && value.MessageActionTarget.Equals(ActionTarget.OFF))
            {
                ElementActive = false;
            }
            if (signature == ElementMessageSignature.Focus && value.MessageActionTarget.Equals(ActionTarget.RESET))
            {
                ElementActiveGUID = String.Empty;
                ElementActive = false;
            }
            if (signature == ElementMessageSignature.Focus && value.MessageActionTarget.Equals(ActionTarget.ON) && ElementGUID.Equals(ElementActiveGUID))
            {
                ElementActiveGUID = ElementGUID;
                ElementActive = true;
            }
            if (signature == ElementMessageSignature.Lock)
            {
                Marqing = value.StateSwitch;
            }
        }

        public bool Subscribed
        {
            get { return unsubscriber == null ? false : true; }
        }

        internal void Subscribe(ref ElementProvider _provider)
        {
            if (_provider != null)
            {
                unsubscriber = _provider.Subscribe(this);
            }
        }

        public void Subscribe(IObservable<ElementMessageSignature, ElementMessageData> provider)
        {
            if (provider != null)
            {
                unsubscriber = provider.Subscribe(this);
            }
        }

        public void Unsubscribe()
        {
            if (unsubscriber != null)
            {
                unsubscriber.Dispose();
                unsubscriber = null;
            }
            else
            {
                Debug.LogWarning("Element: " + ElementGUID + ", failure to subscribe after recompile.");
            }
        }

        private void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public Vector2 Position
        {
            get { return _position; }
            set { _position.Set(value.x, value.y); }
        }

        public Vector2 Size
        {
            get { return _elementSize; }
            protected set
            {
                _elementSize = value;
            }
        }

        [SerializeField]
        public String ElementGUID { get; set; }

        public Rect FrameArea
        {
            get { return _frameArea; }
            protected set { _frameArea = value; }
        }

        public float FrameAreaSetY
        {
            get { return FrameArea.y; }
            set { FrameArea = new Rect(FrameArea.x, value, FrameArea.width, FrameArea.height); }
        }

        [SerializeField]
        public bool ElementActive { get; set; }

        [SerializeField]
        public bool ElementLinked { get; set; }

        public String ElementActiveGUID
        {
            get { return _elementActiveID; }
            set
            {
                _elementActiveID = value;
                NotifyPropertyChanged("ElementActived");
            }
        }

        public Rect LinkArea
        {
            get { return _linkArea; }
            protected set { _linkArea = value; }
        }

        public bool ElementHOT
        {
            get { return _elementHOT; }
            set { _elementHOT = value; }
        }

        public bool LinkHOT
        {
            get { return _linkHOT; }
            set { _linkHOT = value; }
        }

        //DrawElementWindow
        public virtual void DrawElementWindow(int id, float _scrollValueY)//, float _frameOffsetY)
        {
            _elementStyle = ElementActive ? SSBStyles.ElementActive : SSBStyles.Element;
            if (ElementLinked)
            {
                _linkStyle = ElementActive ? SSBStyles.ElementLinkedON : SSBStyles.ElementLinked;
            }
            else
            {
                _linkStyle = ElementActive ? SSBStyles.ElementLinkON : SSBStyles.ElementLink;
            }


            //FrameAreaSetY += (_frameOffsetY - _scrollValueY);
            FrameAreaSetY += (-_scrollValueY);
            GUILayout.BeginArea(FrameArea);
            {
                GUI.Box(new Rect(0, 0, Size.x, Size.y), GUIContent.none, GUISkinSSB.SkinSSB.customStyles[GUISkinSSB.SSBStyle(_elementStyle)]);
            }
            GUILayout.EndArea();
            //GUI.Box(new Rect(FrameArea.width -11, FrameArea.y + (FrameArea.height/2) -6, 10, 10), GUIContent.none, GUISkinSSB.SkinSSB.customStyles[GUISkinSSB.SSBStyle(SSBStyles.RedBox)]);
            LinkArea = new Rect(FrameArea.width - 11, FrameArea.y + (FrameArea.height / 2) - 6, 10, 10);
            GUILayout.BeginArea(LinkArea);
            {
                GUI.Box(new Rect(0, 0, 10, 10), GUIContent.none, GUISkinSSB.SkinSSB.customStyles[GUISkinSSB.SSBStyle(_linkStyle)]);
            }
            GUILayout.EndArea();

            HandleEvents(Event.current);
        }

        protected virtual void HandleEvents(Event e)
        {
            if (ElementActiveGUID != this.ElementGUID)
            {
                ElementActive = false;
            }

            if (FrameArea.Contains(e.mousePosition) && e.rawType == EventType.MouseUp && (e.button == 0) && !ElementActive && !Marqing)
            {
                ElementActiveGUID = ElementGUID;
                ElementActive = true;
                e.Use();
                GUIUtility.hotControl = 0;
                //Debug.Log("active element: " + ElementActiveID + ",state:" + ElementActive);
            }

            if (FrameArea.Contains(e.mousePosition) && e.rawType == EventType.MouseUp && (e.button == 0) && ElementActive && !Marqing)
            {
                ElementActiveGUID = String.Empty;
                ElementActive = false;
                e.Use();
                GUIUtility.hotControl = 0;
                //Debug.Log("active element: " + ElementActiveID + ",state:" + ElementActive);
            }

            if (!FrameArea.Contains(e.mousePosition) && e.rawType == EventType.MouseDrag)
            {
                _inDrag = true;
            }
            if (!FrameArea.Contains(e.mousePosition) && e.rawType == EventType.MouseUp && e.type != EventType.ContextClick && !Marqing && !_inDrag)
            {
                ElementActive = false;
            }
            if (e.rawType == EventType.MouseUp && _inDrag)
            {
                _inDrag = false;
            }
        }

        public virtual void ContextCallback(object userData)
        {
            Debug.Log("Element:" + ElementGUID + ", context item selected:" + userData);
        }

        public virtual void Initialize(String _parent, Vector2 _location, string _title, Vector2 _size)
        {
            ParentNodeGUID = _parent;
            ElementGUID = Guid.NewGuid().ToString();
            ElementActive = false;
            Size = _size;
            Position = new Vector2(_location.x, _location.y);
            FrameArea = new Rect(_location.x, _location.y, _size.x, _size.y);
            ElementLinked = false;
            RollupLocation = RollupLocation.NONE;
        }


    }
}