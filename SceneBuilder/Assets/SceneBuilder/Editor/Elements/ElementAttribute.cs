﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Assets.SceneBuilder.Editor.Elements
{
    [SerializableAttribute]
    [AttributeUsage(AttributeTargets.Class)]

    public class ElementAttribute : Attribute
    {
        private string _displayName;
        private ElementCategory _category;
        private ElementSubCategory _subCategory;
        private string _descriptor = "";
        private Type _elementType;

        public Type ElementType
        {
            get { return _elementType; }
        }

        public ElementSubCategory SubCategory
        {
            get { return _subCategory; }
        }

        public ElementCategory Category
        {
            get { return _category; }
        }

        public string Descriptor
        {
            get { return _descriptor; }
        }

        public ElementAttribute(ElementSubCategory subcat, ElementCategory category, Type elementType)
        {
            _subCategory = subcat;
            _elementType = elementType;
            _category = category;
        }
    }

    public enum ElementCategory
    {
        SCENE,
        LOCATOR,
        POOL
    }

    public enum ElementSubCategory
    {
        Level
    }
}