﻿using UnityEngine;
using UnityEditor;
using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Runtime.CompilerServices;
using Assets.SceneBuilder.Editor.Elements;
using Assets.SceneBuilder.Editor.Nodes;
using Assets.SceneBuilder.Editor.Panels;
using Assets.SceneBuilder.Editor.Providers;
using Assets.SceneBuilder.Editor.Utilities;

namespace Assets.SceneBuilder.Editor.Data
{
    [Serializable]
    public sealed class DataController : ScriptableObject
    {
        public static EventHandler<DataPropertyChangeEventArgs> DataPropertyChangeEvent;

        [SerializeField]
        private List<Node> NodeList { get; set; }

        [SerializeField]
        private List<Element> ElementList { get; set; }

        [SerializeField]
        private List<Connectoid> ConnectoidList { get; set; }

        [SerializeField]
        private int _nodeID;

        [SerializeField]
        private List<NodeBoundary> NodeBoundaries { get; set; }

        [SerializeField]
        private List<ElementLink> ElementLinks { get; set; }

        [SerializeField]
        private List<NodeLink> NodeLinks { get; set; }

        [SerializeField]
        public List<ConnectoidPath> Paths { get; set; }
        private List<ConnectoidPath> LivePath { get; set; }

        private static NodeProvider _nodeProvider;
        private bool _clearDAC = false;
        private const float _zoneYOffset = 20f;
        private const float _zoneXOffset = 10f;

        private const String _guidFormat = "^[A-Fa-f0-9]{32}$|" +
                                   "^({|\\()?[A-Fa-f0-9]{8}-([A-Fa-f0-9]{4}-){3}[A-Fa-f0-9]{12}(}|\\))?$|" +
                                   "^({)?[0xA-Fa-f0-9]{3,10}(, {0,1}[0xA-Fa-f0-9]{3,6}){2}, {0,1}({)([0xA-Fa-f0-9]{3,4}, {0,1}){7}[0xA-Fa-f0-9]{3,4}(}})$";




        // DataController: Destructor
        ~DataController()
        {
            Debug.Log("DataController: Destructor Called.");
        }

        void OnEnable()
        {
            Debug.Log("DataController OnEnabled Called.");
            hideFlags = HideFlags.HideAndDontSave;
            if (NodeList == null)
            {
                NodeList = new List<Node>();
            }
            if (ElementList == null)
            {
                ElementList = new List<Element>();
            }
            if (ConnectoidList == null)
            {
                ConnectoidList = new List<Connectoid>();
            }
            if (NodeBoundaries == null)
            {
                NodeBoundaries = new List<NodeBoundary>();
            }
            if (ElementLinks == null)
            {
                ElementLinks = new List<ElementLink>();
            }
            if (NodeLinks == null)
            {
                NodeLinks = new List<NodeLink>();
            }
            if (Paths == null)
            {
                Paths = new List<ConnectoidPath>();
            }
            if (LivePath == null)
            {
                LivePath = new List<ConnectoidPath>();
            }

            if (_nodeID.Equals(0))
            {
                _nodeID = 1000;
            }

            foreach (Node node in NodeList)
            {
                if (node.DAC == null)
                {
                    node.DAC = this;
                }
                node.PropertyChanged += NodePropertyChanged;  //re-hooks event handler
            }

            foreach (Element element in ElementList)
            {
                element.PropertyChanged += ElementPropertyChanged;
            }

            Linker.OnLinkComplete += AddLink;
        }

        void OnDisable()
        {
            foreach (Node node in NodeList)
            {
                node.PropertyChanged -= NodePropertyChanged;
            }
            foreach (Element element in ElementList)
            {
                element.PropertyChanged -= ElementPropertyChanged;
            }
            Linker.OnLinkComplete -= AddLink;

            ReleaseLivePath();
            //Debug.Log("DataController: OnDisable Called.");
        }

        void OnDestroy()
        {
            if (_nodeProvider != null)
            {
                _nodeProvider = null;
            }
            RemoveAllNodesFinal();
            RemoveAllElementsFinal();
            ClearConnectoidPaths();
            RemoveAllConnectoidsFinal();
            Paths = null;
            LivePath = null;
        }

        internal void EstablishAllNodesProvider(ref NodeProvider _provider)
        {
            if (_provider == null)
            {
                return;
            }
            _nodeProvider = _provider;
            foreach (Node node in NodeList)
            {
                ConnectNodeProvider(node, ref _provider);
            }
        }

        internal void EstablishChildElementsProvider(String _parent, ref ElementProvider _provider)
        {
            if (_provider == null)
            {
                return;
            }
            foreach (Element element in ElementList.FindAll(x => x.ParentNodeGUID == _parent))
            {
                ConnectElementProvider(element, ref _provider);
            }
        }

        public void SetElementProvider(String _element, ref ElementProvider _provider)
        {
            if (_provider == null)
            {
                return;
            }
            ConnectElementProvider(ElementList.Find(x => x.ElementGUID == _element), ref _provider);
        }

        public void AddNode(Node node)
        {
            if (node == null)
            {
                //Debug.LogWarning("Invalid Node: unable to add to DataController.");
                return;
            }
            _nodeID++;
            node.NodeWindowID = _nodeID;
            node.DAC = this;
            NodeList.Add(node);
            ConnectNodeProvider(node, ref _nodeProvider);
            node.PropertyChanged += NodePropertyChanged;

            NodeBoundary _bounds = CreateInstance<NodeBoundary>();
            _bounds.GUID = node.NodeGUID;
            _bounds.Boundary = GenerateNodeBoundary(node);
            NodeBoundaries.Add(_bounds);

            if (node.Connectable)
            {
                NodeLink _link = CreateInstance<NodeLink>();
                _link.GUID = node.NodeGUID;
                _link.Center = CalculateLinkNodeConnector(node);
                NodeLinks.Add(_link);
            }
        }

        public void AddElement(String _parentGUID, Element element)
        {
            if (element == null)
            {
                //Debug.Log("Invalid Element: unable to add to DataController.");
                return;
            }
            ElementList.Add(element);

            ElementLink _elementLink = CreateInstance<ElementLink>();
            _elementLink.GUID = element.ElementGUID;
            _elementLink.ParentGUID = _parentGUID;
            _elementLink.Center = CalculateElementConnector(NodeList.Find(x => x.NodeGUID == element.ParentNodeGUID), element);
            ElementLinks.Add(_elementLink);

            element.PropertyChanged += ElementPropertyChanged;
        }

        private void ElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Element element = sender as Element;
            if (element != null)
            {
                switch (e.PropertyName)
                {
                    case "ElementActived":
                        {
                            //Debug.Log("element activated: " + element.ElementActiveGUID);
                            foreach (Connectoid connectoid in ConnectoidList.FindAll(x => x.Active.Equals(true) && !x.Source.Equals(element.ElementGUID)))
                            {
                                connectoid.Active = false;
                            }

                            //activate
                            foreach (Connectoid connectoid in ConnectoidList.FindAll(x => x.Source.Equals(element.ElementActiveGUID)))
                            {
                                connectoid.Active = true;
                            }

                            //set paths
                            foreach (ConnectoidPath _path in Paths.FindAll(x => x.Active.Equals(true)))
                            {
                                _path.Active = false;
                            }
                            if (element.ElementActiveGUID.Equals(String.Empty))
                            {
                                //Debug.Log("returning, empty element active guid.");
                                return;
                            }
                            else
                            {
                                foreach (ConnectoidPath _path in Paths.FindAll(x => x.ElementGUID.Equals(element.ElementActiveGUID)))
                                {
                                    _path.Active = true;
                                }
                                Paths.Sort((p1, p2) => p1.Active.CompareTo(p2.Active));
                            }
                            break;
                        }

                }
            }
        }

        private void NodePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //Debug.Log("nodepropertychanged event handler fired.");
            Node node = sender as Node;
            if (node != null)
            {
                switch (e.PropertyName)
                {
                    case "CursorChange":
                        {
                            EventHandler<DataPropertyChangeEventArgs> _handler = DataPropertyChangeEvent;
                            if (_handler != null)
                            {
                                DataPropertyChangeEventArgs _event = new DataPropertyChangeEventArgs();
                                _event.Name = "CursorChange";
                                _event.State = node.CursorChange;
                                DataPropertyChangeEvent(null, _event);
                            }
                            break;
                        }
                    case "NodeAreaChanging":
                        {
                            //recalculate node link if link node type
                            if (node.Connectable)
                            {
                                NodeLinks.Find(x => x.GUID == node.NodeGUID).Center = CalculateLinkNodeConnector(node);
                            }
                            ProcessElementStackChanges(node);
                            //recalculate all paths
                            RegeneratePaths();
                            break;
                        }
                    case "NodeElementReset":
                        {
                            //deactivate all elements on focus change
                            foreach (Element element in ElementList.FindAll(x => x.ElementActive.Equals(true)))
                            {
                                element.ElementActive = false;
                                element.ElementActiveGUID = String.Empty;
                            }
                            break;
                        }
                    case "NodeFocusChange":
                        {
                            foreach (Connectoid connectoid in ConnectoidList.FindAll(x => x.Active.Equals(true) && !x.Destination.Equals(node.NodeGUID)))
                            {
                                connectoid.Active = false;
                            }

                            //activate
                            foreach (Connectoid connectoid in ConnectoidList.FindAll(x => x.Destination.Equals(node.NodeGUID)))
                            {
                                connectoid.Active = true;
                            }

                            //set paths
                            foreach (ConnectoidPath _path in Paths.FindAll(x => x.Active.Equals(true)))
                            {
                                _path.Active = false;
                            }
                            if (node.FocusID.Equals(-1))
                            {
                                return;
                            }
                            else
                            {
                                foreach (ConnectoidPath _path in Paths.FindAll(x => x.NodeLinkGUID.Equals(node.NodeGUID)))
                                {
                                    _path.Active = true;
                                }
                                Paths.Sort((p1, p2) => p1.Active.CompareTo(p2.Active));
                            }
                            break;
                        }
                    case "NodeElementStackChanged":
                        {
                            ProcessElementStackChanges(node);
                            RegeneratePaths();
                            break;
                        }
                    case "NodeFoldoutChanged":
                        {
                            ProcessElementStackChanges(node);
                            RegeneratePaths();
                            break;
                        }
                }
            }
            node = null;
        }

        private void RegeneratePaths()
        {
            ClearConnectoidPaths();
            foreach (Connectoid connectoid in ConnectoidList)
            {
                ConnectoidPath _path = CreateInstance<ConnectoidPath>();
                _path.Path = CalculateConnectoidPath(connectoid, Vector2.zero, false);
                _path.ElementGUID = connectoid.Source;
                _path.NodeLinkGUID = connectoid.Destination;
                _path.Active = connectoid.Active;
                Paths.Add(_path);
            }
            Paths.Sort((p1, p2) => p1.Active.CompareTo(p2.Active));
        }

        private void ProcessElementStackChanges(Node node)
        {
            NodeBoundary _boundary = NodeBoundaries.Find(x => x.GUID == node.NodeGUID);
            _boundary.Boundary = GenerateNodeBoundary(node);

            //calculate child element centers and Rollup state
            foreach (ElementLink element in ElementLinks.FindAll(x => x.ParentGUID == node.NodeGUID))
            {
                Vector2 _elementCenter = CalculateElementConnector(node, ElementList.Find(x => x.ElementGUID == element.GUID));
                ElementLinks.Find(x => x.GetHashCode().Equals(element.GetHashCode())).Center = _elementCenter;
                RollupLocation _state = CalculateElementLinkRelativeToElementArea(node, _elementCenter);

                foreach (Connectoid connectoid in ConnectoidList.FindAll(x => x.Source.Equals(element.GUID)))
                {
                    connectoid.Rollup = _state;
                    connectoid.RollupCenter = CalculateRollupCenter(node, _state);
                }
            }

            //update node rollup state
            int upperCount = (from connectoid in ConnectoidList
                              join element in ElementList on connectoid.Source equals element.ElementGUID
                              join children in ElementList on element.ParentNodeGUID equals node.NodeGUID
                              where connectoid.Rollup == RollupLocation.UPPER
                              select connectoid).Count();
            node.RollupUpperActive = upperCount > 0 ? true : false;

            int lowerCount = (from connectoid in ConnectoidList
                              join element in ElementList on connectoid.Source equals element.ElementGUID
                              join children in ElementList on element.ParentNodeGUID equals node.NodeGUID
                              where connectoid.Rollup == RollupLocation.LOWER
                              select connectoid).Count();
            node.RollupLowerActive = lowerCount > 0 ? true : false;
        }

        private Vector2 CalculateRollupCenter(Node node, RollupLocation _state)
        {
            Vector2 _center = new Vector2();
            switch (_state)
            {
                case RollupLocation.UPPER:
                    {
                        _center = new Vector2(node.Position.x + node.Position.width - 12, node.Position.y + node.UpperRollupArea.center.y + EditorStyles.toolbarButton.fixedHeight);
                        break;
                    }
                case RollupLocation.LOWER:
                    {
                        _center = new Vector2(node.Position.x + node.Position.width - 12, node.Position.y + node.LowerRollupArea.center.y + EditorStyles.toolbarButton.fixedHeight);
                        break;
                    }
            }
            return _center;
        }


        public void RemoveSelectedNodes()
        {
            if (NodeList.FindAll(x => x.NodeSelected).Count().Equals(NodeList.Count())) //just checking if all nodes were actually removed in a select
            {
                _clearDAC = true;
            }
            foreach (Node node in NodeList.FindAll(x => x.NodeSelected))
            {
                //de-link any elements connected to this node
                //foreach (Connectoid connectoid in ConnectoidList.FindAll(x => x.Destination.Equals(node.NodeGUID)))
                //{
                //    //check if other connectoids are attached to the element before setting
                //    ElementList.Find(x => x.ElementGUID.Equals(connectoid.Source)).ElementLinked = false;
                //}

                ClearConnectoidsOfNode(node.NodeGUID);
                RemoveAllNodeElements(node.NodeGUID);
                node.PropertyChanged -= NodePropertyChanged;
                node.Unsubscribe();

                ClearNodeBoundariesList(node);
                //NodeBoundaries.RemoveAll(x => x.GUID == node.NodeGUID);

                ClearElementLinks(node);
                ClearNodeLinks(node);

                if (_clearDAC) { node.DAC = null; }
                DestroyImmediate(node);
            }
            NodeList.RemoveAll(x => x.NodeSelected);
            ValidateElementLinks();
        }

        public void RemoveAllNodes()
        {
            foreach (Node node in NodeList)
            {
                RemoveAllNodeElements(node.NodeGUID);
                node.PropertyChanged -= NodePropertyChanged;
                node.Unsubscribe();
                node.DAC = null;
                DestroyImmediate(node);
            }
            ClearConnectoids();
            //ConnectoidList.Clear();

            NodeList.Clear();

            ClearNodeBoundariesList();
            //NodeBoundaries.Clear();
            //ElementLinks.Clear();
            ClearElementLinksList();
            //NodeLinks.Clear();
            ClearNodeLinksList();
            ClearConnectoidPaths();
        }

        private void RemoveAllNodesFinal()
        {
            if (NodeList != null && !NodeList.Count.Equals(0))
            {
                foreach (Node node in NodeList)
                {
                    node.PropertyChanged -= NodePropertyChanged;
                    node.Unsubscribe();
                    node.DAC = null;
                    DestroyImmediate(node);
                }
                NodeList.Clear();

                ClearNodeBoundariesList();
                //NodeBoundaries.Clear();
                //NodeLinks.Clear();
                ClearNodeLinksList();
            }
            NodeList = null;
            NodeBoundaries = null;
            NodeLinks = null;
        }

        public void RemoveSelectedNodeElements(String _parent)
        {

            foreach (Element element in ElementList.FindAll(x => (x.ParentNodeGUID == _parent && x.ElementActive)))
            {
                ClearConnectoidPaths(element);
                ClearConnectoidListOfElement(element.ElementGUID);
                element.Unsubscribe();
                //ElementLinks.RemoveAll(x => x.GUID == element.ElementGUID);
                ClearElementLinks(element);
                DestroyImmediate(element);
            }
            ElementList.RemoveAll(x => (x.ParentNodeGUID == _parent && x.ElementActive));
        }

        public void RemoveAllNodeElements(String _parent)
        {

            foreach (Element element in ElementList.FindAll(x => (x.ParentNodeGUID == _parent)))
            {
                ClearConnectoidPaths(element);
                ClearConnectoidListOfElement(element.ElementGUID);
                element.Unsubscribe();
                DestroyImmediate(element);
            }
            ElementList.RemoveAll(x => x.ParentNodeGUID == _parent);
            //ElementLinks.RemoveAll(x => x.ParentGUID == _parent);
            ClearElementLinks(NodeList.Find(x => x.NodeGUID.Equals(_parent)));
        }

        private void RemoveAllElementsFinal()
        {
            if (ElementList != null && !ElementList.Count.Equals(0))
            {
                foreach (Element element in ElementList)
                {
                    element.Unsubscribe();  //clears nodeProvider references
                    DestroyImmediate(element); //required to clear dangling references
                }
                ElementList.Clear();
                //ElementLinks.Clear();
                ClearElementLinksList();
            }
            ElementList = null;
            ElementLinks = null;
        }

        private void ClearNodeBoundariesList()
        {
            foreach (NodeBoundary boundary in NodeBoundaries)
            {
                DestroyImmediate(boundary);
            }
            NodeBoundaries.Clear();
        }

        private void ClearNodeBoundariesList(Node node)
        {
            foreach (NodeBoundary boundary in NodeBoundaries.FindAll(x => x.GUID.Equals(node.NodeGUID)))
            {
                DestroyImmediate(boundary);
            }
            NodeBoundaries.RemoveAll(x => x.GUID == node.NodeGUID);
        }

        private void ClearNodeLinksList()
        {
            foreach (NodeLink link in NodeLinks)
            {
                DestroyImmediate(link);
            }
            NodeLinks.Clear();
        }

        private void ClearNodeLinks(Node node)
        {
            foreach (NodeLink link in NodeLinks.FindAll(x => x.GUID.Equals(node.NodeGUID)))
            {
                DestroyImmediate(link);
            }
            NodeLinks.RemoveAll(x => x.GUID.Equals(node.NodeGUID));
        }

        private void ClearElementLinksList()
        {
            foreach (ElementLink link in ElementLinks)
            {
                DestroyImmediate(link);
            }
            ElementLinks.Clear();
        }

        private void ClearElementLinks(Element element)
        {
            foreach (ElementLink link in ElementLinks.FindAll(x => x.GUID.Equals(element.ElementGUID)))
            {
                DestroyImmediate(link);
            }
            ElementLinks.RemoveAll(x => x.GUID.Equals(element.ElementGUID));
        }

        private void ClearElementLinks(Node node)
        {
            foreach (ElementLink link in ElementLinks.FindAll(x => x.ParentGUID.Equals(node.NodeGUID)))
            {
                DestroyImmediate(link);
            }
            ElementLinks.RemoveAll(x => x.ParentGUID.Equals(node.NodeGUID));
        }

        //private void ClearConnectoidListOfElement(String _elementID)
        //{
        //    foreach (Connectoid connectoid in ConnectoidList.FindAll(x => x.Source == _elementID))
        //    {
        //        if (ConnectoidList.Count(x => x.Destination == connectoid.Destination) == 1)
        //        {
        //            NodeList.Find(x => x.NodeGUID == connectoid.Destination).Connected = false;
        //        }
        //    }
        //    ConnectoidList.RemoveAll(x => x.Source == _elementID);
        //}

        private void ClearConnectoidsOfNode(String _nodeGUID)
        {
            ClearConnectoidPaths(NodeList.Find(x => x.NodeGUID.Equals(_nodeGUID)));
            ConnectoidList.RemoveAll(x => x.Destination == _nodeGUID);
        }

        private void ClearConnectoidListOfElement(String _elementGUID)
        {
            ClearConnectoidPaths(ElementList.Find(x => x.ElementGUID.Equals(_elementGUID)));
            foreach (Connectoid connectoid in ConnectoidList.FindAll(x => x.Source.Equals(_elementGUID)))
            {
                if (ConnectoidList.Count(x => x.Destination == connectoid.Destination) == 1)
                {
                    NodeList.Find(x => x.NodeGUID == connectoid.Destination).Connected = false;
                }
                DestroyImmediate(connectoid);
            }
            ConnectoidList.RemoveAll(x => x.Source.Equals(_elementGUID));
        }

        private void ClearConnectoidPaths()
        {
            foreach (ConnectoidPath path in Paths)
            {
                path.Path.Clear();
                DestroyImmediate(path);
            }
            Paths.Clear();
        }

        private void ClearConnectoidPaths(Element element)
        {
            foreach (ConnectoidPath path in Paths.FindAll(x => x.ElementGUID.Equals(element.ElementGUID)))
            {
                path.Path.Clear();
                DestroyImmediate(path);
            }
            Paths.RemoveAll(x => x.ElementGUID.Equals(element.ElementGUID));
        }

        private void ClearConnectoidPaths(Node node)
        {
            foreach (ConnectoidPath path in Paths.FindAll(x => x.NodeLinkGUID.Equals(node.NodeGUID)))
            {
                path.Path.Clear();
                DestroyImmediate(path);
            }
            Paths.RemoveAll(x => x.NodeLinkGUID.Equals(node.NodeGUID));
        }

        private void ClearConnectoids()
        {
            foreach (Connectoid connectoid in ConnectoidList)
            {
                DestroyImmediate(connectoid);
            }
            ConnectoidList.Clear();
        }

        private void RemoveAllConnectoidsFinal()
        {
            ClearConnectoids();
            ConnectoidList = null;
        }

        public List<Node> GetNodeList()
        {
            return NodeList;
        }

        public List<ConnectoidPath> GetPaths()
        {
            return Paths;
        }

        public List<Element> GetElementList(String _parent)
        {
            return ElementList.FindAll(x => (x.ParentNodeGUID == _parent));
        }

        static void ConnectNodeProvider(Node _node, ref NodeProvider _provider)
        {
            if (_node != null && !_node.Subscribed)
            {
                _node.Subscribe(ref _provider);
            }
        }

        static void ConnectElementProvider(Element _element, ref ElementProvider _provider)
        {
            if (_element != null && !_element.Subscribed)
            {
                _element.Subscribe(ref _provider);
            }
        }
        static bool GuidCheck(string _guid)
        {
            if (string.IsNullOrEmpty(_guid) || !(new Regex(_guidFormat)).IsMatch(_guid))
            {
                return false;
            }
            return true;
        }

        private void AddLink(object sender, LinkerCompleteEventArg e)
        {
            LinkerCompleteEventArg _event = e;
            if (GuidCheck(_event.Source) && GuidCheck(_event.Destination))
            {
                Connectoid _links = CreateInstance<Connectoid>();
                _links.Source = _event.Source;
                _links.Destination = _event.Destination;
                ConnectoidList.Add(_links);

                //ConnectoidPath _path = new ConnectoidPath();
                ConnectoidPath _path = CreateInstance<ConnectoidPath>();
                _path.Path = CalculateConnectoidPath(_links, Vector2.zero, false);
                _path.ElementGUID = _links.Source;
                _path.NodeLinkGUID = _links.Destination;
                Paths.Add(_path);

                ElementList.Find(x => x.ElementGUID == _event.Source).ElementLinked = true;
                NodeList.Find(x => x.NodeGUID == _event.Destination).Connected = true;
            }
        }

        private void ValidateElementLinks()
        {
            foreach (Element element in ElementList)
            {
                //check if other connectoids are attached to the element before setting
                if (element.ElementLinked)
                {
                    int _count = ConnectoidList.FindAll(x => x.Source.Equals(element.ElementGUID)).Count();
                    if (_count <= 0) { element.ElementLinked = false; }
                }
                //ElementList.Find(x => x.ElementGUID.Equals(connectoid.Source)).ElementLinked = false;
            }
        }
        public Rect GenerateNodeBoundary(Node node)
        {
            Rect _boundary = new Rect();
            _boundary.Set(node.Position.x, node.Position.y, node.Size.x, node.Size.y + _zoneYOffset);
            return _boundary;
        }

        private Vector2 CalculateLinkNodeConnector(Node node)
        {
            return new Vector2(node.Position.x + node.HeaderArea.x + 1, node.Position.y + node.HeaderArea.yMax - 5f);
        }

        private Vector2 CalculateElementConnector(Node node, Element element)
        {
            return new Vector2(node.Position.x + node.HeaderArea.x + element.FrameArea.width - 5, node.Position.y + node.HeaderArea.yMax + element.FrameArea.y + ((element.Size.y) - 7));
        }

        //Rect(Position.width - 12, HeaderArea.height - 3, 5, 5)
        //Rect(Position.width - 12, FrameArea.height + 2, 5, 5),

        private RollupLocation CalculateElementLinkRelativeToElementArea(Node node, Vector2 position)
        {
            //float _elementY = node.Position.y + node.HeaderArea.yMax + element.FrameArea.y + ((element.Size.y) - 7);
            float _areaYMin = node.Position.y + node.HeaderArea.yMax + EditorStyles.toolbarButton.fixedHeight;
            float _areaYMax = node.Position.y + node.FrameArea.height + EditorStyles.toolbarButton.fixedHeight;
            if (position.y < _areaYMin)
            {
                //upper
                return RollupLocation.UPPER;
            }
            else
            {
                if (position.y > _areaYMax)
                {
                    //lower
                    return RollupLocation.LOWER;
                }
                else
                {
                    //center span
                    return RollupLocation.NONE;
                }
            }
        }

        //private List<Vector2> CalculateConnectoidPath(Connectoid _connectoidPair)
        private List<Vector2> CalculateConnectoidPath(Connectoid _connectoidPair, Vector2 mousePosition, bool _livePath)
        {
            List<Vector2> _path = new List<Vector2>();
            Vector2 _sCenter, _dCenter;
            int _pathIndex;
            float _pathOffset;
            Rect _dBounds, _sBounds;

            if (!_livePath)
            {
                Node _destination = NodeList.Find(x => x.NodeGUID == _connectoidPair.Destination);
                Element _source = ElementList.Find(x => x.ElementGUID == _connectoidPair.Source);
                if (_destination == null || _source == null)
                {
                    return _path;  //error
                }

                _dBounds = NodeBoundaries.Find(x => x.GUID == _destination.NodeGUID).Boundary;  //destination node boundaries
                _sBounds = NodeBoundaries.Find(x => x.GUID == _source.ParentNodeGUID).Boundary; //source element parent node boundaries
                _dCenter = NodeLinks.Find(x => x.GUID == _destination.NodeGUID).Center;      //destination link center


                if (_connectoidPair.Rollup.Equals(RollupLocation.NONE))
                {
                    _sCenter = ElementLinks.Find(x => x.GUID == _source.ElementGUID).Center;
                }
                else
                {
                    _sCenter = _connectoidPair.RollupCenter;
                }
                _pathIndex = Paths.Count(x => x.ElementGUID.Equals(_connectoidPair.Source));
                _pathIndex += Paths.Count(x => x.NodeLinkGUID.Equals(_connectoidPair.Destination));
                _pathOffset = _pathIndex * _zoneXOffset;
            }
            else
            {
                _dCenter = mousePosition;
                _sCenter = Linker.SourcePoint;
                _pathOffset = _zoneXOffset;
                _sBounds = NodeBoundaries.Find(x => x.GUID.Equals(NodeList.Find(y => y.NodeGUID.Equals(ElementList.Find(z => z.ElementGUID.Equals(Linker.SourceID)).ParentNodeGUID)).NodeGUID)).Boundary;
                _dBounds = new Rect(mousePosition.x - _zoneXOffset, mousePosition.y - (2 * _zoneXOffset), 40f, 60f);
                if (_sBounds.Overlaps(_dBounds))
                {
                    _dBounds.width = _sBounds.width;
                }
            }



            // (x)E(y) -> (x)N(y)  where ~(-/+) within a cross boundary (upper or lower)
            //
            _path.Add(_dCenter);

            if (_dBounds.x < _sBounds.x) // ?E -> ?N?
            {
                if (_dBounds.y < _sBounds.yMax) // E -> -N?
                {
                    if (_dBounds.yMax > _sBounds.yMax) // E -> -N~  (do lower path graph of N low boundary)
                    {
                        //check if d node below
                        if (_dBounds.y > _sBounds.yMax)
                        {
                            //do cross field
                            Rect _crossField = Rect.MinMaxRect(_dBounds.xMin, _dBounds.yMax, _sBounds.xMax, _sBounds.yMin);
                            _path.Add(new Vector2(_dBounds.xMin, _dCenter.y));
                            _path.Add(new Vector2(_dBounds.xMin, _crossField.center.y));
                            _path.Add(new Vector2(_sBounds.xMax, _crossField.center.y));
                            _path.Add(new Vector2(_sBounds.xMax, _sCenter.y));
                        }
                        else
                        { //within
                            _path.Add(new Vector2(_dBounds.xMin, _dCenter.y));
                            _path.Add(new Vector2(_dBounds.xMin, _dBounds.yMax));
                            _path.Add(new Vector2(_sBounds.xMax, _dBounds.yMax));
                            _path.Add(new Vector2(_sBounds.xMax, _sCenter.y));
                        }
                    }
                    else // E -> -N-
                    {
                        if (_dCenter.y > _sCenter.y) // E -> -N-~-
                        {
                            _path.Add(new Vector2(_dBounds.xMin, _dCenter.y));
                            _path.Add(new Vector2(_dBounds.xMin, _sBounds.yMax));
                            _path.Add(new Vector2(_sBounds.xMax, _sBounds.yMax));
                            _path.Add(new Vector2(_sBounds.xMax, _sCenter.y));
                        }
                        else // E -> -N-~?
                        {
                            if (_dBounds.y < _sBounds.y) // (do upper path graph of upper N boundary)
                            {
                                //check crossfield
                                if (_dBounds.yMax < _sBounds.y)
                                {
                                    Rect _crossField = Rect.MinMaxRect(_dBounds.xMin, _dBounds.yMax, _sBounds.xMax, _sBounds.yMin);
                                    _path.Add(new Vector2(_dBounds.xMin, _dCenter.y));
                                    _path.Add(new Vector2(_dBounds.xMin, _crossField.center.y));
                                    _path.Add(new Vector2(_sBounds.xMax, _crossField.center.y));
                                    _path.Add(new Vector2(_sBounds.xMax, _sCenter.y));
                                }
                                else
                                {
                                    _path.Add(new Vector2(_dBounds.xMin, _dCenter.y));
                                    _path.Add(new Vector2(_dBounds.xMin, _dBounds.yMin));
                                    _path.Add(new Vector2(_sBounds.xMax, _dBounds.yMin));
                                    _path.Add(new Vector2(_sBounds.xMax, _sCenter.y));
                                }
                            }
                            else // (do upper path graph of upper E boundary)
                            {
                                _path.Add(new Vector2(_dBounds.xMin, _dCenter.y));
                                _path.Add(new Vector2(_dBounds.xMin, _sBounds.yMin));
                                _path.Add(new Vector2(_sBounds.xMax, _sBounds.yMin));
                                _path.Add(new Vector2(_sBounds.xMax, _sCenter.y));
                            }
                        }
                    }
                }
                else // E -> -N? (do center graph of field between lower bound E and upper bound N)
                {
                    //check centers
                    Rect _crossField = Rect.MinMaxRect(_sBounds.xMin, _sBounds.yMax, _dBounds.xMax, _dBounds.yMin);
                    _path.Add(new Vector2(_dBounds.xMin, _dCenter.y));
                    _path.Add(new Vector2(_dBounds.xMin, _crossField.center.y));
                    _path.Add(new Vector2(_sBounds.xMax, _crossField.center.y));
                    _path.Add(new Vector2(_sBounds.xMax, _sCenter.y));
                }
            }
            else  // -E -> +N?
            {
                if (_dBounds.x < _sBounds.xMax) //link node is within vertical space
                {
                    //check if link node is above or below element
                    if (_dCenter.y < _sCenter.y) //above
                    {
                        if (_dBounds.yMax > _sBounds.y)
                        {
                            //node is within upper area
                            _path.Add(new Vector2(_dBounds.xMin, _dCenter.y));
                            _path.Add(new Vector2(_dBounds.xMin, _dBounds.yMin));
                            _path.Add(new Vector2(_dBounds.xMax, _dBounds.yMin));
                            _path.Add(new Vector2(_dBounds.xMax, _sCenter.y));
                        }
                        else
                        {
                            //node is above
                            Rect _crossField = Rect.MinMaxRect(_dBounds.xMin, _dBounds.yMax, _sBounds.xMax, _sBounds.yMin);
                            _path.Add(new Vector2(_dBounds.xMin, _dCenter.y));
                            _path.Add(new Vector2(_dBounds.xMin, _crossField.center.y));
                            _path.Add(new Vector2(_sBounds.xMax, _crossField.center.y));
                            _path.Add(new Vector2(_sBounds.xMax, _sCenter.y));
                        }

                    }
                    else //below
                    {
                        if (_dBounds.y > _sBounds.yMax) //node is outside lower limits
                        {
                            Rect _crossField = Rect.MinMaxRect(_sBounds.xMin, _sBounds.yMax, _dBounds.xMax, _dBounds.yMin);
                            _path.Add(new Vector2(_dBounds.xMin, _dCenter.y));
                            _path.Add(new Vector2(_dBounds.xMin, _crossField.center.y));
                            _path.Add(new Vector2(_sBounds.xMax, _crossField.center.y));
                            _path.Add(new Vector2(_sBounds.xMax, _sCenter.y));
                        }
                        else
                        {
                            _path.Add(new Vector2(_dBounds.xMin, _dCenter.y));
                            _path.Add(new Vector2(_dBounds.xMin, _dBounds.yMax));
                            _path.Add(new Vector2(_dBounds.xMax, _dBounds.yMax));
                            _path.Add(new Vector2(_dBounds.xMax, _sCenter.y));
                        }
                    }
                }
                else
                {
                    _path.Add(new Vector2(_sBounds.xMax + _pathOffset, _dCenter.y));
                    _path.Add(new Vector2(_sBounds.xMax + _pathOffset, _sCenter.y));
                }

            }

            //if (!_connectoidPair.Rollup.Equals(RollupLocation.NONE))
            //{
            //    _path.Add(_connectoidPair.RollupCenter);
            //}
            //else
            //{
            _path.Add(_sCenter);
            //}

            return _path;
        }
        internal List<ConnectoidPath> GenerateLivePath(Vector2 mousePosition)
        {
            ConnectoidPath _path = CreateInstance<ConnectoidPath>();
            _path.Path = CalculateConnectoidPath(null, mousePosition, true);
            _path.hideFlags = HideFlags.DontSave;
            LivePath.Add(_path);

            return LivePath;
        }

        internal void ReleaseLivePath()
        {
            foreach (ConnectoidPath path in LivePath)
            {
                path.Path.Clear();
                DestroyImmediate(path);
            }
            LivePath.Clear();
        }
    }


    public class DataPropertyChangeEventArgs : EventArgs
    {
        public String Name { get; set; }
        public bool State { get; set; }


    }



    [Serializable]
    class Connectoid : ScriptableObject
    {
        [SerializeField]
        public String Source { get; set; }

        [SerializeField]
        public String Destination { get; set; }

        [SerializeField]
        public bool Active { get; set; }

        [SerializeField]
        public RollupLocation Rollup { get; set; }

        [SerializeField]
        public Vector2 RollupCenter { get; set; }

        void OnEnable()
        {
            hideFlags = HideFlags.HideAndDontSave;
        }
    }

    [Serializable]
    class NodeBoundary : ScriptableObject
    {
        public String GUID { get; set; }
        public Rect Boundary { get; set; }

        void OnEnable()
        {
            hideFlags = HideFlags.HideAndDontSave;
        }
    }

    [Serializable]
    class ElementLink : ScriptableObject
    {
        public String GUID { get; set; }
        public String ParentGUID { get; set; }
        public Vector2 Center { get; set; }

        void OnEnable()
        {
            hideFlags = HideFlags.HideAndDontSave;
        }
    }

    [Serializable]
    class NodeLink : ScriptableObject
    {
        public String GUID { get; set; }
        public Vector2 Center { get; set; }

        void OnEnable()
        {
            hideFlags = HideFlags.HideAndDontSave;
        }
    }

    [Serializable]
    public class ConnectoidPath : ScriptableObject
    {
        public List<Vector2> Path { get; set; }

        public List<Rect> Frames { get; set; }

        public String ElementGUID { get; set; }

        public String NodeLinkGUID { get; set; }

        public bool Active { get; set; }

        //~ConnectoidPath()
        //{
        //    Path = null;
        //    Frames = null;
        //}
        void OnDisable()
        {
            Path.Clear();
            Path = null;
            Frames.Clear();
            Frames = null;
        }

        void OnEnable()
        {
            hideFlags = HideFlags.HideAndDontSave;
            if (Path == null)
            {
                Path = new List<Vector2>();
            }
            if (Frames == null)
            {
                Frames = new List<Rect>();
            }
        }
        //public ConnectoidPath()
        //{
        //    if (Path == null)
        //    {
        //        Path = new List<Vector2>();
        //    }
        //}
    }

    public enum RollupLocation
    {
        NONE,
        UPPER,
        LOWER
    }
}