﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using Assets.SceneBuilder.Editor.Utilities;
using Assets.SceneBuilder.Editor.Providers;

namespace Assets.SceneBuilder.Editor.Panels
{
    [Serializable]
    public class Panel : ScriptableObject, IObserver<PanelMessageSignature, PanelMessageData>
    {
        //public static GUISkinSSB Skin;
        private static int _panelFocusID;
        private static int _panelCount;
        private int _panelID;

        #region declarations

        [SerializeField]
        private Vector2 _panelSize, _frameStateSize, _staticPosition;

        [SerializeField]
        private Rect _position, _frameArea;

        [SerializeField]
        private bool _frameStateChanged, _staticPositionSet;

        [NonSerialized]
        private IDisposable unsubscriber;


        // LockState: accessor for lockstate
        [SerializeField]
        public bool LockState { get; protected set; }


        // FoldoutActive: accessor for foldout state
        [SerializeField]
        public bool FoldoutActive { get; protected set; }

        // Position: Node accessor for position on artboard
        public virtual Rect Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }

        // Size: size accessor (note: setting adjusts Position property for width & height)
        public virtual Vector2 Size
        {
            get { return _panelSize; }
            protected set
            {
                _panelSize = value;
                _position.Set(_position.x, _position.y, _panelSize.x, _panelSize.y);
            }
        }

        // MinMaxSize: Node min/max (property configured at Node instantiation)
        [SerializeField]
        public Vector2 MinMaxSize { get; protected set; }

        // ResizeVertical: Node accessor to adjust vertical size of node and framearea
        public float ResizeVertical
        {
            get { return _panelSize.y; }
            protected set
            {
                if ((_panelSize.y + value) < MinMaxSize.y)
                {
                    return;
                }
                _panelSize.y += value;
                _position.height += value;
                _frameArea.height += value;
            }
        }

        public float ResizePanelY
        {
            get { return _panelSize.y; }
            protected set
            {
                _position.yMax += value;
                _frameArea.yMax += value;
                _panelSize.y += value;
            }
        }

        // Sticky: accessor for "sticky" position value
        [SerializeField]
        public Vector2 Sticky { get; protected set; }

        public virtual Vector2 StaticPosition
        {
            get { return _staticPosition; }
            protected set { _staticPosition = value; }
        }

        public Vector2 SetStaticPosition
        {
            set 
            {
                _staticPositionSet = true;
                _staticPosition = value;
            }
        }

        public bool StaticPositionLocked
        {
            get { return _staticPositionSet; }
            set { _staticPositionSet = value; }
        }

        // Translate: Node access to adjust node position values (x,y)
        public virtual Vector2 Translate
        {
            set
            {
                _position.x = _staticPosition.x + value.x;
                _position.y = _staticPosition.y + value.y;
            }
        }

        // Title: Node accessor for title property
        [SerializeField]
        public string Title { get; set; }

        // nodeID: Node accessor for assigned Unity controlID (may not be actively updated in playmode state changes)
        //[SerializeField]
        //public int PanelControlID { get; protected set; }

        // NodeWindowID: Node accessor for window property drawing identifier
        [SerializeField]
        public int PanelID 
        {
            get { return _panelID; }
            protected set { _panelID = value; } 
        }

        public int PanelFocusID
        {
            get { return _panelFocusID; }
            protected set { _panelFocusID = value; }
        }

        [SerializeField]
        public String PanelGUID { get; protected set; }
        // FrameArea: access for the Rect value of inner Node window region (full area, includes foldout)
        public Rect FrameArea
        {
            get { return _frameArea; }
            protected set { _frameArea = value; }
        }

        public float FrameWidth
        {
            get { return _frameArea.width; }
            protected set { _frameArea.width = value; }
        }

        public float FrameHeight
        {
            get { return _frameArea.height; }
            protected set { _frameArea.height = value; }
        }

        // FoldoutArea: accessor for the Rect value of the foldout area when foldout is active
        [SerializeField]
        public Rect FoldoutArea { get; protected set; }

        // HeaderArea: accessor for the region considered the Node Header area (based on instantiated node size)
        [SerializeField]
        public Rect HeaderArea { get; protected set; }

        // ElementArea: accessor for the region within the foldout area (resizable)
        [SerializeField]
        public Rect ElementArea { get; protected set; }

        // ElementAreaPositions: accessor for region occupied by elements
        [SerializeField]
        public Rect ElementAreaPositions { get; protected set; }

        // GetStyle: accessor for the GUISkin style of this Node
        [SerializeField]
        public GUIStyle GetStyle { get; protected set; }

        // FrameBorderStyle: accessor for gui style of node
        public SSBStyles FrameBorderStyle { get; protected set; }

        // CursorChange: accessor for cursor state changes (perhaps consider expanding to include the cursor type)
        [SerializeField]
        public bool CursorChange { get; protected set; }

        // FrameStateSize: accessor to get a snapshot of the frame area size and flag the change
        protected Vector2 FrameStateSize
        {
            get { return _frameStateSize; }
            set
            {
                //snapshot node size
                _frameStateSize = value;
                FrameStateChanged = true;
            }
        }

        // FrameStateRestore: accessor to flag that FrameStateChanges occured and foldouts can restore a previous size
        [SerializeField]
        protected bool FrameStateRestore { get; set; }
        [SerializeField]
        protected bool FrameStateChanged { get; set; }

        [SerializeField]
        protected bool IsDraggable { get; set; }

        [SerializeField]
        protected bool FoldoutControlActivated { get; set; }

        [SerializeField]
        protected bool FoldoutResizing { get; set; }

        [SerializeField]
        protected int FoldoutControlID { get; set; }

        [SerializeField]
        public bool PanelSelected { get; protected set; }

        private SSBStyles _styleModeLabel;

        #endregion declarations

        //
        //Methods
        //

        ~Panel() //Destructor
        {
            Debug.Log("Panel Destructor called:" + PanelID);
        }

        public bool Subscribed
        {
            get { return unsubscriber == null ? false : true; }
        }

        void OnDestroy()
        {
            //Debug.Log("Base Node OnDestroy() called:" + nodeID);
        }

        void OnDisable()
        {
            //Skin = null;
        }

        public virtual void OnEnable() //required
        {
            hideFlags = HideFlags.HideAndDontSave;
        }

        // OnCompleted: Message Provider handler for termination of service from provider
        public virtual void OnCompleted(PanelMessageSignature signature, PanelMessageData value)
        {
            if (signature == PanelMessageSignature.Exit)
            {
                Debug.Log("Panel Provider Stopping, Exit Condition; node report:" + PanelID);
            }
        }

        // OnError: Message Provider has dispatched an error condition to node
        public void OnError(Exception exception)
        {
            Debug.Log("Provider Error Message:" + exception.ToString());
        }

        // OnNext: Message Provider general message handler
        public virtual void OnNext(PanelMessageSignature signature, PanelMessageData value)
        {
            //ignore translates for all panels here!
        }

        // Subscribe: creates reference to a message provider.
        public void Subscribe(IObservable<PanelMessageSignature, PanelMessageData> provider)
        {
            if (provider != null)
            {
                unsubscriber = provider.Subscribe(this);
            }
        }

        // Unsubscribe: remove provider subscription.
        public void Unsubscribe()
        {
            unsubscriber.Dispose();
            unsubscriber = null;
        }

        // DrawNodeWindow generates window content of Node
        public virtual void DrawPanelWindow(int id)
        {
            //PanelID = id;  //store ID for focus control
            //if (FrameArea.Contains(Event.current.mousePosition) && (Event.current.button == 1))
            //{
            //    GUI.FocusWindow(id);
            //}

            //if (GUIUtility.hotControl == 0)  //required to switch flag for resize detection
            //{
            //    FoldoutControlActivated = false;
            //}

            //FrameBorderStyle = PanelFocusID.Equals(PanelID) ? FrameBorderStyle : SSBStyles.FrameBaseBorder;
            //GUI.Box(new Rect(HeaderArea.x + 1, HeaderArea.y + 1, HeaderArea.width - 2, HeaderArea.height - 1), GUIContent.none, GUISkinSSB.SkinSSB.customStyles[GUISkinSSB.SSBStyle(FrameBorderStyle)]); //border line

            ////Foldout size / state switching checks
            //if (FoldoutActive && FrameStateRestore) //restore node size from previous state
            //{
            //    FrameStateRestore = false;
            //    Size = FrameStateSize;
            //    FrameHeight = FrameStateSize.y - 14;
            //}
            //if (!FoldoutActive) //default node size when foldout is not active
            //{
            //    Size = MinMaxSize;
            //    FrameHeight = MinMaxSize.y - 14;
            //    FrameStateRestore = true;
            //}
            //if (FoldoutActive && FrameStateChanged) //capture current frame state size
            //{
            //    FrameStateChanged = false;
            //    FrameStateSize = Size;
            //}

            GUILayout.BeginArea(FrameArea);
            {
                //GUI.Label(new Rect((FrameArea.center.x - 25), 0, FrameArea.width - 4, 15), Title, GUISkinSSB.SkinSSB.label); //Title
                //GUI.Box(new Rect(4, 19, FrameArea.xMax - 8, FrameArea.yMax - 28), GUIContent.none, GUISkinSSB.SkinSSB.customStyles[GUISkinSSB.SSBStyle(SSBStyles.RedBox)]);  (corrected frame area for buttons, header, etc...)
            }
            GUILayout.EndArea();
            HandleEvents(Event.current);
        }

        // HandleEvents Method: Virtual method to handle basic events
        protected virtual void HandleEvents(Event e)
        {
            if (FrameArea.Contains(e.mousePosition) && e.rawType == EventType.MouseDown)
            {
                GUI.UnfocusWindow();
                PanelFocusID = PanelID;
                GUI.FocusWindow(PanelFocusID);
                e.Use();
            }
        }

        // ContextCallback Method: Virtual Method to handle "context clicks" in node frame
        public virtual void ContextCallback(object userData)
        {
            Debug.Log("panel:" + PanelID + ", context item selected:" + userData);
        }

        // Initialize: base initialization for node
        public virtual void Initialize(Vector2 _location, string _title, Vector2 _size)
        {
            PanelGUID = Guid.NewGuid().ToString();
            _panelCount++;
            _panelID = _panelCount;
            //PanelControlID = GUIUtility.GetControlID(FocusType.Native);

            Size = _size;
            MinMaxSize = _size;
            _staticPosition = _location;
            _position = new Rect(_location.x, _location.y, _size.x, _size.y);
            FrameArea = new Rect(0, 0, _size.x, _size.y); //may need adjusting
            Sticky = new Vector2(_location.x, _location.y);
            _staticPositionSet = false;
            GetStyle = GUISkinSSB.SkinSSB.window; //_skin.SkinSSB.window;
            FrameBorderStyle = SSBStyles.AnchorNodeFBB;
        }
    }
}
