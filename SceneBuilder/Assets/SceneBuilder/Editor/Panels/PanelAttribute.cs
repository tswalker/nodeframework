﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Assets.SceneBuilder.Editor.Panels
{
    [SerializableAttribute]
    [AttributeUsage(AttributeTargets.Class)]
    class PanelAttribute : Attribute
    {
        public Type PanelType { get; private set; }

        public string DisplayName { get; private set; }

        public PanelCategory Category { get; private set; }

        public string Descriptor { get; private set; }

        public PanelAttribute(string _displayName, PanelCategory _category, Type _panelType)
        {
            DisplayName = _displayName;
            PanelType = _panelType;
            Category = _category;
        }
    }

    public enum PanelCategory
    {
        TOOL,
        INSPECTOR
    }
}

