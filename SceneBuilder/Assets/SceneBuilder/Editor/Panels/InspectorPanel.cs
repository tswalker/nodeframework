﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using Assets.SceneBuilder.Editor.Utilities;
using Assets.SceneBuilder.Editor.Providers;

namespace Assets.SceneBuilder.Editor.Panels
{
    [Serializable]
    [PanelAttribute("Inspector", PanelCategory.INSPECTOR, typeof(InspectorPanel))]
    sealed class InspectorPanel : Panel
    {
        //[SerializeField]
        //private Rect _position;
        //[SerializeField]
        //private Vector2 _panelSize, _staticPosition;

        // OnNext: Message Provider general message handler
        public override void OnNext(PanelMessageSignature signature, PanelMessageData value)
        {
            switch (signature)
            {
                case PanelMessageSignature.Translate:
                    {
                        Translate = value.Translate;
                        break;
                    }
            }
        }

        protected override void HandleEvents(Event e)
        {
            base.HandleEvents(e);
            //detect context click (right mouse click) in node
            if (FrameArea.Contains(e.mousePosition) && (e.type == EventType.ContextClick))
            {
                //do nothing & consume event
                Debug.Log("context click in inspector.");
                e.Use();
            }
        }

        public override void Initialize(Vector2 _location, string _title, Vector2 _size)
        {
            base.Initialize(_location, _title, _size);

            IsDraggable = false;
            LockState = true;
            FoldoutActive = false;
            Title = _title;
        }
    }
}
