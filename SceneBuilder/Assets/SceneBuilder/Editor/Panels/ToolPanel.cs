﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using Assets.SceneBuilder.Editor.Utilities;
using Assets.SceneBuilder.Editor.Providers;

namespace Assets.SceneBuilder.Editor.Panels
{
    [Serializable]
    [PanelAttribute("ToolBox", PanelCategory.TOOL, typeof(ToolPanel))]
    sealed class ToolPanel : Panel
    {
        protected override void HandleEvents(Event e)
        {
            base.HandleEvents(e);
            //detect context click (right mouse click) in node
            if (FrameArea.Contains(e.mousePosition) && (e.type == EventType.ContextClick))
            {
                //do nothing & consume event
                Debug.Log("context click in toolbox.");
                e.Use();
            }
        }

        public override void Initialize(Vector2 _location, string _title, Vector2 _size)
        {
            base.Initialize(_location, _title, _size);

            IsDraggable = false;
            LockState = true;
            FoldoutActive = false;
            Title = _title;
        }
    }
}
