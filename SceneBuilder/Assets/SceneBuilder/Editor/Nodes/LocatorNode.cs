﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using Assets.SceneBuilder.Editor.Utilities;
using Assets.SceneBuilder.Editor.Providers;
using Assets.SceneBuilder.Editor.Elements;

namespace Assets.SceneBuilder.Editor.Nodes
{
    [Serializable]
    [NodeAttribute("Locators", NodeCategory.LOCATORS, typeof(LocatorNode))]
    sealed class LocatorNode : Node
    {
        //Vector4 _boundaries;
        //float _frameOffsetY, _scrollValueV; //_frameOffsetX,  _scrollValueH, 

        public override void OnCompleted(NodeMessageSignature signature, NodeMessageData value)
        {
            base.OnCompleted(signature, value);
        }

        public override void OnNext(NodeMessageSignature signature, NodeMessageData value)
        {
            base.OnNext(signature, value);
        }

        public override void DrawNodeWindow(int id)
        {
            base.DrawNodeWindow(id);

            //LinkConnectoid
            GUILayout.BeginArea(LinkToArea);
            {
                //GUI.Box(new Rect(0,0, 10,10), GUIContent.none, GUISkinSSB.SkinSSB.customStyles[GUISkinSSB.SSBStyle(SSBStyles.RedBox)]);
            }
            GUILayout.EndArea();

            ElementAreaPositions = Utilities.CalculateElementAreaPositions(DAC.GetElementList(NodeGUID));

            ////pathway rollups
            //UpperRollupStyle = RollupUpperActive ? SSBStyles.PathwayRollup : SSBStyles.PathwayRollupEmpty;
            //LowerRollupStyle = RollupLowerActive ? SSBStyles.PathwayRollup : SSBStyles.PathwayRollupEmpty;
            //GUI.Box(new Rect(Position.width - 12, HeaderArea.height - 3, 5, 5), GUIContent.none, _skin.SkinSSB.customStyles[GUISkinSSB.SSBStyle(UpperRollupStyle)]);
            //GUI.Box(new Rect(Position.width - 12, FrameArea.height + 2, 5, 5), GUIContent.none, _skin.SkinSSB.customStyles[GUISkinSSB.SSBStyle(LowerRollupStyle)]);


            FoldoutActive = GUI.Toggle(new Rect(HeaderArea.center.x - 4, HeaderArea.yMax - 11, 9, 9), FoldoutActive, GUIContent.none, GUISkinSSB.SkinSSB.customStyles[GUISkinSSB.SSBStyle(FoldoutStyle)]);
            if (FoldoutActive)
            {
                ElementArea = new Rect(HeaderArea.x + 1, HeaderArea.yMax, FrameArea.width - 1, Size.y - MinMaxSize.y - 5);
                GetStyle = Connected ? GUISkinSSB.SkinSSB.customStyles[GUISkinSSB.SSBStyle(SSBStyles.LinkedNodeExpanded)] : GUISkinSSB.SkinSSB.customStyles[GUISkinSSB.SSBStyle(SSBStyles.LinkNodeExpanded)];
                GUILayout.BeginArea(ElementArea);
                {
                    int _index = 1;
                    //foreach (Element _element in ElementList)
                    foreach (Element _element in DAC.GetElementList(NodeGUID))
                    {
                        //_element.DrawElementWindow(_index, _scrollValueV, _frameOffsetX);
                        _element.DrawElementWindow(_index, ScrollValueVertical);
                        _index++;
                    }
                }
                GUILayout.EndArea();
                FoldoutArea = new Rect(15, FrameArea.yMax, FrameArea.width, 12);
            }
            else
            {
                FoldoutArea = new Rect();
                GetStyle = Connected ? GUISkinSSB.SkinSSB.customStyles[GUISkinSSB.SSBStyle(SSBStyles.LinkedNode)] : GUISkinSSB.SkinSSB.customStyles[GUISkinSSB.SSBStyle(SSBStyles.LinkNode)];
            }

            //if (ElementList.Count() > 0 && FoldoutActive)
            if (DAC.GetElementList(NodeGUID).Count() > 0 && FoldoutActive)
            {
                Utilities.CustomVerticalScrollBar(ElementAreaPositions, ElementArea);
            }

            HandleEvents(Event.current);
            if (IsDraggable)
            {
                GUI.DragWindow();
            }
        }

        protected override void HandleEvents(Event e)
        {
            base.HandleEvents(e);

            //linkto area detection
            if (LinkToArea.Contains(e.mousePosition) && e.rawType == EventType.MouseDown)
            {
                //Debug.Log("mouse down in linkto area.");
                FrameBorderStyle = FrameBorderStyleOnNormal;
                LockState = false;
                IsDraggable = true;
                FocusID = NodeWindowID;
                Vector2 _contactPoint = new Vector2(Position.x + HeaderArea.x + 1, Position.y + HeaderArea.yMax -5f);
                Linker.CloseLink(NodeGUID, _contactPoint);
                e.Use();
            }

            if (LinkToArea.Contains(e.mousePosition) && e.rawType == EventType.MouseUp)
            {
                //Debug.Log("mouse up in linkto area.");
                e.Use();
            }

            //detect context click (right mouse click) in node
            if (FrameArea.Contains(e.mousePosition) && e.rawType == EventType.ContextClick)
            {
                LockState = true;
                IsDraggable = false;
                elementProvider.Notify(ElementMessageSignature.Focus, new ElementMessageData(ActionTarget.ON));
                //Utilities.BuildNodeContextMenu(Callback, ElementCategory.LOCATOR, ElementList);
                Utilities.BuildNodeContextMenu(Callback, ElementCategory.LOCATOR, DAC.GetElementList(NodeGUID));
                e.Use();
            }
        }

        private void Callback(object userData)
        {
            ActionCommand _command = userData as ActionCommand;
            if (_command == null) { return; } //critical, "as" may return null if type is not of ActionCommand

            switch (_command.ID)
            {
                case ActionIdentifier.ADD:
                    {
                        Element _element = CreateInstance(_command.ElementAttribute.ElementType.Name) as Element;
                        _element.Initialize(NodeGUID,new Vector2(0, ElementAreaPositions.yMax), _command.ElementAttribute.SubCategory.ToString(), new Vector2(96, 48));
                        DAC.AddElement(NodeGUID,_element);
                        SetElementProvider(_element.ElementGUID);
                        break;
                    }
                case ActionIdentifier.REMOVE:
                    {
                        if (_command.Target == ActionTarget.ALL)
                        {
                            elementProvider.Notify(ElementMessageSignature.Removed, new ElementMessageData(ActionTarget.ALL)); //notify selected nodes of removal
                            DAC.RemoveAllNodeElements(NodeGUID);
                        }
                        if (_command.Target == ActionTarget.SELECTED)
                        {
                            elementProvider.Notify(ElementMessageSignature.Removed, new ElementMessageData(ActionTarget.SELECTED));
                            DAC.RemoveSelectedNodeElements(NodeGUID);
                        }
                        break;
                    }
            }
        }

        public override void OnEnable()
        {
            base.OnEnable();
        }

        public override void Initialize(Vector2 _location, string _title, Vector2 _size)
        {
            base.Initialize(_location, _title, _size);

            //Size = _size;
            //MinMaxSize = _size;

            //Position = new Rect(_location.x, _location.y, _size.x, _size.y);
            //FrameArea = new Rect(15, 3, _size.x - 26, _size.y - 14);
            //FoldoutArea = new Rect();
            //ElementArea = new Rect();
            //HeaderArea = new Rect(FrameArea);
            //StaticPosition = new Vector2(_location.x, _location.y);

            LinkToArea = new Rect(HeaderArea.x - 5, HeaderArea.center.y - 5, 10, 10);

            LockState = false;
            FoldoutActive = false;
            FrameStateRestore = false;
            FrameStateSize = _size;

            Title = _title;
            NodeControlID = GUIUtility.GetControlID(FocusType.Passive);
            GetStyle = GUISkinSSB.SkinSSB.customStyles[GUISkinSSB.SSBStyle(SSBStyles.LinkNode)];
            CursorChange = false;
            ShowSelectedNodeIcon = true;

            FrameBorderStyleNormal = SSBStyles.LinkNodeFBB;
            FrameBorderStyleOnNormal = SSBStyles.LinkNodeFBBON;
            FrameBorderStyle = FrameBorderStyleNormal;

            Connectable = true;
        }

    }
}
