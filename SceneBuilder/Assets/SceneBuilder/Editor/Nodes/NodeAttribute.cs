﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Assets.SceneBuilder.Editor.Nodes
{
    [SerializableAttribute]
    [AttributeUsage(AttributeTargets.Class)]
    public class NodeAttribute : Attribute
    {
        private string _displayName;
        private NodeCategory _category;
        private string _descriptor = "";
        private Type _nodeType;

        public Type NodeType
        {
            get { return _nodeType; }
        }

        public string DisplayName
        {
            get { return _displayName; }
        }

        public NodeCategory Category
        {
            get { return _category; }
        }

        public string Descriptor
        {
            get { return _descriptor; }
        }

        public NodeAttribute(string displayName, NodeCategory category, Type nodeType)
        {
            _displayName = displayName;
            _nodeType = nodeType;
            _category = category;
        }
    }

    public enum NodeCategory
    {
        TOOL,
        SCENE,
        LOCATORS
    }
}
