﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using Assets.SceneBuilder.Editor.Utilities;
using Assets.SceneBuilder.Editor.Providers;
using Assets.SceneBuilder.Editor.Elements;
using Assets.SceneBuilder.Editor.Data;

namespace Assets.SceneBuilder.Editor.Nodes
{
    [Serializable]
    public class Node : ScriptableObject, IObserver<NodeMessageSignature, NodeMessageData>, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private DataController _dac;
        public DataController DAC { get { return _dac; } set { _dac = value; } }
        private ElementProvider _elementProvider;
        //public GUISkinSSB _skin;
        private static int _focusID;
        private bool NodeAreaChanged { get; set; }
        private bool NodeAreaChangeComplete { get; set; }

        private bool _rollupUpperActive, _rollupLowerActive;

        [SerializeField]
        public bool RollupUpperActive
        {
            get { return _rollupUpperActive; }
            set
            {
                _rollupUpperActive = value;
                //Debug.Log("node upper active: "+value); 
            }
        }

        [SerializeField]
        public bool RollupLowerActive
        {
            get { return _rollupLowerActive; }
            set
            {
                _rollupLowerActive = value;
                //Debug.Log("node lower active: " + value); 
            }
        }


        protected ElementProvider elementProvider
        {
            get { return _elementProvider; }
        }

        #region declarations

        protected NodeUtilities Utilities;

        [SerializeField]
        private Vector2 _nodeSize, _frameStateSize, _staticPosition;

        [SerializeField]
        private Rect _position, _frameArea, _upperRollupArea, _lowerRollupArea;

        [SerializeField]
        private Vector2 _upperRollupCenter, _lowerRollupCenter;

        private bool _frameStateChanged, _cursorState, _eventStateChanged;

        [SerializeField]
        public Vector2 UpperRollupCenter
        {
            get { return _upperRollupCenter; }
            set { _upperRollupCenter = value; }
        }

        [SerializeField]
        public Vector2 LowerRollupCenter
        {
            get { return _lowerRollupCenter;}
            set { _lowerRollupCenter = value; }
        }

        [SerializeField]
        public Rect UpperRollupArea
        {
            get { return _upperRollupArea; }
            protected set { _upperRollupArea = value; }
        }

        [SerializeField]
        public Rect LowerRollupArea
        {
            get { return _lowerRollupArea; }
            protected set { _lowerRollupArea = value; }
        }

        [SerializeField]
        public float ScrollValueVertical { get; set; }

        [SerializeField]
        public float FrameOffsetVertical { get; set; }

        [NonSerialized]
        private IDisposable unsubscriber;

        [SerializeField]
        public bool Connectable { get; protected set; }

        [SerializeField]
        public bool Connected { get; set; }

        // LockState: accessor for lockstate
        [SerializeField]
        public bool LockState { get; protected set; }


        // FoldoutActive: accessor for foldout state
        [SerializeField]
        public bool FoldoutActive { get; protected set; }

        // Position: Node accessor for position on artboard
        public Rect Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;

                if (NodeAreaChanged && NodeAreaChangeComplete)
                {
                    NodeAreaChanged = false;
                    //NotifyPropertyChanged("NodeAreaChanged");
                    NotifyPropertyChanged("NodeAreaChanging");
                }
                if (NodeAreaChanged && !NodeAreaChangeComplete)
                {
                    NotifyPropertyChanged("NodeAreaChanging");
                }
            }
        }

        // Size: Node size accessor (note: setting adjusts Position property for width & height)
        public Vector2 Size
        {
            get { return _nodeSize; }
            protected set
            {
                _nodeSize = value;
                _position.Set(_position.x, _position.y, _nodeSize.x, _nodeSize.y);
            }
        }

        // MinMaxSize: Node min/max (property configured at Node instantiation)
        [SerializeField]
        public Vector2 MinMaxSize { get; protected set; }

        // ResizeVertical: Node accessor to adjust vertical size of node and framearea
        public float ResizeVertical
        {
            get { return _nodeSize.y; }
            protected set
            {
                if ((_nodeSize.y + value) < MinMaxSize.y)
                {
                    return;
                }
                _nodeSize.y += value;
                _position.height += value;
                _frameArea.height += value;
                NotifyPropertyChanged("NodeFoldoutChanged");
            }
        }

        // Sticky: Node accessor for "sticky" position value
        [SerializeField]
        public Vector2 Sticky { get; protected set; }

        // Translate: Node access to adjust node position values (x,y)
        public Vector2 Translate
        {
            set
            {
                _position.x = _staticPosition.x + value.x;
                _position.y = _staticPosition.y + value.y;
                NotifyPropertyChanged("NodeAreaChanging");
            }
        }

        // Title: Node accessor for title property
        [SerializeField]
        public string Title { get; set; }

        // nodeID: Node accessor for assigned Unity controlID (may not be actively updated in playmode state changes)
        [SerializeField]
        public int NodeControlID { get; protected set; }

        // NodeWindowID: Node accessor for window property drawing identifier
        [SerializeField]
        public int NodeWindowID { get; set; }

        // FrameArea: access for the Rect value of inner Node window region (full area, includes foldout)
        public Rect FrameArea
        {
            get { return _frameArea; }
            protected set { _frameArea = value; }
        }

        public float FrameWidth
        {
            get { return _frameArea.width; }
            protected set { _frameArea.width = value; }
        }

        public float FrameHeight
        {
            get { return _frameArea.height; }
            protected set { _frameArea.height = value; }
        }

        // FoldoutArea: accessor for the Rect value of the foldout area when foldout is active
        [SerializeField]
        public Rect FoldoutArea { get; protected set; }

        // HeaderArea: accessor for the region considered the Node Header area (based on instantiated node size)
        [SerializeField]
        public Rect HeaderArea { get; protected set; }

        // ElementArea: accessor for the region within the foldout area (resizable)
        [SerializeField]
        public Rect ElementArea { get; protected set; }

        // ElementAreaPositions: accessor for region occupied by elements
        [SerializeField]
        public Rect ElementAreaPositions { get; protected set; }

        // LinkToArea: accessor of region for connectoid links "to" for link nodes
        [SerializeField]
        public Rect LinkToArea { get; protected set; }

        // GetStyle: accessor for the GUISkin style of this Node
        [SerializeField]
        public GUIStyle GetStyle { get; protected set; }

        // FrameBorderStyle: accessor for current gui style of border overlay
        public SSBStyles FrameBorderStyle { get; protected set; }

        // FrameBorderStyleOnNormal: accessor for gui style of border overlay in OnNormal mode
        public SSBStyles FrameBorderStyleOnNormal { get; protected set; }

        // FrameBorderStyleNormal: accessor for gui style of border overlay in Normal mode
        public SSBStyles FrameBorderStyleNormal { get; protected set; }

        public SSBStyles FoldoutStyle { get; protected set; }
        public SSBStyles FoldoutStyleNotEmpty { get; protected set; }
        public SSBStyles FoldoutStyleEmpty { get; protected set; }

        public SSBStyles UpperRollupStyle { get; protected set; }
        public SSBStyles LowerRollupStyle { get; protected set; }

        // FocusID: accessor for the STATIC identifier of which node has focus
        public int FocusID
        {
            get { return _focusID; }
            protected set
            {
                _focusID = value;
                NotifyPropertyChanged("NodeFocusChange");
            }
        }

        // NodeGUID: unique identifier for Node
        [SerializeField]
        public String NodeGUID { get; protected set; }

        // CursorChange: accessor for cursor state changes (perhaps consider expanding to include the cursor type)
        [SerializeField]
        public bool CursorChange
        {
            get { return _cursorState; }
            protected set
            {
                _cursorState = value;
                NotifyPropertyChanged("CursorChange");
            }
        }

        // FrameStateSize: accessor to get a snapshot of the frame area size and flag the change
        protected Vector2 FrameStateSize
        {
            get { return _frameStateSize; }
            set
            {
                //snapshot node size
                _frameStateSize = value;
                FrameStateChanged = true;
            }
        }

        // FrameStateRestore: accessor to flag that FrameStateChanges occured and foldouts can restore a previous size
        [SerializeField]
        protected bool FrameStateRestore { get; set; }
        [SerializeField]
        protected bool FrameStateChanged { get; set; }

        [SerializeField]
        protected bool IsDraggable { get; set; }

        [SerializeField]
        protected bool FoldoutControlActivated { get; set; }

        [SerializeField]
        protected bool FoldoutResizing { get; set; }

        [SerializeField]
        protected int FoldoutControlID { get; set; }

        [SerializeField]
        protected bool ShowSelectedNodeIcon { get; set; }

        [SerializeField]
        public bool NodeSelected { get; protected set; }

        private SSBStyles _styleModeLabel;

        public bool ElementHot { get; protected set; }
        public bool LinkHot { get; protected set; }

        #endregion declarations

        //
        //Methods
        //

        ~Node() //Destructor
        {
            Debug.Log("Node Destructor called:" + NodeWindowID);
        }

        //void OnDestroy()
        //{
        //    Debug.Log("Base Node OnDestroy() called:" + NodeWindowID);
        //}

        void OnDisable()
        {
            //Debug.Log("Node Base Class: OnDisable Called:" + NodeWindowID);
            //_skin = null;
            _elementProvider = null;
            NodeUtilities.MiniScrollbarPropertyChangeEvent -= OnScrollbarPropertyChange;
            DestroyImmediate(Utilities);
        }

        public virtual void OnEnable() //required
        {
            //Debug.Log("Node Base Class OnEnable Called.");
            hideFlags = HideFlags.HideAndDontSave;
            if (NodeGUID == null)
            {
                NodeGUID = Guid.NewGuid().ToString();
            }

            //if (_skin == null)
            //{
            //    _skin = new GUISkinSSB();
            //}

            if (Utilities == null)
            {
                Utilities = CreateInstance<NodeUtilities>();
            }
            NodeUtilities.MiniScrollbarPropertyChangeEvent += OnScrollbarPropertyChange;

            if (_elementProvider == null)
            {
                _elementProvider = new ElementProvider();
            }
            if (_dac != null)
            {
                _dac.EstablishChildElementsProvider(NodeGUID, ref _elementProvider);
            }
        }

        private void OnScrollbarPropertyChange(object sender, MiniScrollbarPropertyChangeEventArgs e)
        {
            MiniScrollbarPropertyChangeEventArgs _event = e as MiniScrollbarPropertyChangeEventArgs;
            FrameOffsetVertical -= _event.Value.y != 0 ? _event.Value.y : 0;
            ScrollValueVertical = _event.Value.x;
            NotifyPropertyChanged("NodeElementStackChanged");
        }

        protected void SetElementProvider(String _elementID)
        {
            _dac.SetElementProvider(_elementID, ref _elementProvider);
        }

        //public void SetAllElementsProvider()
        //{
        //    if (_elementProvider == null)  //necessary check due to recompiles and enter/exit playmode
        //    {
        //        _elementProvider = new ElementProvider();
        //        //Debug.LogWarning("SetElementProvider has generated a new provider for node:" + NodeGUID);
        //    }
        //    _dac.EstablishElementProvider(NodeGUID, ref _elementProvider);
        //}

        protected void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public bool Subscribed
        {
            get { return unsubscriber == null ? false : true; }
        }


        // OnCompleted: Message Provider handler for termination of service from provider
        public virtual void OnCompleted(NodeMessageSignature signature, NodeMessageData value)
        {
            switch (signature)
            {
                case NodeMessageSignature.Removed:
                    {
                        if (value.MessageActionTarget == ActionTarget.SELECTED && NodeSelected)
                        {
                            Debug.Log("Remove Selected Signal; reporting node:" + NodeWindowID);
                            _elementProvider = null;
                        }
                        if (value.MessageActionTarget == ActionTarget.ALL)
                        {
                            Debug.Log("Remove All Signal; reporting node:" + NodeWindowID);
                            _elementProvider = null;
                        }
                        break;
                    }

                case NodeMessageSignature.Exit:
                    {
                        if (value.MessageActionTarget == ActionTarget.ALL)
                        {
                            Debug.Log("Exit signal; reporting node:" + NodeWindowID);
                            _elementProvider = null;
                        }
                        break;
                    }
            }
        }

        // OnError: Message Provider has dispatched an error condition to node
        public void OnError(Exception exception)
        {
            Debug.Log("Provider Error Message:" + exception.ToString());
        }

        // OnNext: Message Provider general message handler
        public virtual void OnNext(NodeMessageSignature signature, NodeMessageData value)
        {
            switch (signature)
            {
                case NodeMessageSignature.Focus:
                    {
                        switch (value.MessageActionTarget)
                        {
                            case ActionTarget.ON:
                                {
                                    if (FocusID.Equals(NodeWindowID))
                                    {
                                        GUI.FocusWindow(NodeWindowID);
                                        FrameBorderStyle = FrameBorderStyleOnNormal;
                                    }
                                    elementProvider.Notify(ElementMessageSignature.Focus, new ElementMessageData(ActionTarget.ON));
                                    break;
                                }
                            case ActionTarget.OFF:
                                {
                                    FrameBorderStyle = FrameBorderStyleNormal;
                                    if (!FrameArea.Contains(Event.current.mousePosition))
                                    {
                                        elementProvider.Notify(ElementMessageSignature.Focus, new ElementMessageData(ActionTarget.OFF));
                                    }
                                    break;
                                }
                            case ActionTarget.RESET:
                                {
                                    FrameBorderStyle = FrameBorderStyleNormal;
                                    FocusID = -1;
                                    GUI.UnfocusWindow();
                                    //elementProvider.Notify(ElementMessageSignature.Focus, new ElementMessageData(ActionTarget.RESET));
                                    NotifyPropertyChanged("NodeElementReset");
                                    break;
                                }
                        }
                        break;
                    }
                case NodeMessageSignature.Lock:
                    {
                        LockState = value.StateSwitch ? true : false;
                        IsDraggable = value.StateSwitch ? false : true;
                        break;
                    }
                case NodeMessageSignature.Translate:
                    {
                        Translate = value.Translate;
                        break;
                    }
                case NodeMessageSignature.Marquis:
                    {
                        if (value.Message != null)
                        {
                            if (value.Message.Equals("Marqing"))
                            {
                                elementProvider.Notify(ElementMessageSignature.Lock, new ElementMessageData(true));
                                break;
                            }
                            if (value.Message.Equals("MarqComplete"))
                            {
                                elementProvider.Notify(ElementMessageSignature.Lock, new ElementMessageData(false));
                                break;
                            }
                        }

                        if (value.MarquisFrame != null)
                        {
                            if (value.MarquisFrame.Area.Overlaps(this.Position, true))
                            {
                                NodeSelected = NodeSelected ? false : true;
                            }
                        }
                        break;
                    }
            }
        }

        internal void Subscribe(ref NodeProvider _provider)
        {
            if (_provider != null)
            {
                unsubscriber = _provider.Subscribe(this);
            }
        }

        // Subscribe: creates reference to a message provider.
        public void Subscribe(IObservable<NodeMessageSignature, NodeMessageData> provider)
        {
            if (provider != null)
            {
                unsubscriber = provider.Subscribe(this);
            }
        }

        // Unsubscribe: remove provider subscription.
        public void Unsubscribe()
        {
            unsubscriber.Dispose();
            unsubscriber = null;
        }

        // DrawNodeWindow generates window content of Node
        public virtual void DrawNodeWindow(int id)
        {
            if (GUIUtility.hotControl == 0)  //required to switch flag for resize detection
            {
                FoldoutControlActivated = false;
            }

            FoldoutStyle = _dac.GetElementList(NodeGUID).Count > 0 ? FoldoutStyleNotEmpty : FoldoutStyleEmpty;
            FrameBorderStyle = FocusID.Equals(NodeWindowID) ? FrameBorderStyle : FrameBorderStyleNormal;
            GUI.Box(new Rect(0, 0, FrameArea.width + 26, HeaderArea.height + 14), GUIContent.none, GUISkinSSB.SkinSSB.customStyles[GUISkinSSB.SSBStyle(FrameBorderStyle)]); //border line

            //Foldout size & state switching checks
            if (FoldoutActive && FrameStateRestore) //restore node size from previous state
            {
                FrameStateRestore = false;
                Size = FrameStateSize;
                FrameHeight = FrameStateSize.y - 14;
            }
            if (!FoldoutActive) //default node size when foldout is not active
            {
                Size = MinMaxSize;
                FrameHeight = MinMaxSize.y - 14;
                FrameStateRestore = true;
            }
            if (FoldoutActive && FrameStateChanged) //capture current frame state size
            {
                FrameStateChanged = false;
                FrameStateSize = Size;
            }

            GUILayout.BeginArea(FrameArea);
            {
                if (FocusID.Equals(NodeWindowID))
                {
                    GUI.Label(new Rect(26, 0, HeaderArea.width - 4, 15), Title, GUISkinSSB.SkinSSB.label);
                }
                else
                {
                    GUI.Label(new Rect(26, 0, HeaderArea.width - 4, 15), Title, GUISkinSSB.SkinSSB.customStyles[GUISkinSSB.SSBStyle(SSBStyles.LabelOff)]);
                }

                if (ShowSelectedNodeIcon)
                {
                    NodeSelected = GUI.Toggle(new Rect(FrameWidth - 12, 2, 10, 10), NodeSelected, GUIContent.none, GUISkinSSB.SkinSSB.customStyles[GUISkinSSB.SSBStyle(SSBStyles.SelectedNode)]);
                }
            }
            GUILayout.EndArea();
            //pathway rollups
            UpperRollupStyle = RollupUpperActive ? SSBStyles.PathwayRollup : SSBStyles.PathwayRollupEmpty;
            LowerRollupStyle = RollupLowerActive ? SSBStyles.PathwayRollup : SSBStyles.PathwayRollupEmpty;
            UpperRollupArea = new Rect(Position.width - 12, HeaderArea.height - 3, 5, 5);
            LowerRollupArea = new Rect(Position.width - 12, FrameArea.height + 2, 5, 5);

            GUILayout.BeginArea(UpperRollupArea);
            {
                GUI.Box(new Rect(0, 0, 5, 5), GUIContent.none, GUISkinSSB.SkinSSB.customStyles[GUISkinSSB.SSBStyle(UpperRollupStyle)]);
            }
            GUILayout.EndArea();
            GUILayout.BeginArea(LowerRollupArea);
            {
                GUI.Box(new Rect(0, 0, 5, 5), GUIContent.none, GUISkinSSB.SkinSSB.customStyles[GUISkinSSB.SSBStyle(LowerRollupStyle)]);
            }
            GUILayout.EndArea();
            


        }

        // HandleEvents Method: Virtual method to handle basic events
        protected virtual void HandleEvents(Event e)
        {

            if (FrameArea.Contains(e.mousePosition) && (e.button == 1)) //rightclick re-focusing
            {
                FocusID = NodeWindowID;
            }
            if (HeaderArea.Contains(e.mousePosition) && (e.type == EventType.MouseDown) && (e.button == 0))
            {
                FocusID = NodeWindowID;
                NotifyPropertyChanged("NodeElementReset");
                //elementProvider.Notify(ElementMessageSignature.Focus, new ElementMessageData(ActionTarget.RESET));
            }

            if (e.rawType == EventType.MouseUp)
            {
                NodeAreaChangeComplete = true;
            }

            if (FoldoutArea.Contains(e.mousePosition) && !_eventStateChanged)
            {
                CursorChange = true;
                _eventStateChanged = true;
            }
            else
            {
                if (_eventStateChanged)
                {
                    CursorChange = false;
                    _eventStateChanged = false;
                }
            }

            if (!FoldoutControlActivated)
            {
                if (FoldoutArea.Contains(e.mousePosition) && e.rawType == EventType.MouseDown)
                {
                    FoldoutControlActivated = true;
                    GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                    FoldoutControlID = GUIUtility.hotControl;
                    FocusID = NodeWindowID;
                    FrameBorderStyle = FrameBorderStyleOnNormal;
                    e.Use();
                }
            }
            if (FoldoutControlActivated && (e.rawType == EventType.MouseDrag))
            {
                ResizeVertical = e.delta.y;
                FoldoutResizing = true;
                NodeAreaChanged = true;
                NodeAreaChangeComplete = false;
                e.Use();
            }

            if (e.GetTypeForControl(FoldoutControlID) == EventType.MouseDown)
            {
                LockState = true;
                IsDraggable = false;
            }

            if (e.GetTypeForControl(FoldoutControlID) == EventType.MouseUp && FoldoutResizing)
            {
                GUIUtility.hotControl = 0;
                FoldoutResizing = false;
                LockState = false;
                IsDraggable = true;
                e.Use();
            }

            if (FrameArea.Contains(e.mousePosition) && e.rawType == EventType.MouseDown && (e.button == 0))
            {
                FrameBorderStyle = FrameBorderStyleOnNormal;
                LockState = false;
                IsDraggable = true;
                FocusID = NodeWindowID;
            }
            //reset node state for drag and resize
            //if (e.rawType == EventType.MouseUp && LockState)
            //{
            //    LockState = false;
            //    IsDraggable = true;
            //}

            if (FrameArea.Contains(e.mousePosition) && e.type == EventType.MouseDrag && !LockState)
            {
                _staticPosition += e.delta;
                _position.x += e.delta.x;
                _position.y += e.delta.y;
                //_staticPosition.Set(_position.x, _position.y);
                NodeAreaChangeComplete = false;
                NodeAreaChanged = true;
                e.Use();
            }

            if (!FrameArea.Contains(e.mousePosition) && ((e.type == EventType.ContextClick) || (e.button == 1)))
            {
                FrameBorderStyle = FrameBorderStyleNormal;
            }

            //RESET HOT STATES
            if ((LinkHot || ElementHot) && e.rawType == EventType.MouseUp)
            {
                ElementHot = false;
                LinkHot = false;
            }

            //Activate Link
            if (FrameArea.Contains(e.mousePosition) && e.type == EventType.MouseDown && !LockState && !ElementHot)
            {
                List<Element> _temp = _dac.GetElementList(NodeGUID).FindAll(x => x.ElementActive);
                if (_temp != null)
                {
                    Vector2 _mouseOffset = new Vector2(e.mousePosition.x - 15, e.mousePosition.y - HeaderArea.height); //calculate mouse position offset based on frames
                    foreach (Element element in _temp)
                    {
                        if (element.LinkArea.Contains(_mouseOffset))
                        {
                            Vector2 _contactPoint = new Vector2(Position.x + HeaderArea.x + element.FrameArea.width - 5, Position.y + HeaderArea.yMax + element.FrameArea.y + ((element.Size.y) - 7));
                            LinkHot = true;
                            ElementHot = false;
                            element.LinkHOT = true;
                            element.ElementHOT = false;
                            Linker.SetLink(element.ElementGUID, _contactPoint);
                            e.Use();
                            break;
                        }
                    }
                }
            }

            //HOT ELEMENT
            if (FrameArea.Contains(e.mousePosition) && e.type == EventType.MouseDrag && !LockState && !LinkHot)
            {
                List<Element> _temp = _dac.GetElementList(NodeGUID).FindAll(x => x.ElementActive);
                if (_temp != null)
                {
                    Vector2 _mouseOffset = new Vector2(e.mousePosition.x - 15, e.mousePosition.y - HeaderArea.height); //calculate mouse position offset based on frames
                    foreach (Element element in _temp)
                    {
                        if (element.FrameArea.Contains(_mouseOffset))
                        {
                            ElementHot = true;
                            element.ElementHOT = true;
                            element.LinkHOT = false;
                            e.Use();
                            break;
                        }
                    }
                }
            }
        }

        // ContextCallback Method: Virtual Method to handle "context clicks" in node frame
        public virtual void ContextCallback(object userData)
        {
            Debug.Log("node:" + NodeControlID + ", context item selected:" + userData);
        }

        // Initialize: base initialization for node
        public virtual void Initialize(Vector2 _location, string _title, Vector2 _size)
        {
            Size = _size;
            MinMaxSize = _size;
            _staticPosition = _location;
            _position = new Rect(_location.x, _location.y, _size.x, _size.y);

            FrameArea = new Rect(15, 3, _size.x - 26, _size.y - 14);
            FoldoutArea = new Rect();
            ElementArea = new Rect();
            HeaderArea = new Rect(FrameArea);

            NodeAreaChangeComplete = true;
            NodeAreaChanged = false;
            FoldoutStyleNotEmpty = SSBStyles.Foldout;
            FoldoutStyleEmpty = SSBStyles.FoldoutEmpty;
            FoldoutStyle = FoldoutStyleEmpty;

            Connected = false;
            RollupLowerActive = false;
            RollupUpperActive = false;

            UpperRollupArea = new Rect();
            LowerRollupArea = new Rect();

        }
    }
}
