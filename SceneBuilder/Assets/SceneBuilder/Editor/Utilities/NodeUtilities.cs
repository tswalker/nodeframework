﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEditor;
using Assets.SceneBuilder.Editor.Nodes;
using Assets.SceneBuilder.Editor.Elements;

namespace Assets.SceneBuilder.Editor.Utilities
{
    [Serializable]
    public sealed class NodeUtilities : ScriptableObject
    {
        public static EventHandler<MiniScrollbarPropertyChangeEventArgs> MiniScrollbarPropertyChangeEvent;

        [SerializeField]
        private bool _scrollbarVActive, _scrollbarHActive, _boundaryLock, _initSnapShot, _windowSizeStateChange, _scrollValueChanged;
        [SerializeField]
        private Rect _scrollbarV; //_scrollbarH, 
        [SerializeField]
        private float _scrollValueH, _scrollValueV, _boundaryX, _boundaryY, _boundaryZ, _boundaryW, _scrollLastValueV;
        [SerializeField]
        private Vector4 _boundaries;

        [SerializeField]
        public int ScrollbarControlID { get; set; }

        //private static GUISkinSSB Skin;
        //private GUISkinSSB Skin;

        //private bool _scrollbarControlActivated = false;
        private List<ElementAttribute> _elementTypes;

        public const String EmptyGUID = "00000000-0000-0000-0000-000000000000";

        public bool ScrollbarControlActive
        {
            get { return (!ScrollbarControlID.Equals(0)) ? true : false; }
        }

        public void OnDestroy()
        {
        }

        void OnDisable()
        {
            //Skin = null;
        }

        public void OnEnable()
        {
            hideFlags = HideFlags.HideAndDontSave;
            //if (Skin == null)
            //{
            //    Skin = new GUISkinSSB();
            //}
        }


        /// <summary>
        /// CustomScrollBars : generates horizontal and/or vertical scrollbars
        /// </summary>
        /// <param name="_content">Content: The Rect area to be examined which holds the scrollable content.</param>
        /// <param name="_view">View: The Rect area to be considered as the frame or currently viewed region.</param>
        /// <returns>Vector4(_scrollValueH, _scrollValueV, _frameOffsetX, _frameOffsetY), 
        ///  _scrollValueV: current value for the vertical scoll bar
        ///  _frameOffsetY: if this value != 0, the vertical content area no longer expands beyond the view
        /// </returns>
        public void CustomVerticalScrollBar(Rect _content, Rect _view)
        {
            float _yMinU = _content.yMin < _view.yMin ? _content.yMin : 0;
            float _yMaxL = _content.height > _view.height ? _content.height - _view.height : 0;
            float _frameOffsetY = 0;
            float _scrollHeight = 0;
            float _scrollVerticalOffset = 0;

            _boundaries.Set(0, _yMinU, 0, _yMaxL);

            if (_boundaries.y != 0 || _boundaries.w != 0)
            {
                _scrollHeight = _view.height < 50 ? 50 : _view.height;
                _scrollVerticalOffset = _view.height < 50 ? (50 - _view.height) : 0;
                _scrollbarV.Set(_view.x - 15, _view.y - _scrollVerticalOffset + 5, 13, _scrollHeight + 10);
                _scrollValueV = (float)Math.Round(GUI.VerticalScrollbar(_scrollbarV, _scrollValueV, 0, _boundaryY, _boundaryW, GUISkinSSB.SkinSSB.GetStyle("verticalscrollbar")), 0, MidpointRounding.AwayFromZero);
                ScrollbarControlID = GUIUtility.GetControlID(FocusType.Passive);
                _scrollbarVActive = true;
                if (!_scrollLastValueV.Equals(_scrollValueV))
                {
                    _scrollLastValueV = _scrollValueV;
                    _scrollValueChanged = true;
                }
            }
            else
            {
                if (_scrollbarVActive)
                {
                    _scrollbarVActive = false;
                    _scrollValueChanged = false;
                    //_scrollbarControlActivated = false;
                    ScrollbarControlID = 0;
                    GUIUtility.hotControl = 0; //without if check, handle is lost on foldout resize when scrollbar disappears
                    _frameOffsetY = _scrollValueV;
                    _scrollValueV = 0;
                    _scrollbarV.Set(0, 0, 0, 0);
                    MiniScrollbarPropertyChangeEventArgs _event = new MiniScrollbarPropertyChangeEventArgs();
                    _event.Value = new Vector2(_scrollValueV,_frameOffsetY);
                    MiniScrollbarPropertyChangeEvent(null, _event);
                }
            }

            _boundaryLock = ScrollbarControlActive ? false : true;

            if (!_boundaryLock)  //only update scrollbar ranges if scrolling is not occuring
            {
                if (_scrollbarVActive && _scrollValueV == 0)
                {
                    _boundaryY = _boundaries.y;
                    _boundaryW = _boundaries.w;
                }
            }
            if (_scrollbarVActive && _scrollValueChanged)
            {
                MiniScrollbarPropertyChangeEventArgs _event = new MiniScrollbarPropertyChangeEventArgs();
                _event.Value = new Vector2(_scrollValueV, _frameOffsetY);
                MiniScrollbarPropertyChangeEvent(null, _event);
            }
        }

        private static List<ElementAttribute> BuildElementTypeList()
        {
            List<ElementAttribute> _temp = new List<ElementAttribute>();
            var types = AppDomain.CurrentDomain.GetAssemblies().ToList().SelectMany(s => s.GetTypes());
            foreach (var type in types)
            {
                foreach (var attribute in Attribute.GetCustomAttributes(type).OfType<ElementAttribute>())
                {
                    _temp.Add(attribute);
                }
            }
            return _temp;
        }

        public void BuildNodeContextMenu(GenericMenu.MenuFunction2 _callback, ElementCategory _category, List<Element> _elements)
        {
            if (_elementTypes == null)
            {
                _elementTypes = BuildElementTypeList();
            }

            GenericMenu _menu = new GenericMenu();
            _menu.AddSeparator("Elements:");
            foreach (ElementAttribute attribute in _elementTypes)
            {
                if (attribute.Category.Equals(_category))
                {
                    _menu.AddItem(new GUIContent("Create Element/" + attribute.SubCategory))
                        .Visible(true)
                        .Enable(true)
                        .Action(_callback, new ActionCommand(ActionIdentifier.ADD, attribute));
                }
            }
            _menu.AddItem("Remove Element/")
                .Visible(true)
                .Enable(true);
            _menu.AddItem("Remove Element/All")
                .Visible(true)
                .Enable(() => { return _elements.Count > 0; })
                .Action(_callback, new ActionCommand(ActionIdentifier.REMOVE, ActionTarget.ALL));
            _menu.AddItem("Remove Element/Selected")
                .Visible(true)
                .Enable(() =>
                {
                    if (_elements.Count > 0) { return _elements.Find(x => x.ElementActive); }
                    else { return false; }
                })
                .Action(_callback, new ActionCommand(ActionIdentifier.REMOVE, ActionTarget.SELECTED));

            _menu.ShowAsContext();
        }

        public Rect CalculateElementAreaPositions(List<Element> elementList)
        {
            Rect _elementArea = new Rect();
            if (elementList.Count().Equals(0)) { return _elementArea; }

            float _verticalOffset = 0;
            List<Vector2> _temp = new List<Vector2>();
            float _tempHeight = 0;
            foreach (Element element in elementList)
            {
                _temp.Add(element.Position);
                _tempHeight += element.Size.y - 4;
                element.Position = new Vector2(element.Position.x, _verticalOffset);
                element.FrameAreaSetY = _verticalOffset;
                _verticalOffset += element.Size.y - 5;
            }
            if (_temp.Count > 0)
            {
                _elementArea.xMin = _temp.Min(s => s.x);
                _elementArea.yMin = _temp.Min(s => s.y);
                _elementArea.xMax = _temp.Max(s => s.x);
                _elementArea.yMax = _tempHeight;
            }
            return _elementArea;
        }

        public void DrawCurve(Vector2 start, Vector2 end)
        {
            //float _offset = 10f;
            //start.x,start.y , end.x , end.y
            //Debug.Log("drawing a curve?");
            //Handles.DrawBezier(new Vector3(start.x, start.y, 0), new Vector3(end.x, end.y, 0), new Vector3(0, 1, 0), new Vector3(0, -1, 0), Color.white, _aaLine, 1.5f);
            Handles.color = Color.white;
            Handles.DrawLine(new Vector3(start.x, start.y, 0), new Vector3(end.x, end.y, 0));
        }
    }

    public class MiniScrollbarPropertyChangeEventArgs : EventArgs
    {
        public Vector2 Value { get; set; }
        public String GUID { get; set; }
    }
}