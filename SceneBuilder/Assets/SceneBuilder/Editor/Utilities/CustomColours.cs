﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

namespace Assets.SceneBuilder.Editor.Utilities
{
    /// <summary>
    /// CustomColours: Helper class to define colour sets
    /// </summary>
    public class CustomColours
    {
        public static Color DarkerGrey { get; private set; }
        public static Color DarkGrey { get; private set; }
        public static Color MediumGrey { get; private set; }
        public static Color LighterGrey { get; private set; }
        public static Color LightGrey { get; private set; }
        public static Color LightestGrey { get; private set; }
        public static Color ConnectorInActiveGrey { get; private set; }
        public static Color ConnectorActiveGrey { get; private set; }
        
        static CustomColours()
        {
            if (EditorGUIUtility.isProSkin)
            {
                DarkerGrey = new Color(0.1f, 0.1f, 0.1f);
                DarkGrey = new Color(0.145f, 0.145f, 0.145f);
                MediumGrey = new Color(0.25f, 0.25f, 0.25f);
                LightGrey = new Color(0.275f, 0.275f, 0.275f);
                LighterGrey = new Color(0.3f, 0.3f, 0.3f);
                LightestGrey = new Color(0.375f, 0.375f, 0.375f);
            }
            else
            {
                DarkerGrey = new Color(0.4f, 0.4f, 0.4f);
                DarkGrey = new Color(0.4f, 0.4f, 0.4f);
                MediumGrey = new Color(0.7f, 0.7f, 0.7f);
                LightGrey = new Color(0.65f, 0.65f, 0.65f);
                LighterGrey = new Color(0.6f, 0.6f, 0.6f);
                LightestGrey = new Color(0.8f, 0.8f, 0.8f);
            }
            ConnectorInActiveGrey = new Color(0.375f, 0.375f, 0.375f);
            ConnectorActiveGrey = new Color(0.533f, 0.533f, 0.533f);
        }
    }
}
