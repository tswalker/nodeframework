﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

namespace Assets.SceneBuilder.Editor.Utilities
{
    internal sealed class GenericMenuAddItemContext : IGenericMenuAddItemContext
    {

        private static Stack<GenericMenuAddItemContext> s_Pool = new Stack<GenericMenuAddItemContext>();

        public static GenericMenuAddItemContext GetContext(GenericMenu menu, GUIContent content)
        {
            var context = s_Pool.Count > 0
                ? s_Pool.Pop()
                : new GenericMenuAddItemContext();

            context._menu = menu;
            context._content = content;
            context._visible = true;
            context._enabled = true;
            context._on = false;

            return context;
        }

        private static void ReturnContext(GenericMenuAddItemContext context)
        {
            context._menu = null;
            context._content = null;

            s_Pool.Push(context);
        }

        private GenericMenu _menu;
        private GUIContent _content;
        private bool _visible;
        private bool _enabled;
        private bool _on;


        public IGenericMenuAddItemContext Enable(bool enable)
        {
            _enabled = enable;
            return this;
        }

        public IGenericMenuAddItemContext Enable(Func<bool> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException("predicate");

            _enabled = predicate();
            return this;
        }

        public IGenericMenuAddItemContext Visible(bool visible)
        {
            _visible = visible;
            return this;
        }

        public IGenericMenuAddItemContext Visible(Func<bool> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException("predicate");

            _visible = predicate();
            return this;
        }

        public IGenericMenuAddItemContext On(bool on)
        {
            _on = on;
            return this;
        }

        public IGenericMenuAddItemContext On(Func<bool> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException("predicate");

            _on = predicate();
            return this;
        }

        public void Action(GenericMenu.MenuFunction action)
        {
            try
            {
                if (action == null)
                    throw new ArgumentNullException("action");

                if (!_visible)
                    return;
                if (_enabled)
                    _menu.AddItem(_content, _on, action);
                else
                    _menu.AddDisabledItem(_content);
            }
            finally
            {
                ReturnContext(this);
            }
        }

        public void Action(GenericMenu.MenuFunction2 action, object userData)
        {
            try
            {
                if (action == null)
                    throw new ArgumentNullException("action");

                if (!_visible)
                    return;
                if (_enabled)
                    _menu.AddItem(_content, _on, action, userData);
                else
                    _menu.AddDisabledItem(_content);
            }
            finally
            {
                ReturnContext(this);
            }
        }

    }
}
