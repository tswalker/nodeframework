﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

namespace Assets.SceneBuilder.Editor.Utilities
{
    /// <summary>
    /// Extension methods for <see cref="UnityEditor.GenericMenu"/>.
    /// </summary>
    public static class GenericMenuExtensions
    {

        private static Dictionary<string, GUIContent> s_ContentCache = new Dictionary<string, GUIContent>();

        private static GUIContent GetContent(string text)
        {
            GUIContent content;
            if (!s_ContentCache.TryGetValue(text, out content))
            {
                content = new GUIContent(text);
                s_ContentCache[text] = content;
            }
            return content;
        }

        /// <summary>
        /// Begin adding an item to the <see cref="GenericMenu"/>.
        /// </summary>
        /// <example>
        /// <para>Here are some usage examples:</para>
        /// <code language="csharp"><![CDATA[
        /// var menu = new GenericMenu();
        /// 
        /// menu.AddItem(new GUIContent("Reset"))
        ///     .Enable(Selection.activeObject != null)
        ///     .Action(() => {
        ///         // Place reset logic here...
        ///     });
        /// 
        /// menu.AddItem(new GUIContent("Set as Default Object"))
        ///     .Enable(Selection.activeObject != null)
        ///     .On(Selection.activeObject == currentDefaultObject)
        ///     .Action(() => {
        ///         currentDefaultObject = Selection.activeObject;
        ///     });
        /// 
        /// menu.ShowAsContext();
        /// ]]></code>
        /// </example>
        /// <param name="menu">The menu that is being constructed.</param>
        /// <param name="content">Content of menu item.</param>
        /// <returns>
        /// Context object used whilst adding a menu item.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// <list type="bullet">
        /// <item>If <paramref name="menu"/> is a value of <c>null</c>.</item>
        /// <item>If <paramref name="content"/> is a value of <c>null</c>.</item>
        /// </list>
        /// </exception>
        public static IGenericMenuAddItemContext AddItem(this GenericMenu menu, GUIContent content)
        {
            if (menu == null)
                throw new ArgumentNullException("menu");
            if (content == null)
                throw new ArgumentNullException("content");

            return GenericMenuAddItemContext.GetContext(menu, content);
        }

        /// <summary>
        /// Begin adding an item to the <see cref="GenericMenu"/>.
        /// </summary>
        /// <example>
        /// <para>Here are some usage examples:</para>
        /// <code language="csharp"><![CDATA[
        /// var menu = new GenericMenu();
        /// 
        /// menu.AddItem("Reset")
        ///     .Enable(Selection.activeObject != null)
        ///     .Action(() => {
        ///         // Place reset logic here...
        ///     });
        /// 
        /// menu.AddItem("Set as Default Object")
        ///     .Enable(Selection.activeObject != null)
        ///     .On(Selection.activeObject == currentDefaultObject)
        ///     .Action(() => {
        ///         currentDefaultObject = Selection.activeObject;
        ///     });
        /// 
        /// menu.ShowAsContext();
        /// ]]></code>
        /// </example>
        /// <param name="menu">The menu that is being constructed.</param>
        /// <param name="text">Text of menu item.</param>
        /// <returns>
        /// Context object used whilst adding a menu item.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// <list type="bullet">
        /// <item>If <paramref name="menu"/> is a value of <c>null</c>.</item>
        /// <item>If <paramref name="text"/> is a value of <c>null</c>.</item>
        /// </list>
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// If <paramref name="text"/> is an empty string.
        /// </exception>
        public static IGenericMenuAddItemContext AddItem(this GenericMenu menu, string text)
        {
            if (menu == null)
                throw new ArgumentNullException("menu");
            if (text == null)
                throw new ArgumentNullException("text");
            if (text == "")
                throw new ArgumentException("Empty string.", "text");

            return GenericMenuAddItemContext.GetContext(menu, GetContent(text));
        }

        /// <summary>
        /// Adds separator to the <see cref="GenericMenu"/>.
        /// </summary>
        /// <param name="menu">The menu that is being constructed.</param>
        /// <exception cref="System.ArgumentNullException">
        /// If <paramref name="menu"/> is a value of <c>null</c>.
        /// </exception>
        public static void AddSeparator(this GenericMenu menu)
        {
            if (menu == null)
                throw new ArgumentNullException("menu");

            menu.AddSeparator();
        }

    }

}