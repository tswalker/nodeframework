﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

namespace Assets.SceneBuilder.Editor.Utilities
{
    /// <summary>
    /// SSBImages: Helper class that generates in-code images for use
    /// </summary>
    static class SSBImages
    {
        public static Texture2D MarqLine { get; private set; }

        public static Texture2D MarqueeBox { get; private set; }

        public static Texture2D AALine { get; private set; }

        //~SSBImages()
        //{
        //    Debug.Log("SSBImages Destructor Called.");
        //    ScriptableObject.DestroyImmediate(MarqLine);
        //    ScriptableObject.DestroyImmediate(MarqueeBox);
        //    ScriptableObject.DestroyImmediate(AALine);
        //    MarqLine = null;
        //    MarqueeBox = null;
        //    AALine = null;
        //}

        static SSBImages()
        {
            Debug.Log("SSBImages Constructor Called.");
            if (MarqLine == null)
            {
                MarqLine = new Texture2D(3, 3, TextureFormat.ARGB32, false);// = ConstructLineImage();
                ConstructLineImage(MarqLine);
                MarqLine.Apply(false);
                MarqLine.hideFlags = HideFlags.DontSave;
                MarqLine.filterMode = FilterMode.Bilinear;
                MarqLine.wrapMode = TextureWrapMode.Repeat;
                MarqLine.name = "MarqLine";
            }

            if (MarqueeBox == null)
            {
                MarqueeBox = new Texture2D(3, 3, TextureFormat.ARGB32, false);
                ConstructMarqueeBox(MarqueeBox);
                MarqueeBox.Apply(false);
                MarqueeBox.hideFlags = HideFlags.DontSave;
                MarqueeBox.filterMode = FilterMode.Bilinear;
                MarqueeBox.wrapMode = TextureWrapMode.Repeat;
                MarqueeBox.name = "MarqueeBox";
            }

            if (AALine == null)
            {
                AALine = new Texture2D(1, 5, TextureFormat.ARGB32, false);
                ConstructAALineImage(AALine);
                AALine.Apply(false);
                AALine.hideFlags = HideFlags.DontSave;
                AALine.filterMode = FilterMode.Bilinear;
                AALine.wrapMode = TextureWrapMode.Repeat;
                AALine.name = "AALine";
            }
        }

        private static void ConstructLineImage(Texture2D _texture)
        {

            Color32[] _colors = new Color32[9];
            _colors[0] = new Color(1, 1, 1, 1);
            _colors[1] = new Color(1, 1, 1, 0);
            _colors[2] = new Color(1, 1, 1, 1);
            _colors[3] = new Color(1, 1, 1, 0);
            _colors[4] = new Color(1, 1, 1, 0);
            _colors[5] = new Color(1, 1, 1, 0);
            _colors[6] = new Color(1, 1, 1, 1);
            _colors[7] = new Color(1, 1, 1, 0);
            _colors[8] = new Color(1, 1, 1, 1);
            _texture.SetPixels32(_colors);
        }

        private static void ConstructAALineImage(Texture2D _texture)
        {
            Color32[] _colors = new Color32[5];
            _colors[0] = new Color(1, 1, 1, 0);
            _colors[1] = new Color(1, 1, 1, 0.5f);
            _colors[2] = new Color(1, 1, 1, 1);
            _colors[3] = new Color(1, 1, 1, 0.5f);
            _colors[4] = new Color(1, 1, 1, 0);
            _texture.SetPixels32(_colors);
        }

        private static void ConstructMarqueeBox(Texture2D _texture)
        {
            Color32[] _colors = new Color32[9];
            _colors[0] = CustomColours.LightestGrey;
            _colors[1] = CustomColours.LightestGrey;
            _colors[2] = CustomColours.LightestGrey;
            _colors[3] = CustomColours.LightestGrey;
            _colors[4] = new Color(1, 1, 1, 0);
            _colors[5] = CustomColours.LightestGrey;
            _colors[6] = CustomColours.LightestGrey;
            _colors[7] = CustomColours.LightestGrey;
            _colors[8] = CustomColours.LightestGrey;
            _texture.SetPixels32(_colors);
        }
    }
}