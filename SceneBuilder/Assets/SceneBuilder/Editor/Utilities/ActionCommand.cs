﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using Assets.SceneBuilder.Editor.Nodes;
using Assets.SceneBuilder.Editor.Elements;

namespace Assets.SceneBuilder.Editor.Utilities
{
    public class ActionCommand
    {
        public ActionIdentifier ID { get; set; }
        public ActionTarget Target { get; set; }
        public NodeAttribute NodeAttribute { get; set; }
        public ElementAttribute ElementAttribute { get; set; }

        public ActionCommand(ActionIdentifier _identifier, NodeAttribute _nodeAttribute)
        {
            ID = _identifier;
            NodeAttribute = _nodeAttribute;
        }

        public ActionCommand(ActionIdentifier _identifier, ActionTarget _nodeTarget)
        {
            ID = _identifier;
            Target = _nodeTarget;
        }

        public ActionCommand(ActionIdentifier _identifier, ElementAttribute _elementAttribute)
        {
            ID = _identifier;
            ElementAttribute = _elementAttribute;
        }
    }
    public enum ActionIdentifier
    {
        ADD,
        REMOVE
    }

    public enum ActionTarget
    {
        NONE,
        ALL,
        SELECTED,
        RESET,
        ON,
        OFF
    }
}

