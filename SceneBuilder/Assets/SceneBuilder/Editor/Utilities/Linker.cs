﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using Assets.SceneBuilder.Editor.Elements;
using Assets.SceneBuilder.Editor.Nodes;
using Assets.SceneBuilder.Editor.Data;

namespace Assets.SceneBuilder.Editor.Utilities
{
    /// <summary>
    /// Linker Class: Helper class to assist in creating Connectoids
    /// </summary>
    public static class Linker
    {
        public static EventHandler OnStartLink;
        public static EventHandler OnCloseLink;
        public static EventHandler<LinkerCompleteEventArg> OnLinkComplete;

        public static Vector2 SourcePoint { get; private set; }
        public static String SourceID { get; private set; }
        public static String DestinationID { get; private set; }
        public static Vector2 DestinationPoint { get; private set; }


        public static void SetLink(String _elementID, Vector2 _point)
        {
            ClearLinks();
            EventHandler _onLink = OnStartLink;
            if (_onLink != null)
            {
                SourceID = _elementID;
                SourcePoint = _point;
                _onLink(null, EventArgs.Empty);
            }
        }

        public static void CloseLink(String _elementID, Vector2 _point)
        {
            EventHandler _onClose = OnCloseLink;
            if (_onClose != null)
            {
                DestinationID = _elementID;
                DestinationPoint = _point;
                _onClose(null, EventArgs.Empty);
                ProcessLink();
            }
        }

        private static void ProcessLink()
        {
            EventHandler<LinkerCompleteEventArg> _onComplete = OnLinkComplete;

            if (DestinationID == null || SourceID == null)
            {
                Debug.LogWarning("Invalid Identifier during Linkage.");
                ClearLinks();
                return;
            }
            if (DestinationID.Equals(NodeUtilities.EmptyGUID) || SourceID.Equals(NodeUtilities.EmptyGUID))
            {
                Debug.LogWarning("Invalid Identifier during Node Linkage. [0x2]");
                ClearLinks();
                return;//Connectoid Link Canceled
            }
            if (_onComplete != null)
            {
                _onComplete(null, new LinkerCompleteEventArg(SourceID, DestinationID));
            }
            ClearLinks();
        }

        private static void ClearLinks()
        {
            DestinationID = NodeUtilities.EmptyGUID;
            DestinationPoint = Vector2.zero;
            SourceID = NodeUtilities.EmptyGUID;
            SourcePoint = Vector2.zero;
        }
    }

    public class LinkerCompleteEventArg : EventArgs
    {
        public String Source { get; set; }
        public String Destination { get; set; }

        public LinkerCompleteEventArg(String _source, String _destination)
        {
            Source = _source;
            Destination = _destination;
        }
    }
}