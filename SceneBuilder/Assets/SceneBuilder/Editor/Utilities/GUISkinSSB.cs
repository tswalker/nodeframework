﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

namespace Assets.SceneBuilder.Editor.Utilities
{
    static class GUISkinSSB
    {
        //public static GUISkin SkinSSB { get { return _skin; } }
        //private static GUISkin _skin;
        public static GUISkin SkinSSB { get; private set; }
        private static String resourcePath = "Assets/SceneBuilder/Editor/Images/";

        //public void Dissolve()
        //~GUISkinSSB()
        //{
        //    Debug.Log("GUISkinSSB Dissolved.");
        //    ScriptableObject.DestroyImmediate(SkinSSB);
        //    SkinSSB = null;
        //}

        static GUISkinSSB()
        {
            if (SkinSSB == null)
            {
                SkinSSB = ScriptableObject.CreateInstance<GUISkin>();
                SkinSSB.hideFlags = HideFlags.DontSave;
                SkinSSB.name = "GUISkinSSB";
                ConstructSkin(SkinSSB);
            }
        }

        private static void ConstructSkin(GUISkin _skinSSB)
        {
            //Configure general window style settings
                _skinSSB.window.Use((p) =>
                {
                    p.border.left = 8;
                    p.border.right = 8;
                    p.border.top = 18;
                    p.border.bottom = 12;
                    p.padding.top = 4;
                    p.padding.bottom = -3;
                    p.imagePosition = ImagePosition.ImageLeft;
                    p.clipping = TextClipping.Clip;
                    p.alignment = TextAnchor.UpperCenter;
                    p.wordWrap = false;
                    p.stretchWidth = true;
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "ToolPanel.tif", typeof(Texture2D)) as Texture2D;
                    p.normal.textColor = new Color(0.686f, 0.686f, 0.686f);
                    p.onNormal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "ToolPanel_ON.tif", typeof(Texture2D)) as Texture2D;
                    p.onNormal.textColor = new Color(0.786f, 0.786f, 0.786f);
                });

                //Label
                _skinSSB.label.Use((p) =>
                {
                    p.padding.top = 1;
                    p.padding.bottom = 1;
                    p.margin.left = 1;
                    p.margin.right = 1;
                    p.margin.top = 1;
                    p.margin.bottom = 1;
                    p.imagePosition = ImagePosition.ImageLeft;
                    p.clipping = TextClipping.Clip;
                    p.alignment = TextAnchor.UpperLeft;
                    p.wordWrap = true;
                    p.richText = false;
                    p.font = null;//Resources.GetBuiltinResource<Font>("Arial.ttf");
                    p.fontSize = 11;
                    p.fontStyle = FontStyle.Normal;
                    p.normal.textColor = new Color(0.686f, 0.686f, 0.686f);
                });

                //Horizontal Scrollbar
                _skinSSB.horizontalScrollbar.Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "HorizontalBar.tif", typeof(Texture2D)) as Texture2D;
                    p.border.left = 5;
                    p.border.right = 5;
                    p.border.top = 5;
                    p.border.bottom = 5;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedHeight = 11;
                    p.stretchWidth = true;
                });

                //Horizontal Scrollbar Thumb
                _skinSSB.horizontalScrollbarThumb.Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "HorizontalThumb.tif", typeof(Texture2D)) as Texture2D;
                    p.border.left = 0;
                    p.border.right = 0;
                    p.border.top = 0;
                    p.border.bottom = 0;
                    p.overflow = new RectOffset(0, 0, 1, 0);
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedWidth = 19;
                    p.fixedHeight = 13;
                });

                //Horizontal Scrollbar Left Button
                _skinSSB.horizontalScrollbarLeftButton.Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "HorizontalLeftArrow.tif", typeof(Texture2D)) as Texture2D;
                    p.border.left = 0;
                    p.border.right = 0;
                    p.border.top = 0;
                    p.border.bottom = 0;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedWidth = 11;
                    p.fixedHeight = 11;
                });

                //Horizontal Scrollbar Right Button
                _skinSSB.horizontalScrollbarRightButton.Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "HorizontalRightArrow.tif", typeof(Texture2D)) as Texture2D;
                    p.border.left = 0;
                    p.border.right = 0;
                    p.border.top = 0;
                    p.border.bottom = 0;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedWidth = 11;
                    p.fixedHeight = 11;
                });

                //Vertical Scrollbar Right
                _skinSSB.verticalScrollbar.Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "VerticalBar.tif", typeof(Texture2D)) as Texture2D;
                    p.border.left = 5;
                    p.border.right = 5;
                    p.border.top = 5;
                    p.border.bottom = 5;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedWidth = 11;
                    p.stretchWidth = true;
                });

                //Vertical Scrollbar Thumb
                _skinSSB.verticalScrollbarThumb.Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "VerticalThumb.tif", typeof(Texture2D)) as Texture2D;
                    p.border.left = 0;
                    p.border.right = 0;
                    p.border.top = 0;
                    p.border.bottom = 0;
                    p.overflow = new RectOffset(1, 0, 0, 0);
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedWidth = 13;
                    p.fixedHeight = 19;
                });

                //Vertical Scrollbar Left Button
                _skinSSB.verticalScrollbarUpButton.Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "VerticalUpArrow.tif", typeof(Texture2D)) as Texture2D;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedWidth = 11;
                    p.fixedHeight = 11;
                });

                //Vertical Scrollbar Right Button
                _skinSSB.verticalScrollbarDownButton.Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "VerticalDownArrow.tif", typeof(Texture2D)) as Texture2D;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedWidth = 11;
                    p.fixedHeight = 11;
                });

                // CUSTOM Styles
                // ------------ //
                _skinSSB.customStyles = new GUIStyle[]
                    {
                        new GUIStyle() // Element 0
                        {
                            name = "AnchorNode",
                        },
                        new GUIStyle() // Element 1
                        {
                            name = "AnchorNodeExpanded",
                        },
                        new GUIStyle() // Element 2
                        {
                            name = "LinkNode",
                        },
                        new GUIStyle() // Element 3
                        {
                            name = "LinkNodeExpanded",
                        },
                        new GUIStyle() // Element 3
                        {
                            name = "RedBox",
                        },
                        new GUIStyle()
                        {
                            name = "FrameBaseBorder",
                        },
                        new GUIStyle()
                        {
                            name = "FrameBaseBorderON",
                        },
                        new GUIStyle()
                        {
                            name = "Foldout",
                        },
                        new GUIStyle()
                        {
                            name = "FoldoutEmpty",
                        },
                        new GUIStyle()
                        {
                            name = "SelectedNode",
                        },
                        new GUIStyle()
                        {
                            name = "Element",
                        },
                        new GUIStyle()
                        {
                            name = "ElementActive"
                        },
                        new GUIStyle()
                        {
                            name = "LabelOff"
                        },
                        new GUIStyle()
                        {
                            name = "LinkIcon"
                        },
                        new GUIStyle()
                        {
                            name = "LinkIconON"
                        },
                        new GUIStyle()
                        {
                            name = "LinkNodeFBB"
                        },
                        new GUIStyle()
                        {
                            name = "LinkNodeFBBON"
                        },
                        new GUIStyle()
                        {
                            name = "ElementLinked"
                        },
                        new GUIStyle()
                        {
                            name = "ElementLinkedON"
                        },
                        new GUIStyle()
                        {
                            name = "LinkedNode"
                        },
                        new GUIStyle()
                        {
                            name = "LinkedNodeExpanded"
                        },
                        new GUIStyle()
                        {
                            name = "PathwayRollup"
                        },
                        new GUIStyle()
                        {
                            name = "PathwayRollupEmpty"
                        }
                    };

                //Element 0 - Custom Style SSBStyles.AnchorNode
                _skinSSB.customStyles[SSBStyle(SSBStyles.AnchorNode)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "AnchorNode.tif", typeof(Texture2D)) as Texture2D;
                    p.normal.textColor = new Color(0.886f, 0.886f, 0.886f);
                    p.onNormal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "AnchorNode_ON.tif", typeof(Texture2D)) as Texture2D;
                    p.onNormal.textColor = new Color(0.986f, 0.986f, 0.986f);
                    p.border.left = 64;
                    p.border.right = 59;
                    p.border.top = 34;
                    p.border.bottom = 14;
                    p.padding.left = 32;
                    p.padding.top = 5;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageLeft;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 1 - Custom Style SSBStyles.AnchorNodeExpanded
                _skinSSB.customStyles[SSBStyle(SSBStyles.AnchorNodeExpanded)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "AnchorNodeExpanded.tif", typeof(Texture2D)) as Texture2D;
                    p.normal.textColor = new Color(0.9f, 0.9f, 0.9f);
                    p.onNormal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "AnchorNodeExpanded_ON.tif", typeof(Texture2D)) as Texture2D;
                    p.onNormal.textColor = new Color(1f, 1f, 1f);
                    p.border.left = 64;
                    p.border.right = 59;
                    p.border.top = 34;
                    p.border.bottom = 14;
                    p.padding.left = 32;
                    p.padding.top = 5;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageLeft;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 2 - Custom Style SSBStyles.LinkNode
                _skinSSB.customStyles[SSBStyle(SSBStyles.LinkNode)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "LinkNode.tif", typeof(Texture2D)) as Texture2D;
                    p.normal.textColor = new Color(0.686f, 0.686f, 0.686f);
                    p.onNormal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "LinkNode_ON.tif", typeof(Texture2D)) as Texture2D;
                    p.onNormal.textColor = new Color(0.786f, 0.786f, 0.786f);
                    p.border.left = 63;
                    p.border.right = 60;
                    p.border.top = 34;
                    p.border.bottom = 14;
                    p.padding.left = 32;
                    p.padding.top = 5;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageLeft;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 3 - Custom Style SSBStyles.LinkNodeExpanded
                _skinSSB.customStyles[SSBStyle(SSBStyles.LinkNodeExpanded)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "LinkNodeExpanded.tif", typeof(Texture2D)) as Texture2D;
                    p.normal.textColor = new Color(0.686f, 0.686f, 0.686f);
                    p.onNormal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "LinkNodeExpanded_ON.tif", typeof(Texture2D)) as Texture2D;
                    p.onNormal.textColor = new Color(0.786f, 0.786f, 0.786f);
                    p.border.left = 63;
                    p.border.right = 60;
                    p.border.top = 34;
                    p.border.bottom = 14;
                    p.padding.left = 32;
                    p.padding.top = 5;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageLeft;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 4 - Custom Style RedBox
                _skinSSB.customStyles[SSBStyle(SSBStyles.RedBox)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "RedBox.tif", typeof(Texture2D)) as Texture2D;
                    p.normal.textColor = new Color(0.686f, 0.686f, 0.686f);
                    p.border.left = 2;
                    p.border.right = 2;
                    p.border.top = 2;
                    p.border.bottom = 2;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 5 - Custom Style FrameBaseBorder
                _skinSSB.customStyles[SSBStyle(SSBStyles.AnchorNodeFBB)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "AnchorNodeFBB.tif", typeof(Texture2D)) as Texture2D;
                    p.normal.textColor = new Color(0.686f, 0.686f, 0.686f);
                    p.border.left = 64;
                    p.border.right = 59;
                    p.border.top = 34;
                    p.border.bottom = 14;
                    p.padding.left = 32;
                    p.padding.top = 5;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 6 - Custom Style FrameBaseBorder
                _skinSSB.customStyles[SSBStyle(SSBStyles.AnchorNodeFBBON)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "AnchorNodeFBB_ON.tif", typeof(Texture2D)) as Texture2D;
                    p.normal.textColor = new Color(0.686f, 0.686f, 0.686f);
                    p.border.left = 64;
                    p.border.right = 59;
                    p.border.top = 34;
                    p.border.bottom = 14;
                    p.padding.left = 32;
                    p.padding.top = 5;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 7 - Custom Style Foldout
                _skinSSB.customStyles[SSBStyle(SSBStyles.Foldout)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "foldoutDown.tif", typeof(Texture2D)) as Texture2D;
                    p.active.background = AssetDatabase.LoadAssetAtPath(resourcePath + "foldoutUp.tif", typeof(Texture2D)) as Texture2D;
                    p.onNormal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "foldoutUp.tif", typeof(Texture2D)) as Texture2D;
                    p.onActive.background = AssetDatabase.LoadAssetAtPath(resourcePath + "foldoutDown.tif", typeof(Texture2D)) as Texture2D;
                    p.border.left = 1;
                    p.border.right = 1;
                    p.border.top = 1;
                    p.border.bottom = 1;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedHeight = 9;
                    p.fixedWidth = 9;
                });

                //Element 8 - Custom Style FoldoutEmpty
                _skinSSB.customStyles[SSBStyle(SSBStyles.FoldoutEmpty)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "foldoutDownEmpty.tif", typeof(Texture2D)) as Texture2D;
                    p.active.background = AssetDatabase.LoadAssetAtPath(resourcePath + "foldoutUpEmpty.tif", typeof(Texture2D)) as Texture2D;
                    p.onNormal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "foldoutUpEmpty.tif", typeof(Texture2D)) as Texture2D;
                    p.onActive.background = AssetDatabase.LoadAssetAtPath(resourcePath + "foldoutDownEmpty.tif", typeof(Texture2D)) as Texture2D;
                    p.border.left = 1;
                    p.border.right = 1;
                    p.border.top = 1;
                    p.border.bottom = 1;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedHeight = 9;
                    p.fixedWidth = 9;
                });

                //Element 9 - Custom Style SelectedNode
                _skinSSB.customStyles[SSBStyle(SSBStyles.SelectedNode)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "SelectedNode_OFF.tif", typeof(Texture2D)) as Texture2D;
                    p.normal.textColor = new Color(0.686f, 0.686f, 0.686f);
                    p.onNormal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "SelectedNode_ON.tif", typeof(Texture2D)) as Texture2D;
                    p.onNormal.textColor = new Color(0.786f, 0.786f, 0.786f);
                    p.alignment = TextAnchor.MiddleCenter;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedHeight = 10;
                    p.fixedWidth = 10;
                });

                //Element 10 - Custom Style Element
                _skinSSB.customStyles[SSBStyle(SSBStyles.Element)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "Element.tif", typeof(Texture2D)) as Texture2D;
                    p.border.left = 4;
                    p.border.right = 4;
                    p.border.top = 4;
                    p.border.bottom = 7;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 11 - Custom Style Element
                _skinSSB.customStyles[SSBStyle(SSBStyles.ElementActive)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "Element_ON.tif", typeof(Texture2D)) as Texture2D;
                    p.border.left = 4;
                    p.border.right = 4;
                    p.border.top = 4;
                    p.border.bottom = 7;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 12 - Custom Style LabelOff
                _skinSSB.customStyles[SSBStyle(SSBStyles.LabelOff)].Use((p) =>
                {
                    p.padding.top = 1;
                    p.padding.bottom = 1;
                    p.margin.left = 1;
                    p.margin.right = 1;
                    p.margin.top = 1;
                    p.margin.bottom = 1;
                    p.imagePosition = ImagePosition.ImageLeft;
                    p.clipping = TextClipping.Clip;
                    p.alignment = TextAnchor.UpperLeft;
                    p.wordWrap = true;
                    p.richText = false;
                    p.font = null;//Resources.GetBuiltinResource<Font>("Arial.ttf");
                    p.fontSize = 11;
                    p.fontStyle = FontStyle.Normal;
                    p.normal.textColor = new Color(0.486f, 0.486f, 0.486f);
                });

                //Element 13 - Custom Style LinkIcon
                _skinSSB.customStyles[SSBStyle(SSBStyles.ElementLink)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "ElementLink.tif", typeof(Texture2D)) as Texture2D;
                    p.border.left = 2;
                    p.border.right = 2;
                    p.border.top = 2;
                    p.border.bottom = 2;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 14 - Custom Style LinkIconON
                _skinSSB.customStyles[SSBStyle(SSBStyles.ElementLinkON)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "ElementLink_ON.tif", typeof(Texture2D)) as Texture2D;
                    p.border.left = 2;
                    p.border.right = 2;
                    p.border.top = 2;
                    p.border.bottom = 2;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 15 - Custom Style SSBStyles.LinkNodeFBB
                _skinSSB.customStyles[SSBStyle(SSBStyles.LinkNodeFBB)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "LinkNodeFBB.tif", typeof(Texture2D)) as Texture2D;
                    p.normal.textColor = new Color(0.686f, 0.686f, 0.686f);
                    p.border.left = 63;
                    p.border.right = 60;
                    p.border.top = 34;
                    p.border.bottom = 14;
                    p.padding.left = 32;
                    p.padding.top = 5;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageLeft;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 16 - Custom Style SSBStyles.LinkNodeFBBON
                _skinSSB.customStyles[SSBStyle(SSBStyles.LinkNodeFBBON)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "LinkNodeFBB_ON.tif", typeof(Texture2D)) as Texture2D;
                    p.normal.textColor = new Color(0.686f, 0.686f, 0.686f);
                    p.border.left = 63;
                    p.border.right = 60;
                    p.border.top = 34;
                    p.border.bottom = 14;
                    p.padding.left = 32;
                    p.padding.top = 5;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageLeft;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 17 - Custom Style LinkIcon
                _skinSSB.customStyles[SSBStyle(SSBStyles.ElementLinked)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "ElementLinked.tif", typeof(Texture2D)) as Texture2D;
                    p.border.left = 2;
                    p.border.right = 2;
                    p.border.top = 2;
                    p.border.bottom = 2;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 18 - Custom Style LinkIconON
                _skinSSB.customStyles[SSBStyle(SSBStyles.ElementLinkedON)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "ElementLinked_ON.tif", typeof(Texture2D)) as Texture2D;
                    p.border.left = 2;
                    p.border.right = 2;
                    p.border.top = 2;
                    p.border.bottom = 2;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 19 - Custom Style SSBStyles.LinkedNode
                _skinSSB.customStyles[SSBStyle(SSBStyles.LinkedNode)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "LinkedNode.tif", typeof(Texture2D)) as Texture2D;
                    p.normal.textColor = new Color(0.686f, 0.686f, 0.686f);
                    p.onNormal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "LinkedNode_ON.tif", typeof(Texture2D)) as Texture2D;
                    p.onNormal.textColor = new Color(0.786f, 0.786f, 0.786f);
                    p.border.left = 63;
                    p.border.right = 60;
                    p.border.top = 34;
                    p.border.bottom = 14;
                    p.padding.left = 32;
                    p.padding.top = 5;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageLeft;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 20 - Custom Style SSBStyles.LinkNodeExpanded
                _skinSSB.customStyles[SSBStyle(SSBStyles.LinkedNodeExpanded)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "LinkedNodeExpanded.tif", typeof(Texture2D)) as Texture2D;
                    p.normal.textColor = new Color(0.686f, 0.686f, 0.686f);
                    p.onNormal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "LinkedNodeExpanded_ON.tif", typeof(Texture2D)) as Texture2D;
                    p.onNormal.textColor = new Color(0.786f, 0.786f, 0.786f);
                    p.border.left = 63;
                    p.border.right = 60;
                    p.border.top = 34;
                    p.border.bottom = 14;
                    p.padding.left = 32;
                    p.padding.top = 5;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageLeft;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 21 - Custom Style PathwayFoldup
                _skinSSB.customStyles[SSBStyle(SSBStyles.PathwayRollup)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "PathwayRollup.tif", typeof(Texture2D)) as Texture2D;
                    p.normal.textColor = new Color(0.686f, 0.686f, 0.686f);
                    p.border.left = 1;
                    p.border.right = 1;
                    p.border.top = 1;
                    p.border.bottom = 1;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });

                //Element 22 - Custom Style PathwayRollupEmpty
                _skinSSB.customStyles[SSBStyle(SSBStyles.PathwayRollupEmpty)].Use((p) =>
                {
                    p.normal.background = AssetDatabase.LoadAssetAtPath(resourcePath + "PathwayRollupEmpty.tif", typeof(Texture2D)) as Texture2D;
                    p.normal.textColor = new Color(0.686f, 0.686f, 0.686f);
                    p.border.left = 1;
                    p.border.right = 1;
                    p.border.top = 1;
                    p.border.bottom = 1;
                    p.alignment = TextAnchor.UpperLeft;
                    p.imagePosition = ImagePosition.ImageOnly;
                    p.fixedHeight = 0;
                    p.fixedWidth = 0;
                    p.stretchHeight = true;
                    p.stretchWidth = true;
                });
            }

        private static readonly Dictionary<SSBStyles, int> _style = new Dictionary<SSBStyles, int>
        {
            {SSBStyles.AnchorNode, 0},
            {SSBStyles.AnchorNodeExpanded, 1},
            {SSBStyles.LinkNode, 2},
            {SSBStyles.LinkNodeExpanded, 3},
            {SSBStyles.RedBox, 4},
            {SSBStyles.AnchorNodeFBB, 5},
            {SSBStyles.AnchorNodeFBBON, 6},
            {SSBStyles.Foldout, 7},
            {SSBStyles.FoldoutEmpty, 8},
            {SSBStyles.SelectedNode, 9},
            {SSBStyles.Element, 10},
            {SSBStyles.ElementActive, 11},
            {SSBStyles.LabelOff, 12},
            {SSBStyles.ElementLink, 13},
            {SSBStyles.ElementLinkON, 14},
            {SSBStyles.LinkNodeFBB, 15},
            {SSBStyles.LinkNodeFBBON, 16},
            {SSBStyles.ElementLinked, 17},
            {SSBStyles.ElementLinkedON, 18},
            {SSBStyles.LinkedNode, 19},
            {SSBStyles.LinkedNodeExpanded, 20},
            {SSBStyles.PathwayRollup, 21},
            {SSBStyles.PathwayRollupEmpty, 22}
        };

        public static int SSBStyle(SSBStyles _styleEnum)
        {
            return _style[_styleEnum];
        }

    }

    public static class GUIStyleExtensions
    {
        public static void Use(this GUIStyle item, Action<GUIStyle> work)
        {
            work(item);
        }
    }

    public enum SSBStyles
    {
        AnchorNode,
        AnchorNodeExpanded,
        LinkNode,
        LinkNodeExpanded,
        RedBox,
        AnchorNodeFBB,
        AnchorNodeFBBON,
        Foldout,
        FoldoutEmpty,
        SelectedNode,
        Element,
        ElementActive,
        LabelOff,
        ElementLink,
        ElementLinkON,
        LinkNodeFBB,
        LinkNodeFBBON,
        ElementLinked,
        ElementLinkedON,
        LinkedNode,
        LinkedNodeExpanded,
        PathwayRollup,
        PathwayRollupEmpty
    }

}