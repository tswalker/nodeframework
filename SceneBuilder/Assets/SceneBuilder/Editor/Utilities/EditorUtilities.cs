﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using Assets.SceneBuilder.Editor.Nodes;
using Assets.SceneBuilder.Editor.Data;

namespace Assets.SceneBuilder.Editor.Utilities
{
    [Serializable]
    public sealed class EditorUtilities : ScriptableObject
    {
        public static EventHandler<ScrollbarPropertyChangeEventArgs> ScrollbarPropertyChangeEvent;
        public static EventHandler<FrameSizeChangeEventArgs> FrameSizeChangeEvent;

        [SerializeField]
        private bool _scrollbarVActive, _scrollbarHActive, _boundaryLock, _initSnapShot;
        [SerializeField]
        private Rect _scrollbarH, _scrollbarV;
        [SerializeField]
        private float _scrollValueH, _scrollValueV, _boundaryX, _boundaryY, _boundaryZ, _boundaryW;
        [SerializeField]
        private Vector4 _boundaries;
        [SerializeField]
        private Vector2 _frameSnapShot, _frameSnapShotPrevious, _frameSnapShotOffset;

        [SerializeField]
        public int ScrollbarHControlID { get; set; }
        [SerializeField]
        public int ScrollbarVControlID { get; set; }

        public bool ScrollbarControlActive
        {
            get { return (!ScrollbarHControlID.Equals(0) | !ScrollbarVControlID.Equals(0)) ? true : false; }
        }

        public bool ScrollbarVerticalActive
        {
            get { return (!ScrollbarVControlID.Equals(0)) ? true : false;}
        }

        //private GUISkinSSB _skin;
        private float _marqueeOffset = 0f;
        private List<NodeAttribute> _nodeTypes;


        // Methods
        //
        //

        public void OnDestroy()
        {
            Debug.Log("EditorUtilities OnDestroy Called.");
        }

        public void OnDisable()
        {
            //_skin = null;
            _nodeTypes.Clear();
            _nodeTypes = null;
        }

        public void OnEnable()
        {
            hideFlags = HideFlags.HideAndDontSave;
            //if (_skin == null)
            //{
            //    _skin = new GUISkinSSB();
            //}
            
            _initSnapShot = true;

            if (_nodeTypes == null || _nodeTypes.Count().Equals(0))
            {
                _nodeTypes = BuildNodeTypeList();
            }
        }

        /// <summary>
        /// CalcNodeArea: generates a Rect representing the boundary area of all nodes on artboard
        /// </summary>
        /// <param name="nodes">List of type Node to examine</param>
        /// <returns>Rect of node space boundary</returns>
        public Rect CalcNodeArea(List<Node> nodes)
        {
            Rect _nodeArea = new Rect();
            if (nodes != null)
            {
                List<Rect> _temp = new List<Rect>();

                foreach (Node node in nodes)
                {
                    //if (node.GetType() != typeof(ToolNode))
                    //{
                    _temp.Add(node.Position);
                    //}
                }
                if (_temp.Count > 0)
                {
                    _nodeArea.xMin = _temp.Min(s => s.xMin);
                    _nodeArea.yMin = _temp.Min(s => s.yMin);
                    _nodeArea.xMax = _temp.Max(s => s.xMax);
                    _nodeArea.yMax = _temp.Max(s => s.yMax);
                }
                _temp = null;
            }
            return _nodeArea;
        }

        /// <summary>
        /// DrawGrid: generates artboard grid
        /// </summary>
        /// <param name="_factor">Scale Factor (Float)</param>
        /// <param name="_view">Rect of area to draw grid</param>
        public void DrawGrid(float _factor, Rect _view)
        {
            Handles.color = CustomColours.MediumGrey;

            float i, j = 0; //i = line step value for drawing; j = step count to change line color every 10th unit
            float _step = 10 * _factor;

            Rect _tempGrid = new Rect(0, 0, _view.width, _view.height);

            //_tempGrid.Set(0, 0, _view.width, _view.height);

            j = -1;
            //verticle grid < origion
            for (i = _view.center.x; i > _tempGrid.x; i -= _step)
            {
                j = j == 10 ? 1 : j + 1;
                Handles.color = j == 10 ? CustomColours.LighterGrey : CustomColours.MediumGrey;
                Handles.DrawLine(new Vector3(i, _tempGrid.yMin, 0), new Vector3(i, _tempGrid.yMax, 0));
            }
            j = -1;
            //verticle grid > origion
            for (i = _view.center.x; i < _tempGrid.xMax; i += _step)
            {
                j = j == 10 ? 1 : j + 1;
                Handles.color = j == 10 ? CustomColours.LighterGrey : CustomColours.MediumGrey;
                Handles.DrawLine(new Vector3(i, _tempGrid.yMin, 0), new Vector3(i, _tempGrid.yMax, 0));
            }
            j = -1;
            //horizontal grid < origion
            for (i = _view.center.y; i > _tempGrid.y; i -= _step)
            {
                j = j == 10 ? 1 : j + 1;
                Handles.color = j == 10 ? CustomColours.LighterGrey : CustomColours.MediumGrey;
                Handles.DrawLine(new Vector3(_tempGrid.xMin, i, 0), new Vector3(_tempGrid.xMax, i, 0));
            }
            j = -1;
            //horizontal grid > origion
            for (i = _view.center.y; i < _tempGrid.yMax; i += _step)
            {
                j = j == 10 ? 1 : j + 1;
                Handles.color = j == 10 ? CustomColours.LighterGrey : CustomColours.MediumGrey;
                Handles.DrawLine(new Vector3(_tempGrid.xMin, i, 0), new Vector3(_tempGrid.xMax, i, 0));
            }

            //draw origin, this is the 0,0 position of the view area
            Handles.color = CustomColours.DarkGrey;
            Handles.DrawLine(new Vector3(_tempGrid.x, _view.center.y, 0), new Vector3(_tempGrid.xMax, _view.center.y, 0)); //horizontal
            Handles.DrawLine(new Vector3(_view.center.x, _tempGrid.y, 0), new Vector3(_view.center.x, _tempGrid.yMax, 0)); //vertical
            //return _tempGrid;
        }

        /// <summary>
        /// CustomScrollBars : generates horizontal and/or vertical scrollbars
        /// </summary>
        /// <param name="_content">Content: The Rect area to be examined which holds the scrollable content.</param>
        /// <param name="_view">View: The Rect area to be considered as the frame or currently viewed region.</param>
        /// <returns>Vector4(_scrollValueH, _scrollValueV, _frameOffsetX, _frameOffsetY), 
        ///  _scrollValueH: current value for the horizontal scroll bar
        ///  _scrollValueV: current value for the vertical scoll bar
        ///  _frameOffsetX: if this value != 0, the horizontal content area no longer expands beyond the view
        ///  _frameOffsetY: if this value != 0, the vertical content area no longer expands beyond the view
        /// </returns>
        public void CustomScrollBars(Rect _content, Rect _view)
        {
            float _xMinU = _content.xMin < _view.xMin ? _content.xMin : 0;
            float _yMinU = _content.yMin < _view.yMin ? _content.yMin : 0;
            float _xMaxL = _content.xMax > _view.xMax ? _content.xMax - _view.xMax : 0;
            float _yMaxL = _content.yMax > _view.yMax ? _content.yMax - _view.yMax : 0;
            float _frameOffsetX = 0, _frameOffsetY = 0;

            _boundaries.Set(_xMinU, _yMinU, _xMaxL, _yMaxL);
            if (_boundaries.x != 0 || _boundaries.z != 0)
            {
                _scrollbarH.Set(5, _view.yMax - 15, _view.width - 21, 15);
                _scrollValueH = (float)Math.Round(GUI.HorizontalScrollbar(_scrollbarH, _scrollValueH, 0, _boundaryX, _boundaryZ, GUISkinSSB.SkinSSB.GetStyle("horizontalscrollbar")), 0, MidpointRounding.AwayFromZero);
                ScrollbarHControlID = GUIUtility.GetControlID(FocusType.Passive); //(primary = ID, thumb = ID -3, left arrow = ID -2, right arrow = ID -1)
                _scrollbarHActive = true;
            }
            else
            {
                if (_scrollbarHActive)
                {
                    _scrollbarHActive = false;
                    ScrollbarHControlID = 0;
                    GUIUtility.hotControl = 0;
                    _frameOffsetX = _scrollValueH;
                    _scrollValueH = 0;
                    _scrollbarH.Set(0, 0, 0, 0);
                    if (!_scrollbarVActive)
                    {
                        ScrollbarPropertyChangeEventArgs _event = new ScrollbarPropertyChangeEventArgs();
                        _event.Value = new Vector4(_scrollValueH, _scrollValueV, _frameOffsetX, _frameOffsetY);
                        ScrollbarPropertyChangeEvent(null, _event);
                    }
                }
            }

            if (_boundaries.y != 0 || _boundaries.w != 0)
            {
                _scrollbarV.Set(_view.xMax - 15, 5, 15, _view.height - 21);
                _scrollValueV = (float)Math.Round(GUI.VerticalScrollbar(_scrollbarV, _scrollValueV, 0, _boundaryY, _boundaryW, GUISkinSSB.SkinSSB.GetStyle("verticalscrollbar")), 0, MidpointRounding.AwayFromZero);
                ScrollbarVControlID = GUIUtility.GetControlID(FocusType.Passive); //(primary = ID, thumb = ID -3, left arrow = ID -2, right arrow = ID -1)
                _scrollbarVActive = true;
            }
            else
            {
                if (_scrollbarVActive)
                {
                    _scrollbarVActive = false;
                    ScrollbarVControlID = 0;
                    GUIUtility.hotControl = 0;
                    _frameOffsetY = _scrollValueV;
                    _scrollValueV = 0;
                    _scrollbarV.Set(0, 0, 0, 0);
                    if (!_scrollbarHActive)
                    {
                        ScrollbarPropertyChangeEventArgs _event = new ScrollbarPropertyChangeEventArgs();
                        _event.Value = new Vector4(_scrollValueH, _scrollValueV, _frameOffsetX, _frameOffsetY);
                        ScrollbarPropertyChangeEvent(null, _event);
                    }
                }
            }

            _boundaryLock = ScrollbarControlActive ? false : true;
            if (!_boundaryLock)  //only update scrollbar ranges if scrolling is not occuring
            {
                if (_scrollbarHActive && _scrollValueH == 0)
                {
                    _boundaryX = _boundaries.x;
                    _boundaryZ = _boundaries.z;
                }
                if (_scrollbarVActive && _scrollValueV == 0)
                {
                    _boundaryY = _boundaries.y;
                    _boundaryW = _boundaries.w;
                }
            }

            if (_scrollbarVActive || _scrollbarHActive)
            {
                ScrollbarPropertyChangeEventArgs _event = new ScrollbarPropertyChangeEventArgs();
                _event.Value = new Vector4(_scrollValueH, _scrollValueV, _frameOffsetX, _frameOffsetY);
                ScrollbarPropertyChangeEvent(null, _event);
            }
        }

        /// <summary>
        /// FrameSnapShot: captures current window size and returns the difference from last snap shot.
        /// </summary>
        /// <param name="_width">Width: width of current frame (ie: position.width)</param>
        /// <param name="_height">Height: height of current frame (ie: position.height)</param>
        /// <returns>Vector4(_frameSnapShotOffset.x, _frameSnapShotOffset.y, _frameOffsetX, _frameOffsetY):
        /// (note: _frameSnapShotOffset  can be used to move individual objects within seperate view or frame (ie: GUI.Window )
        /// _frameSnapShotOffset.x : horizontal transition value (session running total)
        /// _frameSnapShotOffset.y : vertical transition value (session running total)
        /// _frameOffsetX: horizontal transition value from last snap shot
        /// _frameOffsetY: vertical transition value from last snap shot
        /// </returns>
        public void FrameSnapShot(float _width, float _height)
        {
            float _x = 0, _y = 0;
            float _frameOffsetX = 0, _frameOffsetY = 0;

            _frameSnapShot.x = _width;
            _frameSnapShot.y = _height;
            if (!_frameSnapShot.Equals(_frameSnapShotPrevious))
            {
                if (!_initSnapShot)
                {
                    _x = _frameSnapShot.x - _frameSnapShotPrevious.x;
                    _y = _frameSnapShot.y - _frameSnapShotPrevious.y;
                }
                else
                {
                    _initSnapShot = false;
                }
                _frameSnapShotPrevious.Set(_frameSnapShot.x, _frameSnapShot.y);
                _frameOffsetX = (_x / 2); //half delta which applies to inner "BeginArea" type frames
                _frameOffsetY = (_y / 2);
                _frameSnapShotOffset.x += (_x / 2); //half delta "sticks" objects of inner type frames (for use with nodes and transitions)
                _frameSnapShotOffset.y += (_y / 2);

                FrameSizeChangeEventArgs _event = new FrameSizeChangeEventArgs();
                _event.Value = new Vector4(_frameSnapShotOffset.x, _frameSnapShotOffset.y, _frameOffsetX, _frameOffsetY);
                FrameSizeChangeEvent(null, _event);
            }
        }

        /// <summary>
        /// BuildNodeTypeList: generates available node types in assembly
        /// </summary>
        /// <returns>List of NodeAttribute for node types available in assembly</returns>
        private static List<NodeAttribute> BuildNodeTypeList()
        {
            List<NodeAttribute> _temp = new List<NodeAttribute>();
            var types = AppDomain.CurrentDomain.GetAssemblies().ToList().SelectMany(s => s.GetTypes());
            foreach (var type in types)
            {
                foreach (var attribute in Attribute.GetCustomAttributes(type).OfType<NodeAttribute>())
                {
                    _temp.Add(attribute);
                }
            }
            return _temp;
        }

        /// <summary>
        /// BuildMainContextMenu: Generates context menu for Main of node types
        /// </summary>
        /// <param name="_callback">Callback into Main for handling context selection parse</param>
        /// <param name="_nodes">Nodes currently available on the artboard</param>
        public void BuildMainContextMenu(GenericMenu.MenuFunction2 _callback, List<Node> _nodes)
        {
            GenericMenu _menu = new GenericMenu();
            _menu.AddSeparator("Nodes:");
            foreach (NodeAttribute attribute in _nodeTypes)
            {
                _menu.AddItem(new GUIContent("Create Node/" + attribute.DisplayName))
                    .Visible(true)
                    .Enable(() =>
                    {
                        if (attribute.Category == NodeCategory.LOCATORS)
                        {
                            if (_nodes != null)
                            {
                                return _nodes.Find(x => x.GetType().Equals(typeof(SceneNode)));
                            }
                            else
                            {
                                return false;
                            }

                        }
                        else { return true; }
                    })
                    .Action(_callback, new ActionCommand(ActionIdentifier.ADD, attribute));
            }
            _menu.AddItem("Remove Node/")
                .Visible(true)
                .Enable(true);
            _menu.AddItem("Remove Node/All")
                .Visible(true)
                .Enable(() =>
                {
                    if (_nodes == null)
                    {
                        return false;
                    }
                    return _nodes.Count > 0;
                })
                .Action(_callback, new ActionCommand(ActionIdentifier.REMOVE, ActionTarget.ALL));
            _menu.AddItem("Remove Node/Selected")
                .Visible(true)
                .Enable(() =>
                {
                    if (_nodes == null)
                    {
                        return false;
                    }
                    return _nodes.Find(x => x.NodeSelected);
                })
                .Action(_callback, new ActionCommand(ActionIdentifier.REMOVE, ActionTarget.SELECTED));
            _menu.ShowAsContext();
        }

        /// <summary>
        /// DrawMarquee : Draws selection marquee
        /// </summary>
        /// <param name="_marquee">Rect of area to draw edges as marquee</param>
        public void DrawMarquee(Rect _marquee)
        {
            int _marqLength = 4;
            if (_marqueeOffset >= (2 * _marqLength))
            {
                _marqueeOffset = 0;
            }

            if (_marquee.xMax < _marquee.x) //decrement operations for horizontal
            {
                for (float i = 0; i >= (_marquee.width + (2 * _marqLength)); i -= (_marqLength * 2)) //subdivide width 
                {
                    DrawLine(new Vector2(_marquee.x + i - _marqueeOffset, _marquee.y), new Vector2(_marquee.x + i + _marqLength - _marqueeOffset, _marquee.y), Direction.Horizontal, CustomColours.LightestGrey);
                    DrawLine(new Vector2(_marquee.x + i - _marqueeOffset, _marquee.yMax), new Vector2(_marquee.x + i + _marqLength - _marqueeOffset, _marquee.yMax), Direction.Horizontal, CustomColours.LightestGrey);
                }
            }
            else //increment
            {
                for (float i = 0; i <= (_marquee.width - (2 * _marqLength)); i += (_marqLength * 2)) //subdivide width  
                {
                    DrawLine(new Vector2(_marquee.x + i + _marqueeOffset, _marquee.y), new Vector2(_marquee.x + i + _marqLength + _marqueeOffset, _marquee.y), Direction.Horizontal, CustomColours.LightestGrey);
                    DrawLine(new Vector2(_marquee.x + i + _marqueeOffset, _marquee.yMax), new Vector2(_marquee.x + i + _marqLength + _marqueeOffset, _marquee.yMax), Direction.Horizontal, CustomColours.LightestGrey);
                }
            }
            if (_marquee.yMax < _marquee.y) //decrement operation for height
            {
                for (float i = 0; i >= (_marquee.height + (2 * _marqLength)); i -= (_marqLength * 2)) //subdivide height  
                {
                    DrawLine(new Vector2(_marquee.x, _marquee.y + i - _marqueeOffset), new Vector2(_marquee.x, _marquee.y + i + _marqLength - _marqueeOffset), Direction.Vertical, CustomColours.LightestGrey);
                    DrawLine(new Vector2(_marquee.xMax, _marquee.y + i - _marqueeOffset), new Vector2(_marquee.xMax, _marquee.y + i + _marqLength - _marqueeOffset), Direction.Vertical, CustomColours.LightestGrey);
                }
            }
            else //increment
            {
                for (float i = 0; i <= (_marquee.height - (2 * _marqLength)); i += (_marqLength * 2)) //subdivide height  
                {
                    DrawLine(new Vector2(_marquee.x, _marquee.y + i + _marqueeOffset), new Vector2(_marquee.x, _marquee.y + i + _marqLength + _marqueeOffset), Direction.Vertical, CustomColours.LightestGrey);
                    DrawLine(new Vector2(_marquee.xMax, _marquee.y + i + _marqueeOffset), new Vector2(_marquee.xMax, _marquee.y + i + _marqLength + _marqueeOffset), Direction.Vertical, CustomColours.LightestGrey);
                }
            }
            //corner boxes
            if (_marquee.width > 0 && _marquee.height > 0) //lower right quadrant drag
            {
                GUI.DrawTexture(new Rect(_marquee.x - 3, _marquee.y - 3, SSBImages.MarqueeBox.width, SSBImages.MarqueeBox.height), SSBImages.MarqueeBox, ScaleMode.ScaleToFit, true);
                GUI.DrawTexture(new Rect(_marquee.xMax, _marquee.y - 3, SSBImages.MarqueeBox.width, SSBImages.MarqueeBox.height), SSBImages.MarqueeBox, ScaleMode.ScaleToFit, true);
                GUI.DrawTexture(new Rect(_marquee.x - 3, _marquee.yMax, SSBImages.MarqueeBox.width, SSBImages.MarqueeBox.height), SSBImages.MarqueeBox, ScaleMode.ScaleToFit, true);
                GUI.DrawTexture(new Rect(_marquee.xMax, _marquee.yMax, SSBImages.MarqueeBox.width, SSBImages.MarqueeBox.height), SSBImages.MarqueeBox, ScaleMode.ScaleToFit, true);
            }
            else if (_marquee.width < 0 && _marquee.height > 0) //lower left quadrant drag
            {
                GUI.DrawTexture(new Rect(_marquee.x, _marquee.y - 3, SSBImages.MarqueeBox.width, SSBImages.MarqueeBox.height), SSBImages.MarqueeBox, ScaleMode.ScaleToFit, true);
                GUI.DrawTexture(new Rect(_marquee.x + 1, _marquee.yMax + 1, SSBImages.MarqueeBox.width, SSBImages.MarqueeBox.height), SSBImages.MarqueeBox, ScaleMode.ScaleToFit, true);
                GUI.DrawTexture(new Rect(_marquee.xMax - 4, _marquee.yMax + 1, SSBImages.MarqueeBox.width, SSBImages.MarqueeBox.height), SSBImages.MarqueeBox, ScaleMode.ScaleToFit, true);
                GUI.DrawTexture(new Rect(_marquee.xMax - 4, _marquee.y - 3, SSBImages.MarqueeBox.width, SSBImages.MarqueeBox.height), SSBImages.MarqueeBox, ScaleMode.ScaleToFit, true);
            }
            else if (_marquee.width < 0 && _marquee.height < 0) //upper left quadrant drag
            {
                GUI.DrawTexture(new Rect(_marquee.x + 1, _marquee.y + 1, SSBImages.MarqueeBox.width, SSBImages.MarqueeBox.height), SSBImages.MarqueeBox, ScaleMode.ScaleToFit, true);
                GUI.DrawTexture(new Rect(_marquee.xMax - 5, _marquee.y + 1, SSBImages.MarqueeBox.width, SSBImages.MarqueeBox.height), SSBImages.MarqueeBox, ScaleMode.ScaleToFit, true);
                GUI.DrawTexture(new Rect(_marquee.xMax - 4, _marquee.yMax - 4, SSBImages.MarqueeBox.width, SSBImages.MarqueeBox.height), SSBImages.MarqueeBox, ScaleMode.ScaleToFit, true);
                GUI.DrawTexture(new Rect(_marquee.x + 1, _marquee.yMax - 5, SSBImages.MarqueeBox.width, SSBImages.MarqueeBox.height), SSBImages.MarqueeBox, ScaleMode.ScaleToFit, true);
            }
            else if (_marquee.width > 0 && _marquee.height < 0) //upper right quadrant drag
            {
                GUI.DrawTexture(new Rect(_marquee.x - 3, _marquee.y + 1, SSBImages.MarqueeBox.width, SSBImages.MarqueeBox.height), SSBImages.MarqueeBox, ScaleMode.ScaleToFit, true);
                GUI.DrawTexture(new Rect(_marquee.x - 3, _marquee.yMax - 4, SSBImages.MarqueeBox.width, SSBImages.MarqueeBox.height), SSBImages.MarqueeBox, ScaleMode.ScaleToFit, true);
                GUI.DrawTexture(new Rect(_marquee.xMax + 1, _marquee.yMax - 4, SSBImages.MarqueeBox.width, SSBImages.MarqueeBox.height), SSBImages.MarqueeBox, ScaleMode.ScaleToFit, true);
                GUI.DrawTexture(new Rect(_marquee.xMax + 1, _marquee.y + 1, SSBImages.MarqueeBox.width, SSBImages.MarqueeBox.height), SSBImages.MarqueeBox, ScaleMode.ScaleToFit, true);
            }

            _marqueeOffset += 0.025f;

        }

        /// <summary>
        /// DrawLine: generates a simple line
        /// </summary>
        /// <param name="start">Vector2 Start position of line</param>
        /// <param name="end">Vector2 End position of line</param>
        /// <param name="_direction">Direction enum (Horizontal, Vertical)</param>
        /// <param name="_color">Line color</param>
        public void DrawLine(Vector2 start, Vector2 end, Direction _direction, Color _color)
        {
            Vector3 startTan = new Vector3();
            Vector3 endTan = new Vector3();

            switch (_direction)
            {
                case Direction.Horizontal:
                    {
                        startTan = new Vector3(start.x, start.y, 0) + Vector3.right;
                        endTan = new Vector3(end.x, end.y, 0) + Vector3.left;
                        break;
                    }
                case Direction.Vertical:
                    {
                        startTan = new Vector3(start.x, start.y, 0) + Vector3.up;
                        endTan = new Vector3(end.x, end.y, 0) + Vector3.down;
                        break;
                    }
            }

            Handles.DrawBezier(new Vector3(start.x, start.y, 0), new Vector3(end.x, end.y, 0), startTan, endTan, _color, SSBImages.MarqLine, 0.5f);
        }

        /// <summary>
        /// DrawAALine: generates a smooth line of certain width
        /// </summary>
        /// <param name="start">Start (Vector2)</param>
        /// <param name="end">End (Vector2)</param>
        /// <param name="_direction">Direction enum (Horizontal, Vertical)</param>
        /// <param name="_color">Line color (Color)</param>
        /// <param name="_width">Line width (float)</param>
        public void DrawAALine(Vector2 start, Vector2 end, Direction _direction, Color _color, float _width)
        {
            Vector3 startTan = new Vector3();
            Vector3 endTan = new Vector3();

            switch (_direction)
            {
                case Direction.Horizontal:
                    {
                        startTan = new Vector3(start.x, start.y, 0) + Vector3.right;
                        endTan = new Vector3(end.x, end.y, 0) + Vector3.left;
                        break;
                    }
                case Direction.Vertical:
                    {
                        startTan = new Vector3(start.x, start.y, 0) + Vector3.up;
                        endTan = new Vector3(end.x, end.y, 0) + Vector3.down;
                        break;
                    }
            }

            Handles.DrawBezier(new Vector3(start.x, start.y, 0), new Vector3(end.x, end.y, 0), startTan, endTan, _color, SSBImages.AALine, _width);
        }

        public void DrawBezierAACurve(Vector3 start, Vector3 end, Vector3 startTan, Vector3 endTan, Color _color, float _width)
        {
            //Vector3 startTan = new Vector3(p1.x,p1.y,0) + Vector3.right * 10;
            //Vector3 endTan = new Vector3(p2.x,p2.y,0) + Vector3.left * 10;
            Handles.DrawBezier(start, end, startTan, endTan, _color, SSBImages.AALine, _width);
        }
        public void DrawConnectoidPaths(List<ConnectoidPath> connectoids, float horizontalOffset, float verticalOffset)
        {
            foreach (ConnectoidPath connectoid in connectoids)
            {
                float chamfer = 10f;
                Color _lineColor;
                _lineColor = connectoid.Active ? CustomColours.ConnectorActiveGrey : CustomColours.ConnectorInActiveGrey;

                //DEBUG BLUE LINE
                //for (int i = 0; i < connectoid.Path.Count() - 1; i++)
                //{
                //    DrawAALine(new Vector2(connectoid.Path[i].x + horizontalOffset, connectoid.Path[i].y + verticalOffset), new Vector2(connectoid.Path[i + 1].x + horizontalOffset, connectoid.Path[i + 1].y + verticalOffset), Direction.Horizontal, Color.red, 0.5f);
                //}

                if (connectoid.Path.Count() > 4)
                {  // 6 point bezier
                    Vector2 p0 = new Vector2(connectoid.Path[0].x + horizontalOffset, connectoid.Path[0].y + verticalOffset);
                    Vector2 p1 = new Vector2(connectoid.Path[1].x + horizontalOffset, connectoid.Path[1].y + verticalOffset);
                    Vector2 p2 = new Vector2(connectoid.Path[2].x + horizontalOffset, connectoid.Path[2].y + verticalOffset);
                    Vector2 p3 = new Vector2(connectoid.Path[3].x + horizontalOffset, connectoid.Path[3].y + verticalOffset);
                    Vector2 p4 = new Vector2(connectoid.Path[4].x + horizontalOffset, connectoid.Path[4].y + verticalOffset);
                    Vector2 p5 = new Vector2(connectoid.Path[5].x + horizontalOffset, connectoid.Path[5].y + verticalOffset);

                    Vector2 c1 = new Vector2(p1.x + 10f, p1.y);
                    Vector2 c2;
                    Vector2 c3;
                    Vector2 c4;
                    Vector2 c5;
                    Vector2 c6;
                    Vector2 c7;

                    if (p1.y < p2.y)
                    {

                        c2 = new Vector2(p1.x, p1.y + chamfer);
                        c3 = new Vector2(p2.x, p2.y - chamfer);
                    }
                    else
                    {
                        c2 = new Vector2(p1.x, p1.y - chamfer);
                        c3 = new Vector2(p2.x, p2.y + chamfer);
                    }

                    float mChamfer = chamfer;
                    if (Mathf.Abs(p1.x - p3.x) < (2 * chamfer))
                    {
                        mChamfer = Mathf.Abs(p1.x - p3.x) / 2;
                    }
                    c4 = new Vector2(p2.x + mChamfer, p2.y);
                    c5 = new Vector2(p3.x - mChamfer, p3.y);

                    if (p3.y < p4.y)
                    {

                        c6 = new Vector2(p3.x, p3.y + chamfer);
                        c7 = new Vector2(p4.x, p4.y - chamfer);
                    }
                    else
                    {
                        c6 = new Vector2(p3.x, p3.y - chamfer);
                        c7 = new Vector2(p4.x, p4.y + chamfer);
                    }
                    
                    Vector2 c8 = new Vector2(p4.x - 10f, p5.y);

                    DrawAALine(p0, c1, Direction.Horizontal, _lineColor, 1.55f);
                    DrawAALine(c2, c3, Direction.Horizontal, _lineColor, 1.55f);
                    DrawAALine(c4, c5, Direction.Horizontal, _lineColor, 1.55f);
                    DrawAALine(c6, c7, Direction.Horizontal, _lineColor, 1.55f);
                    DrawAALine(c8, p5, Direction.Horizontal, _lineColor, 1.55f);

                    Vector3 startTan = new Vector3(p1.x, p1.y, 0);
                    Vector3 end = new Vector3(c2.x, c2.y, 0);

                    Vector3 endTan;
                    endTan = Vector3.RotateTowards(c2, p1, 0f, 1f);
                    DrawBezierAACurve(c1, end, startTan, endTan, _lineColor, 1.55f);

                    startTan = new Vector3(p2.x, p2.y, 0);
                    endTan = Vector3.RotateTowards(c4, p2, 0f, 1f);
                    DrawBezierAACurve(c3, c4, startTan, endTan, _lineColor, 1.55f);

                    startTan = new Vector3(p3.x, p3.y, 0);
                    endTan = Vector3.RotateTowards(c6, p3, 0f, 1f);
                    DrawBezierAACurve(c5, c6, startTan, endTan, _lineColor, 1.55f);

                    startTan = new Vector3(p4.x, p4.y, 0);
                    endTan = Vector3.RotateTowards(c8, p4, 0f, 1f);
                    DrawBezierAACurve(c7, c8, startTan, endTan, _lineColor, 1.55f);
                }
                else //4 point
                {
                    Vector2 p0 = new Vector2(connectoid.Path[0].x + horizontalOffset, connectoid.Path[0].y + verticalOffset);
                    Vector2 p1 = new Vector2(connectoid.Path[1].x + horizontalOffset, connectoid.Path[1].y + verticalOffset);
                    Vector2 p2 = new Vector2(connectoid.Path[2].x + horizontalOffset, connectoid.Path[2].y + verticalOffset);
                    Vector2 p3 = new Vector2(connectoid.Path[3].x + horizontalOffset, connectoid.Path[3].y + verticalOffset);

                    Vector2 c1 = new Vector2(p1.x + 10f, p1.y);
                    Vector2 c2;
                    Vector2 c3;

                    if (Mathf.Abs(p0.y - p3.y) < (2*chamfer))
                    {
                        chamfer = Mathf.Abs(p0.y - p3.y)/2;
                    }
                    if (p1.y < p2.y)
                    {

                        c2 = new Vector2(p1.x, p1.y + chamfer);
                        c3 = new Vector2(p2.x, p2.y - chamfer);
                    }
                    else
                    {
                        c2 = new Vector2(p1.x, p1.y - chamfer);
                        c3 = new Vector2(p2.x, p2.y + chamfer);
                    }
                    
                    Vector2 c4 = new Vector2(p2.x - 10f, p2.y);  //adding offset per pathway count

                    DrawAALine(p0, c1, Direction.Horizontal, _lineColor, 1.55f);
                    DrawAALine(c2, c3, Direction.Horizontal, _lineColor, 1.55f);
                    DrawAALine(p3, c4, Direction.Horizontal, _lineColor, 1.55f);

                    Vector3 start = new Vector3(c1.x, c1.y, 0);
                    Vector3 startTan = new Vector3(p1.x, p1.y, 0);
                    Vector3 end = new Vector3(c2.x, c2.y, 0);

                    Vector3 endTan;
                    endTan = Vector3.RotateTowards(c2, p1, 0f, 1f);
                    DrawBezierAACurve(start, end, startTan, endTan, _lineColor, 1.55f);

                    startTan = new Vector3(p2.x, p2.y, 0);
                    endTan = Vector3.RotateTowards(c4, p2, 0f, 1f);
                    DrawBezierAACurve(c3, c4, startTan, endTan, _lineColor, 1.55f);

                }
            }
        }
    }

    public class ScrollbarPropertyChangeEventArgs : EventArgs
    {
        public Vector4 Value { get; set; }
    }

    public class FrameSizeChangeEventArgs : EventArgs
    {
        public Vector4 Value { get; set; }
    }

    public enum Direction
    {
        Horizontal,
        Vertical
    }
}
