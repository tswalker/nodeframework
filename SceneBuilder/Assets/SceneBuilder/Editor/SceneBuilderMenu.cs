﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using Assets.SceneBuilder.Editor.Nodes;
using Assets.SceneBuilder.Editor.Panels;
using Assets.SceneBuilder.Editor.Utilities;
using Assets.SceneBuilder.Editor.Providers;
using Assets.SceneBuilder.Editor.Data;

namespace Assets.SceneBuilder.Editor
{
    public class SceneBuilderMenu : EditorWindow
    {
        //private EditorApplication.CallbackFunction _editorCallBack;

        static SceneBuilderMenu window;
        private int _mainwindowID, _artBoardID, _frameAreaID;
        private Rect _menuArea, _frameArea, _artBoard, _nodeArea, _nodeAreaPositions, _mainScrollFrame, _mainScrollView;
        private Vector2 _contextMousePosition, _frameSnapShot, _frameSnapShotPrevious, _frameSnapShotOffset, _mainScrollOffset;
        private Vector4 _snapShot, _snapShotOffset;
        private float _zoomFactor = 1;
        private float _hSize, _vSize, _boundaryX, _boundaryY, _boundaryZ, _boundaryW, _frameOffsetX, _frameOffsetY, _scrollValueH, _scrollValueV;
        private float _tXSnapShot, _tYSnapShot; //_tXOffset, _tYOffset, 
        private bool _boundaryLock, _scrollbarHActive, _scrollbarVActive, _unfocus;
        private bool _toolBoxState = true;
        private bool _inspectorPanelState = false;

        private bool _marqueeActive, _marqueeON;
        private Vector2 _marqueeSTART, _marqueeEND;
        private Rect _marquee;

        private List<Panel> panelList;

        private NodeProvider nodeProvider;
        private PanelProvider panelProvider;

        [SerializeField]
        private EditorUtilities Utilities;

        private DataController _dac;

        //private static GUISkinSSB Skin;

        private bool _onLinkActive = false;
        private bool _onPanning = false;
        private Texture2D _cursorConnectoid, _cursorHandIcon;
        private MouseCursor _cursor;

        [MenuItem("Scene Builder/Editor Window")]
        static void Init()
        {
            window = EditorWindow.GetWindow<SceneBuilderMenu>();
            window.title = "Scene Builder";
            window.minSize = new Vector2(640, 480);
            window.Initialize();
            
        }

        [MenuItem("Scene Builder/Editor Window Close")]
        static void Abort()
        {
            window = EditorWindow.GetWindow<SceneBuilderMenu>("Scene Builder");
            window.Close();
        }

        public void Initialize()
        {
            _frameArea = new Rect();
            _artBoard = new Rect();
            _menuArea = new Rect();
            _mainScrollFrame = new Rect();
            _frameOffsetX = 0;
            _frameOffsetY = 0;
            _scrollValueH = 0;
            _scrollValueV = 0;
        }

        void OnDisable()
        {
            //EditorApplication.update -= _editorCallBack;
            Linker.OnStartLink -= AckOnLink;
            Linker.OnCloseLink -= AckOnClose;
            DataController.DataPropertyChangeEvent -= OnDataPropertyChange;
            EditorUtilities.ScrollbarPropertyChangeEvent -= OnScrollbarPropertyChange;
            EditorUtilities.FrameSizeChangeEvent -= OnFrameSizeChange;
        }

        void OnEnable()
        {
            //if (Skin == null)
            //{
            //    Skin = new GUISkinSSB();
            //}

            Linker.OnStartLink += AckOnLink;
            Linker.OnCloseLink += AckOnClose;

            _cursorConnectoid = AssetDatabase.LoadAssetAtPath("Assets/SceneBuilder/Editor/Gizmos/Connectoid.tif", typeof(Texture2D)) as Texture2D;
            _cursorHandIcon = AssetDatabase.LoadAssetAtPath("Assets/SceneBuilder/Editor/Gizmos/HandIcon.tif", typeof(Texture2D)) as Texture2D;

            //setup necessary libs
            if (Utilities == null)
            {
                Utilities = CreateInstance<EditorUtilities>();
            }
            EditorUtilities.ScrollbarPropertyChangeEvent += OnScrollbarPropertyChange;
            EditorUtilities.FrameSizeChangeEvent += OnFrameSizeChange;

            //_snapShot = Utilities.FrameSnapShot(position.width, position.height);
            _cursor = MouseCursor.Arrow;

            //NOTE: null-coalescing operator does not work on these!
            if (nodeProvider == null)
            {
                nodeProvider = new NodeProvider();
            }
            if (panelProvider == null)
            {
                panelProvider = new PanelProvider();
            }

            if (_dac == null)
            {
                _dac = CreateInstance<DataController>();
            }
            _dac.EstablishAllNodesProvider(ref nodeProvider); //required after provider generation
            DataController.DataPropertyChangeEvent += OnDataPropertyChange;

            if (panelList == null)
            {
                panelList = new List<Panel>();
            }

            //setup STATIC node panel
            if (panelList.Count == 0) //was re-created or new
            {
                ToolPanel _toolPanel = CreateInstance<ToolPanel>();
                _toolPanel.Initialize(new Vector2(0, 1), "Toolbox", new Vector2(110, 300));
                panelList.Add(_toolPanel);

                InspectorPanel _inspectorPanel = CreateInstance<InspectorPanel>();
                _inspectorPanel.Initialize(new Vector2(0, 1), "Inspector",new Vector2(180,300));
                panelList.Add(_inspectorPanel);

            }

            foreach (Panel panel in panelList)
            {
                if (!panel.Subscribed)
                {
                    panel.Subscribe(panelProvider);
                }
            }

            //_editorCallBack = new EditorApplication.CallbackFunction(FrameSnapShot);
            //EditorApplication.update += _editorCallBack;
        }

        //_tXOffset = _frameOffsetX + _frameSnapShotOffset.x - _scrollValueH;
        //_tYOffset = _frameOffsetY + _frameSnapShotOffset.y - _scrollValueV;

        private void OnFrameSizeChange(object sender, FrameSizeChangeEventArgs e)
        {
            FrameSizeChangeEventArgs _event = e as FrameSizeChangeEventArgs;
            _frameOffsetX += _event.Value.z != 0 ? _event.Value.z : 0;
            _frameOffsetY += _event.Value.w != 0 ? _event.Value.w : 0;
            _frameSnapShotOffset.x = _event.Value.x;
            _frameSnapShotOffset.y = _event.Value.y;
            if (nodeProvider != null)
            {
                nodeProvider.Notify(NodeMessageSignature.Translate, new NodeMessageData(new Vector2(_frameOffsetX + _frameSnapShotOffset.x - _scrollValueH, _frameOffsetY + _frameSnapShotOffset.y - _scrollValueV)));
            }
        }

        private void OnScrollbarPropertyChange(object sender, ScrollbarPropertyChangeEventArgs e)
        {
            ScrollbarPropertyChangeEventArgs _event = e as ScrollbarPropertyChangeEventArgs;
            _frameOffsetX -= _event.Value.z != 0 ? _event.Value.z : 0;
            _frameOffsetY -= _event.Value.w != 0 ? _event.Value.w : 0;
            _scrollValueH = _event.Value.x;
            _scrollValueV = _event.Value.y;
            if (nodeProvider != null)
            {
                nodeProvider.Notify(NodeMessageSignature.Translate, new NodeMessageData(new Vector2(_frameOffsetX + _frameSnapShotOffset.x - _scrollValueH, _frameOffsetY + _frameSnapShotOffset.y - _scrollValueV)));
            }
        }

        private void OnDataPropertyChange(object sender, EventArgs e)
        {
            DataPropertyChangeEventArgs _event = e as DataPropertyChangeEventArgs;
            if (_event != null)
            {
                switch (_event.Name)
                {
                    case "CursorChange":
                        {
                            if (_event.State)
                            {
                                _cursor = MouseCursor.ResizeVertical;
                                _onPanning = false;
                            }
                            else
                            {
                                _cursor = MouseCursor.Arrow;
                            }
                            break;
                        }
                }
            }
            else
            {
                Debug.LogWarning("DataPropertyChangeEvent: Null Event Raised Warning.");
            }
        }


        private void AckOnLink(object sender, EventArgs e)
        {
            _onLinkActive = true;
        }

        private void AckOnClose(object sender, EventArgs e)
        {
            _onLinkActive = false;
            _cursor = MouseCursor.Arrow;
        }



        void OnGUI()
        {
            GUI.skin = GUISkinSSB.SkinSSB;  //required for Unity to deal with automatic skin usage such as scrollbar buttons
            if (_onLinkActive)
            {
                Cursor.SetCursor(_cursorConnectoid, new Vector2(0, 0), CursorMode.Auto);
                _cursor = MouseCursor.CustomCursor;
            }
            if (_onPanning)
            {
                Cursor.SetCursor(_cursorHandIcon, new Vector2(0, 0), CursorMode.Auto);
                _cursor = MouseCursor.CustomCursor;
            }

            _mainwindowID = GUIUtility.GetControlID(this.GetInstanceID(), FocusType.Passive, position);
            //menu - toggle bar area
            _menuArea.Set(0, 0, position.width, EditorStyles.toolbarButton.fixedHeight);
            GUILayout.BeginArea(_menuArea);
            {
                GUILayout.BeginHorizontal(EditorStyles.toolbar);
                GUILayout.FlexibleSpace();
                _toolBoxState = GUILayout.Toggle(_toolBoxState, "Tool Box", EditorStyles.toolbarButton);
                _inspectorPanelState = GUILayout.Toggle(_inspectorPanelState, "Inspector Panel", EditorStyles.toolbarButton);
                GUILayout.EndHorizontal();
            }
            GUILayout.EndArea();

            //_frameArea is the editor window space
            _frameArea.Set(0, EditorStyles.toolbarButton.fixedHeight, position.width, position.height);

            GUILayout.BeginArea(_frameArea);
            {
                EditorGUIUtility.AddCursorRect(_frameArea, _cursor);  //required for cursor changes

                //_artBoard is the region within the frame minus togglebar
                _artBoard.Set(_frameOffsetX - _scrollValueH, _frameOffsetY - EditorStyles.toolbarButton.fixedHeight - _scrollValueV, _frameArea.xMax, _frameArea.yMax);
                _nodeAreaPositions = Utilities.CalcNodeArea(_dac.GetNodeList());
                Utilities.DrawGrid(_zoomFactor, _artBoard);

                //draw node -> element paths
                //Utilities.DrawConnectoidPaths(_dac.GetPaths(), _tXOffset , _tYOffset - EditorStyles.toolbarButton.fixedHeight);
                Utilities.DrawConnectoidPaths(_dac.GetPaths(), 0, 0 - EditorStyles.toolbarButton.fixedHeight);

                BeginWindows();
                {
                    foreach (Panel panel in panelList)
                    {
                        if (panel.GetType().Equals(typeof(ToolPanel)) && _toolBoxState)
                        {
                            panel.Position = GUI.Window(panel.PanelID, panel.Position, panel.DrawPanelWindow, panel.Title, panel.GetStyle);
                        }
                        else
                        {
                            if (panel.GetType().Equals(typeof(InspectorPanel)) && _inspectorPanelState)
                            {
                                float _shift = 0;
                                //do a messaging to translate and include event for frameresize handling to place inspector in correct position on artboard
                                if (!panel.StaticPositionLocked)
                                {
                                    panel.SetStaticPosition = new Vector2(position.width - panel.Position.width, 0);
                                }
                                if (Utilities.ScrollbarVerticalActive) { _shift = 15f; }
                                panelProvider.Notify(PanelMessageSignature.Translate, new PanelMessageData(new Vector2((_frameSnapShotOffset.x * 2) - _shift, 0)));
                                panel.Position = GUI.Window(panel.PanelID, panel.Position, panel.DrawPanelWindow, panel.Title, panel.GetStyle);
                            }
                        }
                    }
                    foreach (Node node in _dac.GetNodeList())
                    {
                        node.Position = GUI.Window(node.NodeWindowID, node.Position, node.DrawNodeWindow, "", node.GetStyle);
                    }
                }
                EndWindows();

                _mainScrollFrame.Set(0, 0, _frameArea.width - EditorStyles.toolbarButton.fixedWidth, _frameArea.height - EditorStyles.toolbarButton.fixedHeight);
                Utilities.CustomScrollBars(_nodeAreaPositions, _mainScrollFrame);
                Utilities.FrameSnapShot(position.width, position.height); //used to "sticky" artboard position while resizing main window

                //_tXOffset = _frameOffsetX + _frameSnapShotOffset.x - _scrollValueH;
                //_tYOffset = _frameOffsetY + _frameSnapShotOffset.y - _scrollValueV;

            }//end _frameArea
            GUILayout.EndArea();

            //handle events
            HandleEvents(Event.current);
            Repaint();
        }

        private void HandleEvents(Event e)
        {
            //prep for possible marquee action
            if (!_marqueeActive)
            {
                _marqueeSTART.Set(e.mousePosition.x, e.mousePosition.y);
                _marqueeEND.Set(0, 0);
            }

            //drawing temporary connectoid
            if (_onLinkActive)
            {
                //draw line from connectoid origination to current mouse position
                nodeProvider.Notify(NodeMessageSignature.Lock, new NodeMessageData(true));
                //Utilities.DrawAALine(new Vector2(Linker.SourcePoint.x, Linker.SourcePoint.y), new Vector2(e.mousePosition.x, e.mousePosition.y), Direction.Horizontal, customColor.LightestGrey, 2.33f);
                Utilities.DrawConnectoidPaths(_dac.GenerateLivePath(e.mousePosition), 0, 0);
                _dac.ReleaseLivePath();
            }

            //detects for connectoid cancel action (mouse down in artboard)
            if (e.type == EventType.MouseDown && _onLinkActive)
            {
                nodeProvider.Notify(NodeMessageSignature.Lock, new NodeMessageData(false));
                Linker.CloseLink(NodeUtilities.EmptyGUID, Vector2.zero);
                //e.Use();
            }

            //detects possible start of marquee
            if (e.type == EventType.MouseDown && e.modifiers != EventModifiers.Alt && (e.button == 0) && !_onLinkActive)
            {
                nodeProvider.Notify(NodeMessageSignature.Lock, new NodeMessageData(true));
                if (!_marqueeActive && _marqueeON)
                {
                    nodeProvider.Notify(NodeMessageSignature.Focus, new NodeMessageData(ActionTarget.OFF));
                }
                _marqueeON = true; //begin a marquee?
                nodeProvider.Notify(NodeMessageSignature.Marquis, new NodeMessageData("Marqing"));
            }
            if (e.type == EventType.MouseDrag && e.modifiers != EventModifiers.Alt)
            {
                nodeProvider.Notify(NodeMessageSignature.Lock, new NodeMessageData(true));
                if (_marqueeON) //ok, start drawing a marquee grab contact point for START just once.
                {
                    _marqueeActive = true;
                    _marqueeSTART = e.mousePosition;
                }
                if (_marqueeActive)
                {
                    _marqueeON = false;
                    _marqueeEND += e.delta;
                    _marquee.Set(_marqueeSTART.x, _marqueeSTART.y, _marqueeEND.x, _marqueeEND.y);
                }
            }
            if (_marqueeActive)
            {
                Utilities.DrawMarquee(_marquee);
            }


            //pan cursor changes in _frameArea ALT+leftMouseDrag
            if (e.modifiers == EventModifiers.Alt && _frameArea.Contains(e.mousePosition))
            {
                if (e.type == EventType.MouseDrag)
                {
                    nodeProvider.Notify(NodeMessageSignature.Lock, new NodeMessageData(false));
                    nodeProvider.Notify(NodeMessageSignature.Focus, new NodeMessageData(ActionTarget.OFF));
                    _frameOffsetX += e.delta.x;
                    _frameOffsetY += e.delta.y;
                    e.Use();
                    _onPanning = true;
                }
                if (e.type == EventType.MouseUp)
                {
                    nodeProvider.Notify(NodeMessageSignature.Focus, new NodeMessageData(ActionTarget.ON));
                    _cursor = MouseCursor.Arrow;
                    _onPanning = false;
                    //debugger
                }
                if (e.type == EventType.MouseDown)
                {
                    nodeProvider.Notify(NodeMessageSignature.Focus, new NodeMessageData(ActionTarget.OFF));
                }
                nodeProvider.Notify(NodeMessageSignature.Translate, new NodeMessageData(new Vector2(_frameOffsetX + _frameSnapShotOffset.x - _scrollValueH, _frameOffsetY + _frameSnapShotOffset.y - _scrollValueV)));
            }

            //zoom grid
            if (e.type == EventType.scrollWheel && e.modifiers == EventModifiers.Alt)
            {
                float _in, _out;
                _out = _zoomFactor < 1 ? 0.05f : 0.1f;
                _in = _zoomFactor > 1 ? -0.1f : -.05f;
                _zoomFactor += e.delta.y < 0 ? _out : _in;
                _zoomFactor = Mathf.Clamp(_zoomFactor, 0.5f, 25.0f);
                _zoomFactor = (float)Math.Round(_zoomFactor, 3);
            }

            //checking for context menu
            if (e.type == EventType.ContextClick)
            {
                nodeProvider.Notify(NodeMessageSignature.Lock, new NodeMessageData(true)); //prevent nodes from moving due to random & odd mouse drag event been raised
                if (_frameArea.Contains(e.mousePosition))
                {
                    //adjust for difference between artboard and framearea
                    //new Vector2(_frameOffsetX + _frameSnapShotOffset.x - _scrollValueH, _frameOffsetY + _frameSnapShotOffset.y - _scrollValueV)));
                    _contextMousePosition.x = e.mousePosition.x - _frameOffsetX - _frameSnapShotOffset.x;
                    _contextMousePosition.y = e.mousePosition.y - _frameOffsetY - _frameSnapShotOffset.y - EditorStyles.toolbarButton.fixedHeight;
                    Utilities.BuildMainContextMenu(this.Callback, _dac.GetNodeList());
                    e.Use();
                }
            }

            if ((e.rawType == EventType.MouseUp) && e.modifiers != EventModifiers.Alt && !_onLinkActive) //mouseup somewhere not related to panning
            {
                nodeProvider.Notify(NodeMessageSignature.Lock, new NodeMessageData(false));
                if (panelList.Find(x => x.FrameArea.Contains(e.mousePosition)))
                {
                    //just check to see if up event occured in a toolbox so you don't reset nodes
                    nodeProvider.Notify(NodeMessageSignature.Focus, new NodeMessageData(ActionTarget.ON));
                }
                else
                {
                    //if marquee is not being drawn, deactivate nodes and elements
                    if (!_marqueeActive)
                    {
                        if (e.button == 0)
                        {
                            nodeProvider.Notify(NodeMessageSignature.Focus, new NodeMessageData(ActionTarget.RESET));
                            nodeProvider.Notify(NodeMessageSignature.Marquis, new NodeMessageData("MarqComplete"));
                            GUIUtility.hotControl = 0;
                        }
                        _marqueeON = false;
                    }
                }
                if (_marqueeActive)
                {
                    //a marquee was made, notify nodes of it and have them select themselves
                    nodeProvider.Notify(NodeMessageSignature.Focus, new NodeMessageData(ActionTarget.ON));
                    nodeProvider.Notify(NodeMessageSignature.Marquis, new NodeMessageData(new Marquis(_marquee)));
                    nodeProvider.Notify(NodeMessageSignature.Marquis, new NodeMessageData("MarqComplete"));
                    _marqueeActive = false;
                    _marqueeON = false;
                    _marqueeSTART.Set(0, 0);
                    _marquee.Set(0, 0, 0, 0);
                    _marqueeEND.Set(0, 0);
                }
            }

            //Main Window Drag Detection & Node Focus State Change
            if ((GUIUtility.hotControl + 2).Equals(_mainwindowID))
            {
                _unfocus = true;
                GUI.UnfocusWindow();
                nodeProvider.Notify(NodeMessageSignature.Focus, new NodeMessageData(ActionTarget.OFF));
            }
            if (!((GUIUtility.hotControl + 2).Equals(_mainwindowID)) && _unfocus)
            {
                _unfocus = false;
                nodeProvider.Notify(NodeMessageSignature.Focus, new NodeMessageData(ActionTarget.ON));
                nodeProvider.Notify(NodeMessageSignature.Lock, new NodeMessageData(false));
            }
        }

        //context menu callback function
        private void Callback(object userData)
        {
            ActionCommand _command = userData as ActionCommand;
            if (_command == null) { return; } //critical, "as" may return null if type is not of ActionCommand

            switch (_command.ID)
            {
                case ActionIdentifier.ADD:
                    {
                        Node _node = CreateInstance(_command.NodeAttribute.NodeType.Name) as Node;
                        _node.Initialize(_contextMousePosition, _command.NodeAttribute.DisplayName, new Vector2(124, 60));
                        _dac.AddNode(_node);
                        if (nodeProvider != null)
                        {
                            nodeProvider.Notify(NodeMessageSignature.Translate, new NodeMessageData(new Vector2(_frameOffsetX + _frameSnapShotOffset.x - _scrollValueH, _frameOffsetY + _frameSnapShotOffset.y - _scrollValueV)));
                        }
                        break;
                    }
                case ActionIdentifier.REMOVE:
                    {
                        if (_command.Target == ActionTarget.ALL)
                        {
                            nodeProvider.Notify(NodeMessageSignature.Removed, new NodeMessageData(ActionTarget.ALL)); //notify selected nodes of removal
                            _dac.RemoveAllNodes();
                        }
                        if (_command.Target == ActionTarget.SELECTED)
                        {
                            nodeProvider.Notify(NodeMessageSignature.Removed, new NodeMessageData(ActionTarget.SELECTED));
                            _dac.RemoveSelectedNodes();
                        }
                        break;
                    }
            }
        }

        void OnDestroy()
        {
            nodeProvider.Notify(NodeMessageSignature.Exit, new NodeMessageData(ActionTarget.ALL)); //calls OnComplete
            foreach (Panel panel in panelList)
            {
                panel.Unsubscribe();  //clears nodeProvider references
                DestroyImmediate(panel); //required to clear dangling references
            }
            panelList.Clear();
            panelList = null;
            nodeProvider = null;
            panelProvider = null;
            DestroyImmediate(Utilities);
            Utilities = null;
            DestroyImmediate(_dac);
            _dac = null;
            Debug.Log("Scene Builder Done...");
        }
    }
}
