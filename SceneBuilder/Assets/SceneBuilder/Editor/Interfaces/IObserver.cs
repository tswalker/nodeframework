﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public interface IObserver<Ta, Tb>
{
    void OnCompleted(Ta signature, Tb value);
    void OnError(Exception exception);
    void OnNext(Ta signature, Tb value);
    void Subscribe(IObservable<Ta, Tb> provider);
    void Unsubscribe();
}
