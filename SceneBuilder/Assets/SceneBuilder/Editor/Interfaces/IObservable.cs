﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public interface IObservable<Ta,Tb>
{
    IDisposable Subscribe(IObserver<Ta,Tb> observer);

}
