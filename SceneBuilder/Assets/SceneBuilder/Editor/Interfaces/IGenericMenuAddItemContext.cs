﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

namespace Assets.SceneBuilder.Editor.Utilities
{

    /// <summary>
    /// Describes current context of adding an item to a <see cref="GenericMenu"/>.
    /// </summary>
    /// <seealso cref="GenericMenuExtensions.AddItem(GenericMenu, GUIContent)"/>
    /// <seealso cref="GenericMenuExtensions.AddItem(GenericMenu, string)"/>
    public interface IGenericMenuAddItemContext
    {

        /// <summary>
        /// Specifies whether menu item should be enabled.
        /// </summary>
        /// <example>
        /// <code language="csharp"><![CDATA[
        /// var menu = new GenericMenu();
        /// 
        /// menu.AddItem("This item is disabled!")
        ///     .Enable(false)
        ///     .Action(() => {
        ///         Debug.Log("User cannot make this appear in log!");
        ///     });
        /// ]]></code>
        /// </example>
        /// <param name="enable">Specifies whether item is enabled.</param>
        /// <returns>
        /// The <see cref="IGenericMenuAddItemContext"/> instance for chained method calls.
        /// </returns>
        IGenericMenuAddItemContext Enable(bool enable);

        /// <summary>
        /// Specifies whether menu item should be enabled.
        /// </summary>
        /// <example>
        /// <code language="csharp"><![CDATA[
        /// var menu = new GenericMenu();
        /// 
        /// menu.AddItem("This item is disabled!")
        ///     .Enable(() => false)
        ///     .Action(() => {
        ///         Debug.Log("User cannot make this appear in log!");
        ///     });
        /// ]]></code>
        /// </example>
        /// <param name="predicate">Predicate that determines whether item is enabled by
        /// by returning a value of <c>true</c>.</param>
        /// <returns>
        /// The <see cref="IGenericMenuAddItemContext"/> instance for chained method calls.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// If <paramref name="predicate"/> is a value of <c>null</c>.
        /// </exception>
        IGenericMenuAddItemContext Enable(Func<bool> predicate);

        /// <summary>
        /// Specifies whether menu item should be visible in menu.
        /// </summary>
        /// <example>
        /// <code language="csharp"><![CDATA[
        /// var menu = new GenericMenu();
        /// 
        /// menu.AddItem("Visible when item is selected!")
        ///     .Visible(Selection.activeObject != null)
        ///     .Action(() => {
        ///         string objectName = Selection.activeObject.name;
        ///         Debug.Log(string.Format("Selected object is called '{0}'.", objectName));
        ///     });
        /// ]]></code>
        /// </example>
        /// <param name="visible">Specifies whether item is visible.</param>
        /// <returns>
        /// The <see cref="IGenericMenuAddItemContext"/> instance for chained method calls.
        /// </returns>
        IGenericMenuAddItemContext Visible(bool visible);

        /// <summary>
        /// Specifies whether menu item should be visible.
        /// </summary>
        /// <example>
        /// <code language="csharp"><![CDATA[
        /// var menu = new GenericMenu();
        /// 
        /// menu.AddItem("Visible when item is selected!")
        ///     .Visible(() => Selection.activeObject != null)
        ///     .Action(() => {
        ///         string objectName = Selection.activeObject.name;
        ///         Debug.Log(string.Format("Selected object is called '{0}'.", objectName));
        ///     });
        /// ]]></code>
        /// </example>
        /// <param name="predicate">Predicate that determines whether item is visible by
        /// returning a value of <c>true</c>.</param>
        /// <returns>
        /// The <see cref="IGenericMenuAddItemContext"/> instance for chained method calls.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// If <paramref name="predicate"/> is a value of <c>null</c>.
        /// </exception>
        IGenericMenuAddItemContext Visible(Func<bool> predicate);

        /// <summary>
        /// Specifies whether menu item should be in an "on" state (aka checked).
        /// </summary>
        /// <example>
        /// <code language="csharp"><![CDATA[
        /// var menu = new GenericMenu();
        /// 
        /// menu.AddItem("Item is checked!")
        ///     .On(true)
        ///     .Action(() => {
        ///         Debug.Log("Selected checked menu item!");
        ///     });
        /// ]]></code>
        /// </example>
        /// <param name="on">Indicates if menu item is "on".</param>
        /// <returns>
        /// The <see cref="IGenericMenuAddItemContext"/> instance for chained method calls.
        /// </returns>
        IGenericMenuAddItemContext On(bool on);

        /// <summary>
        /// Specifies whether menu item should be in an "on" state (aka checked).
        /// </summary>
        /// <example>
        /// <code language="csharp"><![CDATA[
        /// var menu = new GenericMenu();
        /// 
        /// menu.AddItem("Item is checked!")
        ///     .On(() => true)
        ///     .Action(() => {
        ///         Debug.Log("Selected checked menu item!");
        ///     });
        /// ]]></code>
        /// </example>
        /// <param name="predicate">Predicate that determines whether item is "on" by
        /// returning a value of <c>true</c>.</param>
        /// <returns>
        /// The <see cref="IGenericMenuAddItemContext"/> instance for chained method calls.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// If <paramref name="predicate"/> is a value of <c>null</c>.
        /// </exception>
        IGenericMenuAddItemContext On(Func<bool> predicate);

        /// <summary>
        /// Finalize addition of menu item by assigning an action to it.
        /// </summary>
        /// <example>
        /// <code language="csharp"><![CDATA[
        /// var menu = new GenericMenu();
        /// 
        /// menu.AddItem("Log message to console!")
        ///     .Action(() => {
        ///         Debug.Log("Hello, world!");
        ///     });
        /// ]]></code>
        /// </example>
        /// <param name="action">Delegate to perform menu action.</param>
        /// <exception cref="System.ArgumentNullException">
        /// If <paramref name="action"/> is a value of <c>null</c>.
        /// </exception>
        void Action(GenericMenu.MenuFunction action);

        /// <summary>
        /// Finalize addition of menu item by assigning an action to it.
        /// </summary>
        /// <example>
        /// <code language="csharp"><![CDATA[
        /// var menu = new GenericMenu();
        /// 
        /// menu.AddItem("Log message to console!")
        ///     .Action(userData => {
        ///         Debug.Log("Hello, {0}!", (string)userData);
        ///     }, "my friend");
        /// ]]></code>
        /// </example>
        /// <param name="action">Delegate to perform menu action.</param>
        /// <param name="userData">Additional data that is made available to menu action.</param>
        /// <exception cref="System.ArgumentNullException">
        /// If <paramref name="action"/> is a value of <c>null</c>.
        /// </exception>
        void Action(GenericMenu.MenuFunction2 action, object userData);

    }

}

//The MIT License (MIT) 
//Copyright (c) 2014 Rotorz Limited 
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
//to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
//and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: 
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. 
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
